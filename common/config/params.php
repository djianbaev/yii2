<?php
return [
    'adminEmail' => 'admin@life-vibes.com',
    'infoEmail' => 'info@life-vibes.com',
    'managerEmail' => 'manager@life-vibes.com',
    'bookingEmail' => 'booking@life-vibes.com',
    'senderEmail' => 'admin@life-vibes.com',
    'senderName' => 'Life Vibes',
    'user.passwordResetTokenExpire' => 3600,
];

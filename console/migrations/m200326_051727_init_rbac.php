<?php

use yii\db\Migration;

/**
 * Class m200326_051727_init_rbac
 */
class m200326_051727_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        // add "userAction" permission
        $userAction = $auth->createPermission('userAction');
        $userAction->description = 'User action';
        $auth->add($userAction);

        //add "user" role
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user,$userAction);

        // add "admin"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($admin, 1);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Plus */

$this->title = 'Изменить страница "Преимущества"';
$this->params['breadcrumbs'][] = ['label' => 'Страница "Преимущества"', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="plus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

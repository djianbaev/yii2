<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PlusItem */

$this->title = 'Добавить эл-т "Преимущества"';
$this->params['breadcrumbs'][] = ['label' => 'Элементы "Преимущества"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plus-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

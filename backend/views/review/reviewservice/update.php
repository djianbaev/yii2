<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\review\ReviewService */

$this->title = 'Изменить Отзыв от ' . $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Отзывы Сервисов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fullname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="review-service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\review\ReviewService */

$this->title = 'Create Review Service';
$this->params['breadcrumbs'][] = ['label' => 'Review Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-service-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

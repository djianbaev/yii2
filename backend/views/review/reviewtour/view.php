<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\review\ReviewTour */

$this->title = 'Отзыв от ' . $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Отзыв Туров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="review-tour-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить этот отзыв?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if ($main): ?>
            <?= Html::a('Убрать из главной страницы', ['offmain', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php else: ?>
            <?= Html::a('Добавить на главную страницу', ['tomain', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'tour_id',
                    'fullname',
                    'email:email',
                    'text:ntext',
                    'image',
                    'status',
                    'created_date',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= Html::img($model->image, ['class' => 'img-responsive']) ?>
        </div>
    </div>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\review\ReviewTour */

$this->title = 'Изменить Отзыв от ' . $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Отзывы Туров', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fullname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="review-tour-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\review\ReviewTour */

$this->title = 'Create Review Tour';
$this->params['breadcrumbs'][] = ['label' => 'Review Tours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-tour-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

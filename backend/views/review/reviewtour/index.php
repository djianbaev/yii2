<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\review\ReviewTourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы туров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-tour-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'tour_id',
            'fullname',
            'email:email',
            'text:ntext',
            //'image',
            'status',
            //'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Team */

$this->title = 'Изменить "Команда"';
$this->params['breadcrumbs'][] = ['label' => 'Страница "Команда"', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="team-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

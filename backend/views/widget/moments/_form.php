<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexMoments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="index-moments-form">

    <?php $form = ActiveForm::begin(); ?>


    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item active">
            <a class="nav-link active" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru"
               aria-selected="true">Русский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en"
               aria-selected="false">Английский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr"
               aria-selected="false">Французский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="it-tab" data-toggle="tab" href="#it" role="tab" aria-controls="it"
               aria-selected="false">Итальянский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="de-tab" data-toggle="tab" href="#de" role="tab" aria-controls="de"
               aria-selected="false">Немецкий</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="img-tab" data-toggle="tab" href="#img" role="tab" aria-controls="img"
               aria-selected="false">Изображение</a>
        </li>
    </ul>

    <div class="tab-content lv-tab" id="aboutTabs">
        <div class="tab-pane fade active in" id="ru" role="tabpanel" aria-labelledby="ru-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_ru')->textarea(['rows'=>6]); ?>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_en')->textarea(['rows'=>6]); ?>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="fr" role="tabpanel" aria-labelledby="fr-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_fr')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_fr')->textarea(['rows'=>6]); ?>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="it" role="tabpanel" aria-labelledby="it-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_it')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_it')->textarea(['rows'=>6]); ?>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="de" role="tabpanel" aria-labelledby="de-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_de')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_de')->textarea(['rows'=>6]); ?>
                </div>
            </div>
        </div>




        <div class="tab-pane fade" id="img" role="tabpanel" aria-labelledby="img-tab">
            <?= $form->field($model, 'fileUpdate')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
            ]) ?>
        </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton('СОХРАНИТЬ', ['class' => 'btn btn-success','style'=>['width'=>'100%']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

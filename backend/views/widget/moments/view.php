<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexMoments */

$this->title = $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Блок Моменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="index-moments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            'text_ru:ntext',
            'text_en:ntext',
            'text_fr:ntext',
            'text_it:ntext',
            'text_de:ntext',
            'image',
        ],
    ]) ?>
    <?php echo Html::img($model->image,['class'=>'img-responsive'])?>
</div>

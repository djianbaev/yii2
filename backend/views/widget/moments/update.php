<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexMoments */

$this->title = 'Изменить: ' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Блок Моменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="index-moments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

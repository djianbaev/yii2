<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexDestination */

$this->title = 'Изменить Блок Города';
$this->params['breadcrumbs'][] = ['label' => 'Блок Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="index-destination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexDestination */

$this->title = 'Блок Города';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="index-destination-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title_ru',
            'title_en',
            'title_fr',
            'title_it',
            'title_de',
            'sub_title_ru',
            'sub_title_en',
            'sub_title_fr',
            'sub_title_it',
            'sub_title_de',
        ],
    ]) ?>

</div>

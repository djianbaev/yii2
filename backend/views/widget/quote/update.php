<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexQuote */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Цитата (блок с моментами)', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="index-quote-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\MomentsItem */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moments-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'moments'=>$moments
    ]) ?>

</div>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 13.04.2020
 * Time: 22:42
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexSlider */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="slider-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'dropZoneEnabled' => true, 'multiple' => false],
    ]) ?>
    <?php ActiveForm::end(); ?>


    <hr>
    <style>
        .col-md-6 {
            border: 0.5px solid #c1c1c1;
            padding: 5px;
        }
    </style>
    <?php if ($models): ?>
        <h2>Фотографии</h2>
        <div class="row">
            <?php foreach ($models as $item): ?>
                <div class="col-md-6">
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $item->id], [
                        'class' => 'btn btn-default col-md-12',
                        'data' => [
                            'confirm' => 'Вы уверены что хотите удалить эту запись?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <img src="<?php echo $item->image ?>" class="img-responsive">
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

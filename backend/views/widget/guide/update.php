<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexGuide */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Гид (Главная Страница)', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="index-guide-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

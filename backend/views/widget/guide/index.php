<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model backend\models\widget\IndexGuide */

$this->title = 'Гид (Главная страница)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index-guide-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'quote_ru:ntext',
            'quote_en:ntext',
            'quote_fr:ntext',
            'quote_it:ntext',
            'quote_de:ntext',
            'title_ru',
            'title_en',
            'title_fr',
            'title_it',
            'title_de',
            'button_ru',
            'button_en',
            'button_fr',
            'button_it',
            'button_de',
        ],
    ]) ?>

    <?php echo Html::img($model->image,['class'=>'img-responsive'])?>
</div>

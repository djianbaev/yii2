<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\TourMenu */

$this->title = 'Меню туры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            'text_ru:ntext',
            'text_en:ntext',
            'text_fr:ntext',
            'text_it:ntext',
            'text_de:ntext',
            'image',
        ],
    ]) ?>

    <?= Html::img($model->image,['class'=>'img-responsive'])?>

</div>

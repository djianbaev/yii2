<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\TourMenu */

$this->title = 'Изменить Меню тур';
$this->params['breadcrumbs'][] = ['label' => 'Меню Тур', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="tour-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

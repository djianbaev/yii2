<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\tour\ServiceItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сервисы Объекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            //'intro_ru:ntext',
            //'intro_en:ntext',
            //'intro_fr:ntext',
            //'intro_it:ntext',
            //'intro_de:ntext',
            //'text_en:ntext',
            //'text_ru:ntext',
            //'text_fr:ntext',
            //'text_it:ntext',
            //'text_de:ntext',
            //'price',
            //'duration',
            //'location',
            //'date:ntext',
            //'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

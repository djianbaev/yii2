<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\TourSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name_ru') ?>

    <?= $form->field($model, 'name_en') ?>

    <?= $form->field($model, 'name_fr') ?>

    <?= $form->field($model, 'name_it') ?>

    <?php // echo $form->field($model, 'name_de') ?>

    <?php // echo $form->field($model, 'descr_ru') ?>

    <?php // echo $form->field($model, 'descr_en') ?>

    <?php // echo $form->field($model, 'descr_fr') ?>

    <?php // echo $form->field($model, 'descr_it') ?>

    <?php // echo $form->field($model, 'descr_de') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'days_night_ru') ?>

    <?php // echo $form->field($model, 'days_night_en') ?>

    <?php // echo $form->field($model, 'days_night_fr') ?>

    <?php // echo $form->field($model, 'days_night_it') ?>

    <?php // echo $form->field($model, 'days_night_de') ?>

    <?php // echo $form->field($model, 'text_ru') ?>

    <?php // echo $form->field($model, 'text_en') ?>

    <?php // echo $form->field($model, 'text_fr') ?>

    <?php // echo $form->field($model, 'text_it') ?>

    <?php // echo $form->field($model, 'text_de') ?>

    <?php // echo $form->field($model, 'include_ru') ?>

    <?php // echo $form->field($model, 'include_en') ?>

    <?php // echo $form->field($model, 'include_fr') ?>

    <?php // echo $form->field($model, 'include_it') ?>

    <?php // echo $form->field($model, 'include_de') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

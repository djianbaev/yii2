<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 02.04.2020
 * Time: 18:26
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */
/* @var $gallery backend\models\tour\TourGallery */

$this->title = 'Галерея';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Галерея';

?>
<div class="guide-gallery-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($gallery, 'file[]')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'dropZoneEnabled' => true, 'multiple' => true],
    ]) ?>
    <?php ActiveForm::end(); ?>


    <hr>
    <style>
        .col-md-3 {
            border: 0.5px solid #c1c1c1;
            padding: 5px;
        }
    </style>
    <?php if ($db_images): ?>
        <h2>Фотографии</h2>
        <div class="row">
            <?php foreach ($db_images as $item): ?>
                <div class="col-md-3">
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['gallerydelete', 'id' => $model->id, 'gid' => $item->id], [
                        'class' => 'btn btn-default col-md-12',
                        'data' => [
                            'confirm' => 'Вы уверены что хотите удалить эту запись?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <img src="<?php echo $item->image ?>" class="img-responsive">
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

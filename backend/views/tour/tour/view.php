<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */

$this->title = $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tour-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Список дней', ['days', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Календарь', ['cals', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Галерея', ['gallery', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            'descr_ru:ntext',
            'descr_en:ntext',
            'descr_fr:ntext',
            'descr_it:ntext',
            'descr_de:ntext',
            'price',
            'days_night_ru',
            'days_night_en',
            'days_night_fr',
            'days_night_it',
            'days_night_de',
            'text_ru:ntext',
            'text_en:ntext',
            'text_fr:ntext',
            'text_it:ntext',
            'text_de:ntext',
            'include_ru:ntext',
            'include_en:ntext',
            'include_fr:ntext',
            'include_it:ntext',
            'include_de:ntext',
            'image',
            'slug',
        ],
    ]) ?>
    <?php echo Html::img($model->image,['class'=>'img-responsive'])?>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 09.04.2020
 * Time: 20:30
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */

$this->title = 'Список дней';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Список дней';
?>
<div class="tour-days">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('Добавить', ['daycreate', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    <div id="w0" class="grid-view">

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th><a href="" data-sort="name_ru">Название Ru</a></th>
                <th><a href="" data-sort="name_en">Название En</a></th>
                <th><a href="" data-sort="name_fr">Название Fr</a></th>
                <th class="action-column">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($days)): ?>
                <?php foreach ($days as $day): ?>
                    <tr>
                        <td><?php echo $day['name_ru']?></td>
                        <td><?php echo $day['name_en']?></td>
                        <td><?php echo $day['name_fr']?></td>
                        <td>
                            <a href="<?php echo Url::to(['dayview','id'=>$model->id,'did'=>$day['id']])?>" title="Просмотр" aria-label="Просмотр" data-pjax="0">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="<?php echo Url::to(['dayupdate','id'=>$model->id,'did'=>$day['id']])?>" title="Редактировать" aria-label="Редактировать"
                               data-pjax="0">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a href="<?php echo Url::to(['daydelete','id'=>$model->id,'did'=>$day['id']])?>" title="Удалить" aria-label="Удалить" data-pjax="0"
                               data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

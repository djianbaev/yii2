<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */

$this->title = 'Добавить тур';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

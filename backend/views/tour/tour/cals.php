<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 09.04.2020
 * Time: 19:15
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */

$this->title = 'Лучшее время для путешествий';
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Лучшее время для путешествий';
?>
<div class="tour-cals">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (empty($rowD)): ?>
        <form method="post">
            <input type="hidden" name="_csrf-backend" value="<?php echo Yii::$app->request->csrfToken ?>">
            <?php foreach ($rowC as $item): ?>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <span class="form-control"><?php echo $item['name_ru'] ?></span>
                    </div>
                    <div class="col-md-6 form-group">
                        <select class="form-control" name="status_<?php echo $item['id'] ?>">
                            <option value="1">Да</option>
                            <option value="0">Нет</option>
                        </select>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="form-group">
                <button type="submit" class="btn btn-success" style="width: 100%;">СОХРАНИТЬ</button>
            </div>
        </form>
    <?php endif; ?>




    <?php if (!empty($rowD)): ?>
        <form method="post">
            <input type="hidden" name="_csrf-backend" value="<?php echo Yii::$app->request->csrfToken ?>">
            <?php foreach ($rowD as $item): ?>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <span class="form-control"><?php echo $item['name_ru'] ?></span>
                    </div>
                    <div class="col-md-6 form-group">
                        <select class="form-control" name="status_<?php echo $item['cal_id'] ?>">
                            <?php if ($item['value'] == 1): ?>
                                <option value="1" selected>Да</option>
                                <option value="0">Нет</option>
                            <?php else: ?>
                                <option value="1">Да</option>
                                <option value="0" selected>Нет</option>
                            <?php endif; ?>

                        </select>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="form-group">
                <button type="submit" class="btn btn-success" style="width: 100%;">СОХРАНИТЬ</button>
            </div>
        </form>
    <?php endif; ?>

</div>

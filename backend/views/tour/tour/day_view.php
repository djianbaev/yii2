<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */
/* @var $day_model backend\models\tour\TourDay */

$this->title = $day_model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Список дней', 'url' => ['days', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tour-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['dayupdate', 'id' => $model->id,'did'=>$day_model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['daydelete', 'id' => $model->id,'did'=>$day_model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>

    </p>

    <?= DetailView::widget([
        'model' => $day_model,
        'attributes' => [
            //'id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            'text_ru:ntext',
            'text_en:ntext',
            'text_fr:ntext',
            'text_it:ntext',
            'text_de:ntext',
        ],
    ]) ?>

</div>

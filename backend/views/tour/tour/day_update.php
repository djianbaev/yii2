<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 09.04.2020
 * Time: 20:30
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\tour\Tour */

$this->title = 'Изменить: '.$day_model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Туры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Список дней', 'url' => ['days', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => $day_model->name_ru, 'url' => ['dayview', 'id' => $model->id,'did'=>$day_model->id]];
$this->params['breadcrumbs'][] = 'Список дней';
?>
<div class="tour-days">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_day', [
        'day_model'=>$day_model
    ]) ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Contact */

$this->title = 'Контакт Страница';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'address_ru',
            'address_en',
            'address_fr',
            'address_it',
            'address_de',
            'phone',
            'email:email',
            'work_ru',
            'work_en',
            'work_fr',
            'work_it',
            'work_de',
            'banner',
        ],
    ]) ?>
    <?= Html::img($model->banner,['class'=>'img-responsive'])?>
</div>

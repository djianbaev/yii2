<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item active">
            <a class="nav-link active" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru"
               aria-selected="true">Адрес</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en"
               aria-selected="false">Телефон,Почта</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr"
               aria-selected="false">Раб.часы</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="img-tab" data-toggle="tab" href="#img" role="tab" aria-controls="img"
               aria-selected="false">Баннер</a>
        </li>
    </ul>
    <div class="tab-content lv-tab" id="aboutTabs">
        <div class="tab-pane fade active in" id="ru" role="tabpanel" aria-labelledby="ru-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'address_ru')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_en')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_fr')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_it')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_de')->textInput(['maxlength' => true])?></div>
            </div>
        </div>

        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
            </div>
        </div>


        <div class="tab-pane fade" id="fr" role="tabpanel" aria-labelledby="fr-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'work_ru')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'work_en')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'work_fr')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'work_it')->textInput(['maxlength' => true])?></div>
                <div class="col-md-12"><?= $form->field($model, 'work_de')->textInput(['maxlength' => true])?></div>
            </div>
        </div>


        <div class="tab-pane fade" id="img" role="tabpanel" aria-labelledby="img-tab">
            <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
            ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('СОХРАНИТЬ', ['class' => 'btn btn-success','style'=>['width'=>'100%']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

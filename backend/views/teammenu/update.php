<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TeamMenu */

$this->title = 'Изменить Меню "Команда"';
$this->params['breadcrumbs'][] = ['label' => 'Меню "Команда"', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="team-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\About */

$this->title = 'Изменить О Нас';
$this->params['breadcrumbs'][] = ['label' => 'О Нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="about-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

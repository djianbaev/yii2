<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\city\GuideMenu */

$this->title = 'Добавить подкатегорию';
$this->params['breadcrumbs'][] = ['label' => 'Мой гид - Подкатегории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

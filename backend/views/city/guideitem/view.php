<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\city\GuideItem */

$this->title = $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Мой гид - Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="guide-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Галерея', ['gallery', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'guide_menu_id',
            'city_id',
            'name_ru',
            //'name_en',
            //'name_fr',
            //'name_it',
            //'name_de',
            'text_ru:html',
            //'text_en:ntext',
            //'text_fr:ntext',
            //'text_it:ntext',
            //'text_de:ntext',
            [
                'label' => 'Изображение',
                'value' => Html::img($model->image, ['class' => 'img-responsive']),
                'format' => 'html'
            ],
            'status',
            'top',
        ],
    ]) ?>

</div>

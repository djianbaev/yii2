<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\city\GuideItem */

$this->title = 'Изменить объект:' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Мой гид - Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="guide-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
        'cities'=>$cities,
        'menus'=>$menus
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\city\GuideItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guide-item-form">

    <?php $form = ActiveForm::begin(); ?>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item active">
            <a class="nav-link active" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru"
               aria-selected="true">Русский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en"
               aria-selected="false">Английский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr"
               aria-selected="false">Французский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="it-tab" data-toggle="tab" href="#it" role="tab" aria-controls="it"
               aria-selected="false">Итальянский</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="de-tab" data-toggle="tab" href="#de" role="tab" aria-controls="de"
               aria-selected="false">Немецкий</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="img-tab" data-toggle="tab" href="#img" role="tab" aria-controls="img"
               aria-selected="false">Изображения</a>
        </li>
    </ul>

    <div class="tab-content lv-tab" id="aboutTabs">
        <div class="tab-pane fade active in" id="ru" role="tabpanel" aria-labelledby="ru-tab">
            <div class="row">

                <div class="col-md-6">
                    <?= $form->field($model, 'guide_menu_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map($menus, 'id', 'name_ru'),
                        ['prompt' => '----Выберите подкатегорию----']
                    ) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'city_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map($cities, 'id', 'name_ru'),
                        ['prompt' => '----Выберите город----']
                    ) ?>
                </div>

                <div class="col-md-6"><?= $form->field($model, 'top')->dropDownList([0 => 'Нет', 1 => 'Да']) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'status')->dropDownList([1 => 'Активный', 0 => 'Неактивный']) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_ru')->widget(CKEditor::className(), [
                        'editorOptions' => [
                            'present' => 'basic',
                            'inline' => false
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_en')->textInput(['maxlength' => true, 'onchange' => 'slug();']) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_en')->widget(CKEditor::className(), [
                        'editorOptions' => [
                            'present' => 'basic',
                            'inline' => false
                        ],
                    ]); ?>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="fr" role="tabpanel" aria-labelledby="fr-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_fr')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_fr')->widget(CKEditor::className(), [
                        'editorOptions' => [
                            'present' => 'basic',
                            'inline' => false
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="it" role="tabpanel" aria-labelledby="it-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_it')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_it')->widget(CKEditor::className(), [
                        'editorOptions' => [
                            'present' => 'basic',
                            'inline' => false
                        ],
                    ]); ?>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="de" role="tabpanel" aria-labelledby="de-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'name_de')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text_de')->widget(CKEditor::className(), [
                        'editorOptions' => [
                            'present' => 'basic',
                            'inline' => false
                        ],
                    ]); ?>
                </div>
            </div>
        </div>


        <div class="tab-pane fade" id="img" role="tabpanel" aria-labelledby="img-tab">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                    ]) ?>
                    <?= $form->field($model, 'slug')->hiddenInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('СОХРАНИТЬ', ['class' => 'btn btn-success', 'style' => ['width' => '100%']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function slug() {
        var text = $('#guideitem-name_en')[0].value;
        var slug = url_slug(text);
        $('#guideitem-slug')[0].value = slug;
    }

    function url_slug(s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': undefined,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }
</script>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\city\GuideItem */

$this->title = 'Добавить объект';
$this->params['breadcrumbs'][] = ['label' => 'Мой гид - Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cities'=>$cities,
        'menus'=>$menus
    ]) ?>

</div>

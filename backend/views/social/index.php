<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Social */

$this->title = 'Соц.сети, контакты, о нас (подвал)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'telegram',
            'instagram',
            'facebook',
            'youtube',
            'whatsapp',
            'twitter',
            'pinterest',
            'phone',
            'email:email',
            'address_ru',
            'address_en',
            'address_fr',
            'address_it',
            'address_de',
            'about_ru:ntext',
            'about_en:ntext',
            'about_fr:ntext',
            'about_it:ntext',
            'about_de:ntext',
        ],
    ]) ?>


</div>

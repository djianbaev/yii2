<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Social */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item active">
            <a class="nav-link active" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru"
               aria-selected="true">Соц.сети</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en"
               aria-selected="false">Контакты в шапке</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="fr-tab" data-toggle="tab" href="#fr" role="tab" aria-controls="fr"
               aria-selected="false">О Нас (подвал)</a>
        </li>
    </ul>




    <div class="tab-content lv-tab" id="aboutTabs">
        <div class="tab-pane fade active in" id="ru" role="tabpanel" aria-labelledby="ru-tab">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'telegram')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'whatsapp')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'pinterest')->textInput(['maxlength' => true]) ?></div>
            </div>
        </div>

        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_ru')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_en')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_fr')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_it')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'address_de')->textInput(['maxlength' => true]) ?></div>
            </div>
        </div>


        <div class="tab-pane fade" id="fr" role="tabpanel" aria-labelledby="fr-tab">
            <div class="row">
                <div class="col-md-12"><?= $form->field($model, 'about_ru')->textarea(['rows' => 6]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'about_en')->textarea(['rows' => 6]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'about_fr')->textarea(['rows' => 6]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'about_it')->textarea(['rows' => 6]) ?></div>
                <div class="col-md-12"><?= $form->field($model, 'about_de')->textarea(['rows' => 6]) ?></div>
            </div>
        </div>

    </div>


    <div class="form-group">
        <?= Html::submitButton('СОХРАНИТЬ', ['class' => 'btn btn-success','style'=>['width'=>'100%']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Social */

$this->title = 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Соц.сети, контакты, о нас (подвал)', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="social-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

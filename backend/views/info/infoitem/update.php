<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\info\InfoItem */

$this->title = 'Изменить: ' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="info-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
        'menus'=>$menus
    ]) ?>

</div>

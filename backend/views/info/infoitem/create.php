<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\info\InfoItem */

$this->title = 'Добавить информацию';
$this->params['breadcrumbs'][] = ['label' => 'Информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'menus'=>$menus
    ]) ?>

</div>

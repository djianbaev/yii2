<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\info\InfoMenu */

$this->title = 'Добавить подкатегорию';
$this->params['breadcrumbs'][] = ['label' => 'Информация - Подкатегории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

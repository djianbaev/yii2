<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TeamItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fio_ru') ?>

    <?= $form->field($model, 'fio_en') ?>

    <?= $form->field($model, 'fio_fr') ?>

    <?= $form->field($model, 'fio_it') ?>

    <?php // echo $form->field($model, 'fio_de') ?>

    <?php // echo $form->field($model, 'position_ru') ?>

    <?php // echo $form->field($model, 'position_en') ?>

    <?php // echo $form->field($model, 'position_fr') ?>

    <?php // echo $form->field($model, 'position_it') ?>

    <?php // echo $form->field($model, 'position_de') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

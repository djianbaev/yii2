<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TeamItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Элементы "Команда"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'fio_ru',
            //'fio_en',
            //'fio_fr',
            //'fio_it',
            //'fio_de',
            'position_ru',
            //'position_en',
            //'position_fr',
            //'position_it',
            //'position_de',
            //'image',
            'email:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

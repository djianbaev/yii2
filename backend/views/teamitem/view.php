<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TeamItem */

$this->title = $model->fio_ru;
$this->params['breadcrumbs'][] = ['label' => 'Элементы "Команда"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="team-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'email:email',
            [
                'label' => 'Аватарка',
                'value'=>Html::img($model->image),
                'format'=>'html'

            ],
            'fio_ru',
            'fio_en',
            'fio_fr',
            'fio_it',
            'fio_de',
            'position_ru',
            'position_en',
            'position_fr',
            'position_it',
            'position_de',
        ],
    ]) ?>

</div>

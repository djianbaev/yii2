<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TeamItem */

$this->title = 'Изменить: ' . $model->fio_ru;
$this->params['breadcrumbs'][] = ['label' => 'Элементы "Команда"', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fio_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="team-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>

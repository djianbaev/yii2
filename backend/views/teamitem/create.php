<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TeamItem */

$this->title = 'Добавить эл-т "Команда"';
$this->params['breadcrumbs'][] = ['label' => 'Элементы "Команда"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

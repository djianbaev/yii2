<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео галерея';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    img{
        width: 100%;
    }
</style>
<div class="video-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-4"><img src="<?php echo \yii\helpers\Url::base()?>/images/y1.png"></div>
        <div class="col-md-4"><img src="<?php echo \yii\helpers\Url::base()?>/images/y2.png"></div>
        <div class="col-md-4"><img src="<?php echo \yii\helpers\Url::base()?>/images/y3.png"></div>
    </div>
    <hr><br>

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model,'main')->hiddenInput(['value'=>0])->label(false)?>
    <?php echo $form->field($model, 'link')->textInput(['placeholder'=>'Вставьте сюда ссылку из ютуба как показано на рисунке']) ?>
    <?php echo Html::submitButton('Загрузить', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end(); ?>

    <hr>


    <div class="row">
        <?php if (!empty($datas)): ?>
            <?php foreach ($datas as $data): ?>
                <div class="col-md-6">
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Убрать', ['delete', 'id' => $data['id']], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Вы уверены что хотите удалить эту запись?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php if($data['main'] == 0):?>
                        <?= Html::a('<i class="glyphicon glyphicon-facetime-video"></i> На главную страницу', ['tomain', 'id' => $data['id']], [
                            'class' => 'btn btn-primary',
                        ]) ?>
                    <?php else:?>
                        <?= Html::a('<i class="glyphicon glyphicon-facetime-video"></i> Убрать с главной страницы', ['offmain', 'id' => $data['id']], [
                            'class' => 'btn btn-primary',
                        ]) ?>
                    <?php endif;?>
                    <?php echo $data['link'] ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FooterItem */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Подвал меню эл-ы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'footers'=>$footers
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FooterItem */

$this->title = $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Подвал меню эл-ы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="footer-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'footer_id',
            'name_ru',
            'name_en',
            'name_fr',
            'name_it',
            'name_de',
            'text_ru:ntext',
            'text_en:ntext',
            'text_fr:ntext',
            'text_it:ntext',
            'text_de:ntext',
            'image',
        ],
    ]) ?>
    <?=Html::img($model->image,['class'=>'img-responsive'])?>
</div>

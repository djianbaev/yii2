<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PlusMenu */

$this->title = 'Меню "Преимущества"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plus-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title_ru',
            'title_en',
            //'title_fr',
            //'title_it',
            //'title_de',
            'text_ru:html',
            'text_en:html',
            //'text_fr:ntext',
            //'text_it:ntext',
            //'text_de:ntext',
            //'image',
        ],
    ]) ?>

    <?php echo Html::img($model->image,['class'=>'img-responsive'])?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PlusMenu */

$this->title = 'Изменить Меню "Преимущества"';
$this->params['breadcrumbs'][] = ['label' => 'Меню "Преимущества"', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="plus-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

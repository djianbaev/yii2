<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PlusItem */

$this->title = 'Пользователь: '.$user['username'];
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="plus-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Логин</th>
            <td><?php echo $user['username']?></td>
        </tr>
        <tr>
            <th>Эл.адрес</th>
            <td><?php echo $user['email']?></td>
        </tr>
        <?php if(!empty($userdata)):?>
            <tr>
                <th>Имя Фамилия</th>
                <td><?php echo $userdata['name'].' '.$userdata['surname']?></td>
            </tr>
            <tr>
                <th>Номер телефона</th>
                <td><?php echo $userdata['phone']?></td>
            </tr>
            <tr>
                <th>Страна</th>
                <td><?php echo $userdata['country']?></td>
            </tr>
            <tr>
                <th>Город</th>
                <td><?php echo $userdata['city']?></td>
            </tr>
            <tr>
                <th>Дата рождения</th>
                <td><?php echo $userdata['bday']?></td>
            </tr>
        <?php endif;?>
        </tbody>
    </table>

</div>

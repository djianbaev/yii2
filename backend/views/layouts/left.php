<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->homeUrl ?>/images/admin.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Администратор</p>

                <a href="#"><i class="fa fa-circle text-success" style="color: #eec32a;"></i> Online</a>
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Навигация', 'options' => ['class' => 'header']],
                    ['label' => 'Выход', 'icon' => 'sign-out', 'url' => ['site/logout']],
                    ['label' => 'Вход', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],


                    [
                        'label' => 'Главная страница',
                        'icon' => 'home',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Слайдер', 'icon' => 'circle-o', 'url' => ['/widget/slider'],],
                            ['label' => 'Цитата', 'icon' => 'circle-o', 'url' => ['/widget/quote'],],
                            ['label' => 'Блок моменты', 'icon' => 'circle-o', 'url' => ['/widget/moments'],],
                            ['label' => 'Объекты', 'icon' => 'circle-o', 'url' => ['/widget/momentsitem'],],
                            ['label' => 'Гид', 'icon' => 'circle-o', 'url' => ['/widget/guide'],],
                            ['label' => 'Блок Города', 'icon' => 'circle-o', 'url' => ['/widget/destination'],],
                            ['label' => 'Подвал меню', 'icon' => 'circle-o', 'url' => ['/footer'],],
                            ['label' => 'Подвал меню эл-ы', 'icon' => 'circle-o', 'url' => ['/footeritem'],],
                        ],
                    ],

                    [
                        'label' => 'Контактные данные',
                        'icon' => 'facebook-official',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Соц.сети', 'icon' => 'circle-o', 'url' => ['/social'],],
                            ['label' => 'Контакт Ст-ца', 'icon' => 'circle-o', 'url' => ['/contact'],],
                        ],
                    ],









                    [
                        'label' => 'Меню сайта',
                        'icon' => 'sitemap',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Главное меню', 'icon' => 'circle-o', 'url' => ['/menu'],],

                            [
                                'label' => 'Вкладка О Нас',
                                'icon' => 'circle',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Меню "О Нас"', 'icon' => 'edit', 'url' => ['/aboutmenu'],],
                                    ['label' => 'Ст-ца "О Нас"', 'icon' => 'edit', 'url' => ['/about'],],

                                    ['label' => 'Меню "Преимущества"', 'icon' => 'edit', 'url' => ['/plusmenu'],],
                                    ['label' => 'Ст-ца "Преимущества"', 'icon' => 'edit', 'url' => ['/plus'],],
                                    ['label' => 'Эл-ы "Преимущества"', 'icon' => 'edit', 'url' => ['/plusitem'],],

                                    ['label' => 'Меню "Команда"', 'icon' => 'edit', 'url' => ['/teammenu'],],
                                    ['label' => 'Ст-ца "Команда"', 'icon' => 'edit', 'url' => ['/team'],],
                                    ['label' => 'Эл-ы "Команда"', 'icon' => 'edit', 'url' => ['/teamitem'],],
                                ],
                            ],
                        ],
                    ],
                    /*** Guide ***/
                    [
                        'label' => 'Мой Гид',
                        'icon' => 'life-bouy',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Города', 'icon' => 'circle-o', 'url' => ['/city/city'],],
                            ['label' => 'Подкатегории', 'icon' => 'circle-o', 'url' => ['/city/guidemenu'],],
                            ['label' => 'Объекты', 'icon' => 'circle-o', 'url' => ['/city/guideitem'],],

                        ],
                    ],

                    /*** Info ***/
                    [
                        'label' => 'Региональная инфор-я',
                        'icon' => 'info',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Подкатегории', 'icon' => 'circle-o', 'url' => ['/info/infomenu'],],
                            ['label' => 'Информация', 'icon' => 'circle-o', 'url' => ['/info/infoitem'],],

                        ],
                    ],

                    /*** tours ***/
                    [
                        'label' => 'Опыт',
                        'icon' => 'trophy',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Сервисы', 'icon' => 'circle-o', 'url' => ['/tour/service'],],
                            ['label' => 'Сервисы Объекты', 'icon' => 'circle-o', 'url' => ['/tour/serviceitem'],],


                            [
                                'label' => 'Туры',
                                'icon' => 'circle',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Календарь', 'icon' => 'circle-o', 'url' => ['/tour/calendar'],],
                                    ['label' => 'Меню Туры', 'icon' => 'circle-o', 'url' => ['/tour/tourmenu'],],
                                    ['label' => 'Туры', 'icon' => 'circle-o', 'url' => ['/tour/tour'],],

                                ],
                            ],

                        ],
                    ],

                    [
                        'label' => 'Отзывы',
                        'icon' => 'comments',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Отзывы туров', 'icon' => 'circle-o', 'url' => ['/review/reviewtour'],],
                            ['label' => 'Отзывы сервисов', 'icon' => 'circle-o', 'url' => ['/review/reviewservice'],],
                        ],
                    ],

                    ['label' => 'Видео галерея', 'icon' => 'youtube-play', 'url' => ['/video'],],

                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['client/index']],
                ],
            ]
        ) ?>

    </section>

</aside>

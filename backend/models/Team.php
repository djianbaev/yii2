<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Заголовок Ru',
            'title_en' => 'Заголовок En',
            'title_fr' => 'Заголовок Fr',
            'title_it' => 'Заголовок It',
            'title_de' => 'Заголовок De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
        ];
    }
}

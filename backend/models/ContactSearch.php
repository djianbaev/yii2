<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Contact;

/**
 * ContactSearch represents the model behind the search form of `backend\models\Contact`.
 */
class ContactSearch extends Contact
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['address_ru', 'address_en', 'address_fr', 'address_it', 'address_de', 'phone', 'email', 'work_ru', 'work_en', 'work_fr', 'work_it', 'work_de', 'banner'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contact::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'address_ru', $this->address_ru])
            ->andFilterWhere(['like', 'address_en', $this->address_en])
            ->andFilterWhere(['like', 'address_fr', $this->address_fr])
            ->andFilterWhere(['like', 'address_it', $this->address_it])
            ->andFilterWhere(['like', 'address_de', $this->address_de])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'work_ru', $this->work_ru])
            ->andFilterWhere(['like', 'work_en', $this->work_en])
            ->andFilterWhere(['like', 'work_fr', $this->work_fr])
            ->andFilterWhere(['like', 'work_it', $this->work_it])
            ->andFilterWhere(['like', 'work_de', $this->work_de])
            ->andFilterWhere(['like', 'banner', $this->banner]);

        return $dataProvider;
    }
}

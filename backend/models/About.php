<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Заголовок Ru',
            'title_en' => 'Заголовок En',
            'title_fr' => 'Заголовок Fr',
            'title_it' => 'Заголовок It',
            'title_de' => 'Заголовок De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображение',
            'file' => 'Изображение',
        ];
    }
}

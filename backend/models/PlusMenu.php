<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plus_menu".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class PlusMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plus_menu';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Заоловок Ru',
            'title_en' => 'Заоловок En',
            'title_fr' => 'Заоловок Fr',
            'title_it' => 'Заоловок It',
            'title_de' => 'Заоловок De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображение (квадратное)',
            'file' => 'Изображение (квадратное)',
        ];
    }
}

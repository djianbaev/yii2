<?php

namespace backend\models\info;

use Yii;

/**
 * This is the model class for table "info_menu".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property string $slug
 * @property int $number
 * @property int $status
 */
class InfoMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'info_menu';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'slug', 'number', 'status'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['number', 'status'], 'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image', 'slug'], 'string', 'max' => 255],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наменование Ru',
            'name_en' => 'Наменование En',
            'name_fr' => 'Наменование Fr',
            'name_it' => 'Наменование It',
            'name_de' => 'Наменование De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображение (квадрат)',
            'file' => 'Изображение (квадрат)',
            'fileUpdate' => 'Изображение (квадрат)',
            'slug' => 'Slug',
            'number' => 'Порядковый номер',
            'status' => 'Статус',
        ];
    }
}

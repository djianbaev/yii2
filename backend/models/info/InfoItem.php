<?php

namespace backend\models\info;

use Yii;

/**
 * This is the model class for table "info_item".
 *
 * @property int $id
 * @property int $info_menu_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property string $slug
 */
class InfoItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'info_item';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','info_menu_id', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'slug'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['info_menu_id'],'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image','slug'], 'string', 'max' => 255],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наменование Ru',
            'name_en' => 'Наменование En',
            'name_fr' => 'Наменование Fr',
            'name_it' => 'Наменование It',
            'name_de' => 'Наменование De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображение (16х9)',
            'file' => 'Изображение (16х9)',
            'fileUpdate' => 'Изображение (16х9)',
            'slug' => 'Slug',
        ];
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "team_item".
 *
 * @property int $id
 * @property string $fio_ru
 * @property string $fio_en
 * @property string $fio_fr
 * @property string $fio_it
 * @property string $fio_de
 * @property string $position_ru
 * @property string $position_en
 * @property string $position_fr
 * @property string $position_it
 * @property string $position_de
 * @property string $image
 * @property string $email
 */
class TeamItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_item';
    }
    public $file,$fileUpdate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','fio_ru', 'fio_en', 'fio_fr', 'fio_it', 'fio_de', 'position_ru', 'position_en', 'position_fr', 'position_it', 'position_de', 'image', 'email'], 'required'],
            [['fio_ru', 'fio_en', 'fio_fr', 'fio_it', 'fio_de', 'position_ru', 'position_en', 'position_fr', 'position_it', 'position_de', 'image', 'email'], 'string', 'max' => 255],
            [['file','fileUpdate'],'file','extensions'=>'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio_ru' => 'Имя Фамилия Ru',
            'fio_en' => 'Имя Фамилия En',
            'fio_fr' => 'Имя Фамилия Fr',
            'fio_it' => 'Имя Фамилия It',
            'fio_de' => 'Имя Фамилия De',
            'position_ru' => 'Должность Ru',
            'position_en' => 'Должность En',
            'position_fr' => 'Должность Fr',
            'position_it' => 'Должность It',
            'position_de' => 'Должность De',
            'image' => 'Фотография (квадратное)',
            'file' => 'Фотография (квадратное)',
            'fileUpdate' => 'Фотография (квадратное)',
            'email' => 'Эл.почта',
        ];
    }
}

<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "tour_calendar".
 *
 * @property int $id
 * @property int $tour_id
 * @property int $calendar_id
 * @property int $value
 */
class TourCalendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour_calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'calendar_id', 'value'], 'required'],
            [['tour_id', 'calendar_id', 'value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Тур',
            'calendar_id' => 'Месяц',
            'value' => 'Значение',
        ];
    }
}

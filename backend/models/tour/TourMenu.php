<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "tour_menu".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class TourMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour_menu';
    }

    public $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image'], 'string', 'max' => 255],
            [['fileUpdate'], 'file', 'extensions' => 'png,jpeg,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображение (квадрат)',
            'fileUpdate' => 'Изображение (квадрат)',
        ];
    }
}

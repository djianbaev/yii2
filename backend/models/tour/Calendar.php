<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de'], 'required'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименования Ru',
            'name_en' => 'Наименования En',
            'name_fr' => 'Наименования Fr',
            'name_it' => 'Наименования It',
            'name_de' => 'Наименования De',
        ];
    }
}

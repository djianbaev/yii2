<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "tour".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $descr_ru
 * @property string $descr_en
 * @property string $descr_fr
 * @property string $descr_it
 * @property string $descr_de
 * @property int $price
 * @property string $days_night_ru
 * @property string $days_night_en
 * @property string $days_night_fr
 * @property string $days_night_it
 * @property string $days_night_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $include_ru
 * @property string $include_en
 * @property string $include_fr
 * @property string $include_it
 * @property string $include_de
 * @property string $image
 * @property string $slug
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'descr_ru', 'descr_en', 'descr_fr', 'descr_it', 'descr_de', 'price', 'days_night_ru', 'days_night_en', 'days_night_fr', 'days_night_it', 'days_night_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'include_ru', 'include_en', 'include_fr', 'include_it', 'include_de', 'image', 'slug'], 'required'],
            [['descr_ru', 'descr_en', 'descr_fr', 'descr_it', 'descr_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'include_ru', 'include_en', 'include_fr', 'include_it', 'include_de'], 'string'],
            [['price'], 'integer'],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'jpg,png,jpeg'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'days_night_ru', 'days_night_en', 'days_night_fr', 'days_night_it', 'days_night_de', 'image', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'descr_ru' => 'Описание Ru',
            'descr_en' => 'Описание En',
            'descr_fr' => 'Описание Fr',
            'descr_it' => 'Описание It',
            'descr_de' => 'Описание De',
            'price' => 'Цена',
            'days_night_ru' => 'Дни и Ночи Ru',
            'days_night_en' => 'Дни и Ночи En',
            'days_night_fr' => 'Дни и Ночи Fr',
            'days_night_it' => 'Дни и Ночи It',
            'days_night_de' => 'Дни и Ночи De',
            'text_ru' => 'Подробное описание Ru',
            'text_en' => 'Подробное описание En',
            'text_fr' => 'Подробное описание Fr',
            'text_it' => 'Подробное описание It',
            'text_de' => 'Подробное описание De',
            'include_ru' => 'Включено Ru',
            'include_en' => 'Включено En',
            'include_fr' => 'Включено Fr',
            'include_it' => 'Включено It',
            'include_de' => 'Включено De',
            'image' => 'Изображение (16х9)',
            'file' => 'Изображение (16х9)',
            'fileUpdate' => 'Изображение (16х9)',
            'slug' => 'Слаг',
        ];
    }
}

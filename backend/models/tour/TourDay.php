<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "tour_day".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 */
class TourDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'required'],
            [['tour_id'], 'integer'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tour ID',
            'name_ru' => 'Название Ru',
            'name_en' => 'Название En',
            'name_fr' => 'Название Fr',
            'name_it' => 'Название It',
            'name_de' => 'Название De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
        ];
    }
}

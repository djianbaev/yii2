<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "service_item".
 *
 * @property int $id
 * @property int $service_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $intro_ru
 * @property string $intro_en
 * @property string $intro_fr
 * @property string $intro_it
 * @property string $intro_de
 * @property string $text_en
 * @property string $text_ru
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property int $price
 * @property string $duration
 * @property string $location
 * @property string $date
 * @property string $image
 */
class ServiceItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_item';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file', 'service_id','name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'intro_ru', 'intro_en', 'intro_fr', 'intro_it', 'intro_de', 'text_en', 'text_ru', 'text_fr', 'text_it', 'text_de', 'price', 'duration', 'location', 'date', 'image'], 'required'],
            [['intro_ru', 'intro_en', 'intro_fr', 'intro_it', 'intro_de', 'text_en', 'text_ru', 'text_fr', 'text_it', 'text_de', 'date'], 'string'],
            [['price','service_id'], 'integer'],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'png,jpg,jpeg'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'duration', 'location', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id'=>'Сервис',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'intro_ru' => 'Краткая информация Ru',
            'intro_en' => 'Краткая информация En',
            'intro_fr' => 'Краткая информация Fr',
            'intro_it' => 'Краткая информация It',
            'intro_de' => 'Краткая информация De',
            'text_en' => 'Текст En',
            'text_ru' => 'Текст Ru',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'price' => 'Цена',
            'duration' => 'Продолжительность',
            'location' => 'Местопровидение',
            'date' => 'Числа(Даты)',
            'image' => 'Изображение (16х9)',
            'file' => 'Изображение (16х9)',
            'fileUpdate' => 'Изображение (16х9)',
        ];
    }
}

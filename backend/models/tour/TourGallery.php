<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "tour_gallery".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $image
 */
class TourGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tour_gallery';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tour_id', 'image'], 'required'],
            [['tour_id'], 'integer'],
            [['file'],'file','extensions'=>'png,jpg,jpeg','maxFiles'=>4],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Тур',
            'image' => 'Изображение',
            'file' => 'Изображение',
        ];
    }
}

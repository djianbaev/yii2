<?php

namespace backend\models\tour;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\tour\ServiceItem;

/**
 * ServiceItemSearch represents the model behind the search form of `backend\models\tour\ServiceItem`.
 */
class ServiceItemSearch extends ServiceItem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price'], 'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'intro_ru', 'intro_en', 'intro_fr', 'intro_it', 'intro_de', 'text_en', 'text_ru', 'text_fr', 'text_it', 'text_de', 'duration', 'location', 'date', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServiceItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_fr', $this->name_fr])
            ->andFilterWhere(['like', 'name_it', $this->name_it])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'intro_ru', $this->intro_ru])
            ->andFilterWhere(['like', 'intro_en', $this->intro_en])
            ->andFilterWhere(['like', 'intro_fr', $this->intro_fr])
            ->andFilterWhere(['like', 'intro_it', $this->intro_it])
            ->andFilterWhere(['like', 'intro_de', $this->intro_de])
            ->andFilterWhere(['like', 'text_en', $this->text_en])
            ->andFilterWhere(['like', 'text_ru', $this->text_ru])
            ->andFilterWhere(['like', 'text_fr', $this->text_fr])
            ->andFilterWhere(['like', 'text_it', $this->text_it])
            ->andFilterWhere(['like', 'text_de', $this->text_de])
            ->andFilterWhere(['like', 'duration', $this->duration])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}

<?php

namespace backend\models\tour;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\tour\Tour;

/**
 * TourSearch represents the model behind the search form of `backend\models\tour\Tour`.
 */
class TourSearch extends Tour
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price'], 'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'descr_ru', 'descr_en', 'descr_fr', 'descr_it', 'descr_de', 'days_night_ru', 'days_night_en', 'days_night_fr', 'days_night_it', 'days_night_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'include_ru', 'include_en', 'include_fr', 'include_it', 'include_de', 'image', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tour::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_fr', $this->name_fr])
            ->andFilterWhere(['like', 'name_it', $this->name_it])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'descr_ru', $this->descr_ru])
            ->andFilterWhere(['like', 'descr_en', $this->descr_en])
            ->andFilterWhere(['like', 'descr_fr', $this->descr_fr])
            ->andFilterWhere(['like', 'descr_it', $this->descr_it])
            ->andFilterWhere(['like', 'descr_de', $this->descr_de])
            ->andFilterWhere(['like', 'days_night_ru', $this->days_night_ru])
            ->andFilterWhere(['like', 'days_night_en', $this->days_night_en])
            ->andFilterWhere(['like', 'days_night_fr', $this->days_night_fr])
            ->andFilterWhere(['like', 'days_night_it', $this->days_night_it])
            ->andFilterWhere(['like', 'days_night_de', $this->days_night_de])
            ->andFilterWhere(['like', 'text_ru', $this->text_ru])
            ->andFilterWhere(['like', 'text_en', $this->text_en])
            ->andFilterWhere(['like', 'text_fr', $this->text_fr])
            ->andFilterWhere(['like', 'text_it', $this->text_it])
            ->andFilterWhere(['like', 'text_de', $this->text_de])
            ->andFilterWhere(['like', 'include_ru', $this->include_ru])
            ->andFilterWhere(['like', 'include_en', $this->include_en])
            ->andFilterWhere(['like', 'include_fr', $this->include_fr])
            ->andFilterWhere(['like', 'include_it', $this->include_it])
            ->andFilterWhere(['like', 'include_de', $this->include_de])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}

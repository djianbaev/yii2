<?php

namespace backend\models\tour;

use Yii;

/**
 * This is the model class for table "service_gallery".
 *
 * @property int $id
 * @property int $service_item_id
 * @property string $image
 */
class ServiceGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_gallery';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_item_id', 'image'], 'required'],
            [['service_item_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_item_id' => 'Service Item ID',
            'image' => 'Image',
        ];
    }
}

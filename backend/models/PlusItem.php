<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plus_item".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class PlusItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plus_item';
    }

    public $file,$fileUpdate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image'], 'string', 'max' => 255],
            [['file','fileUpdate'],'file','extensions'=>'png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименования Ru',
            'name_en' => 'Наименования En',
            'name_fr' => 'Наименования Fr',
            'name_it' => 'Наименования It',
            'name_de' => 'Наименования De',
            'text_ru' => 'Текст Ru',
            'text_en' => 'Текст En',
            'text_fr' => 'Текст Fr',
            'text_it' => 'Текст It',
            'text_de' => 'Текст De',
            'image' => 'Изображения(512x512 png)',
            'fileUpdate' => 'Изображения(512x512 png)',
            'file' => 'Изображения(512x512 png)',
        ];
    }
}

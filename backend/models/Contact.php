<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $address_ru
 * @property string $address_en
 * @property string $address_fr
 * @property string $address_it
 * @property string $address_de
 * @property string $phone
 * @property string $email
 * @property string $work_ru
 * @property string $work_en
 * @property string $work_fr
 * @property string $work_it
 * @property string $work_de
 * @property string $banner
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'],'file','extensions'=>'png,jpg,jpeg'],
            [['address_ru', 'address_en', 'address_fr', 'address_it', 'address_de', 'phone', 'email', 'work_ru', 'work_en', 'work_fr', 'work_it', 'work_de', 'banner'], 'required'],
            [['address_ru', 'address_en', 'address_fr', 'address_it', 'address_de', 'phone', 'email', 'work_ru', 'work_en', 'work_fr', 'work_it', 'work_de', 'banner'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_ru' => 'Адрес Ru',
            'address_en' => 'Адрес En',
            'address_fr' => 'Адрес Fr',
            'address_it' => 'Адрес It',
            'address_de' => 'Адрес De',
            'phone' => 'Телефоны',
            'email' => 'Эл.почты',
            'work_ru' => 'Рабочие Часы Ru',
            'work_en' => 'Рабочие Часы En',
            'work_fr' => 'Рабочие Часы Fr',
            'work_it' => 'Рабочие Часы It',
            'work_de' => 'Рабочие Часы De',
            'banner' => 'Баннер (1920х380)',
            'file' => 'Баннер (1920х380)',
        ];
    }
}

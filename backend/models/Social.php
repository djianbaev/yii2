<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "social".
 *
 * @property int $id
 * @property string $telegram
 * @property string $instagram
 * @property string $facebook
 * @property string $youtube
 * @property string $whatsapp
 * @property string $twitter
 * @property string $pinterest
 * @property string $phone
 * @property string $email
 * @property string $address_ru
 * @property string $address_en
 * @property string $address_fr
 * @property string $address_it
 * @property string $address_de
 * @property string $about_ru
 * @property string $about_en
 * @property string $about_fr
 * @property string $about_it
 * @property string $about_de
 */
class Social extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telegram', 'instagram', 'facebook', 'youtube', 'whatsapp', 'twitter', 'pinterest', 'phone', 'email', 'address_ru', 'address_en', 'address_fr', 'address_it', 'address_de', 'about_ru', 'about_en', 'about_fr', 'about_it', 'about_de'], 'required'],
            [['about_ru', 'about_en', 'about_fr', 'about_it', 'about_de'], 'string'],
            [['telegram', 'instagram', 'facebook', 'youtube', 'whatsapp', 'twitter', 'pinterest', 'phone', 'email', 'address_ru', 'address_en', 'address_fr', 'address_it', 'address_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telegram' => 'Telegram',
            'instagram' => 'Instagram',
            'facebook' => 'Facebook',
            'youtube' => 'Youtube',
            'whatsapp' => 'Whatsapp',
            'twitter' => 'Twitter',
            'pinterest' => 'Pinterest',
            'phone' => 'Телефон',
            'email' => 'Эл.почта',
            'address_ru' => 'Адрес Ru',
            'address_en' => 'Адрес En',
            'address_fr' => 'Адрес Fr',
            'address_it' => 'Адрес It',
            'address_de' => 'Адрес De',
            'about_ru' => 'О Нас Ru',
            'about_en' => 'О Нас En',
            'about_fr' => 'О Нас Fr',
            'about_it' => 'О Нас It',
            'about_de' => 'О Нас De',
        ];
    }
}

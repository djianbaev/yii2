<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "footer".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de'], 'required'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Названия Ru',
            'name_en' => 'Названия En',
            'name_fr' => 'Названия Fr',
            'name_it' => 'Названия It',
            'name_de' => 'Названия De',
        ];
    }
}

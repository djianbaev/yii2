<?php

namespace backend\models\review;

use Yii;

/**
 * This is the model class for table "review_index".
 *
 * @property int $id
 * @property int $review_tour_id
 * @property int $review_service_id
 */
class ReviewIndex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review_index';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['review_tour_id', 'review_service_id'], 'required'],
            [['review_tour_id', 'review_service_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'review_tour_id' => 'Тур',
            'review_service_id' => 'Сервис',
        ];
    }
}

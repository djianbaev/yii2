<?php

namespace backend\models\review;

use Yii;

/**
 * This is the model class for table "review_service".
 *
 * @property int $id
 * @property int $service_item_id
 * @property string $fullname
 * @property string $email
 * @property string $text
 * @property string $image
 * @property int $status
 * @property string $created_date
 */
class ReviewService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_item_id', 'fullname', 'email', 'text', 'image', 'status', 'created_date'], 'required'],
            [['service_item_id', 'status'], 'integer'],
            [['text'], 'string'],
            [['fullname', 'email', 'image', 'created_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_item_id' => 'Сервис ',
            'fullname' => 'Полное имя',
            'email' => 'Email',
            'text' => 'Отзыв',
            'image' => 'Фото',
            'status' => 'Статус',
            'created_date' => 'Дата добавления',
        ];
    }
}

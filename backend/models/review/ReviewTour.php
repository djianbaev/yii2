<?php

namespace backend\models\review;

use Yii;

/**
 * This is the model class for table "review_tour".
 *
 * @property int $id
 * @property int $tour_id
 * @property string $fullname
 * @property string $email
 * @property string $text
 * @property string $image
 * @property int $status
 * @property string $created_date
 */
class ReviewTour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review_tour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'email', 'text', 'image', 'status', 'created_date','tour_id'], 'required'],
            [['text'], 'string'],
            [['status','tour_id'], 'integer'],
            [['fullname', 'email', 'image', 'created_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'ID Тура',
            'fullname' => 'Полное имя',
            'email' => 'Эл.почта',
            'text' => 'Отзыв',
            'image' => 'Фото',
            'status' => 'Статус',
            'created_date' => 'Дата добавления',
        ];
    }
}

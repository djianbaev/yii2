<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "footer_item".
 *
 * @property int $id
 * @property int $footer_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class FooterItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'footer_item';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['footer_id', 'file', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['footer_id'], 'integer'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image'], 'string', 'max' => 255],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'jpg,jpeg,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'footer_id' => 'Footer ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'text_ru' => 'Содержимое Ru',
            'text_en' => 'Содержимое En',
            'text_fr' => 'Содержимое Fr',
            'text_it' => 'Содержимое It',
            'text_de' => 'Содержимое De',
            'image' => 'Изображение (16х9)',
            'file' => 'Изображение (16х9)',
            'fileUpdate' => 'Изображение (16х9)',
        ];
    }
}

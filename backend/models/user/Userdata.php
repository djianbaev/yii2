<?php

namespace backend\models\user;

use Yii;

/**
 * This is the model class for table "userdata".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $country
 * @property string $city
 * @property string $phone
 * @property string $bday
 */
class Userdata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userdata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'surname', 'country', 'city', 'phone', 'bday'], 'required'],
            [['user_id'], 'integer'],
            [['name', 'surname', 'country', 'city', 'phone', 'bday'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'country' => 'Country',
            'city' => 'City',
            'phone' => 'Phone',
            'bday' => 'Bday',
        ];
    }
}

<?php

namespace backend\models\user;

use Yii;

/**
 * This is the model class for table "userbook".
 *
 * @property int $id
 * @property int $user_id
 * @property string $booking_ru
 * @property string $created_date
 * @property string $booking_en
 * @property string $booking_fr
 * @property string $booking_it
 * @property string $booking_de
 */
class Userbook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userbook';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'booking_ru', 'created_date', 'booking_en', 'booking_fr', 'booking_it', 'booking_de'], 'required'],
            [['user_id'], 'integer'],
            [['booking_ru', 'created_date', 'booking_en', 'booking_fr', 'booking_it', 'booking_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'booking_ru' => 'Booking Ru',
            'created_date' => 'Created Date',
            'booking_en' => 'Booking En',
            'booking_fr' => 'Booking Fr',
            'booking_it' => 'Booking It',
            'booking_de' => 'Booking De',
        ];
    }
}

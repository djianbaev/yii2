<?php

namespace backend\models\city;

use Yii;

/**
 * This is the model class for table "guide_menu".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property string $slug
 * @property int $number
 * @property int $status
 */
class GuideMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guide_menu';
    }

    public $file,$fileUpdate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'slug', 'number', 'status'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['number', 'status'], 'integer'],
            [['file','fileUpdate'],'file','extensions'=>'png,jpg,jpeg'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'text_ru' => 'Короткий текст Ru',
            'text_en' => 'Короткий текст En',
            'text_fr' => 'Короткий текст Fr',
            'text_it' => 'Короткий текст It',
            'text_de' => 'Короткий текст De',
            'image' => 'Изображения (квадрат)',
            'file' => 'Изображения (квадрат)',
            'fileUpdate' => 'Изображения (квадрат)',
            'slug' => 'Слаг',
            'number' => 'Порядковый номер',
            'status' => 'Статус',
        ];
    }
}

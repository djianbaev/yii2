<?php

namespace backend\models\city;

use Yii;

/**
 * This is the model class for table "guide_gallery".
 *
 * @property int $id
 * @property int $guide_item_id
 * @property string $image
 */
class GuideGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guide_gallery';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['guide_item_id', 'image'], 'required'],
            [['guide_item_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guide_item_id' => 'Guide Item ID',
            'image' => 'Image',
            'file' => 'Изображение',
        ];
    }
}

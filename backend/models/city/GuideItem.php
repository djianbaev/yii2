<?php

namespace backend\models\city;

use Yii;

/**
 * This is the model class for table "guide_item".
 *
 * @property int $id
 * @property int $guide_menu_id
 * @property int $city_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property int $status
 * @property int $top
 * @property string $slug
 */
class GuideItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guide_item';
    }

    public $file,$fileUpdate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','guide_menu_id', 'city_id', 'name_ru', 'name_en','slug', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'status', 'top'], 'required'],
            [['guide_menu_id', 'city_id', 'status', 'top'], 'integer'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image','slug'], 'string', 'max' => 255],
            [['file','fileUpdate'],'file','extensions'=>'jpg,png,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guide_menu_id' => 'Подкатегория',
            'city_id' => 'Город',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'НаименованиеDe',
            'text_ru' => 'Описание Ru',
            'text_en' => 'Описание En',
            'text_fr' => 'Описание Fr',
            'text_it' => 'Описание It',
            'text_de' => 'Описание De',
            'image' => 'Изображение (16х9)',
            'file' => 'Изображение (16х9)',
            'fileUpdate' => 'Изображение (16х9)',
            'status' => 'Статус',
            'top' => 'В топ списка',
            'slug' => 'Слаг',
        ];
    }
}

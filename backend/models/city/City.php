<?php

namespace backend\models\city;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property string $map
 * @property int $status
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    public $f_image, $f_image_update;
    public $f_map, $f_map_update;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_image', 'f_map', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'map', 'status'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['status'], 'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image', 'map'], 'string', 'max' => 255],
            [['f_image', 'f_image_update'], 'file', 'extensions' => 'jpg,jpeg,png'],
            [['f_map', 'f_map_update'], 'file', 'extensions' => 'png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'text_ru' => 'Описание Ru',
            'text_en' => 'Описание En',
            'text_fr' => 'Описание Fr',
            'text_it' => 'Описание It',
            'text_de' => 'Описание De',
            'image' => 'Изображение (16х9 вертикальная)',
            'f_image' => 'Изображение (16х9 вертикальная)',
            'f_image_update' => 'Изображение (16х9 вертикальная)',
            'map' => 'Карта',
            'f_map' => 'Карта',
            'f_map_update' => 'Карта',
            'status' => 'Статус активности',
        ];
    }
}

<?php

namespace backend\models\city;

use Yii;

/**
 * This is the model class for table "city_gallery".
 *
 * @property int $id
 * @property int $city_id
 * @property string $image
 */
class CityGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_gallery';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'image'], 'required'],
            [['city_id'], 'integer'],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg','maxFiles'=>4],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'image' => 'Image',
        ];
    }
}

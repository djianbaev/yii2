<?php

namespace backend\models\city;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\city\GuideMenu;

/**
 * GuideMenuSearch represents the model behind the search form of `backend\models\city\GuideMenu`.
 */
class GuideMenuSearch extends GuideMenu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'number', 'status'], 'integer'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuideMenu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_fr', $this->name_fr])
            ->andFilterWhere(['like', 'name_it', $this->name_it])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'text_ru', $this->text_ru])
            ->andFilterWhere(['like', 'text_en', $this->text_en])
            ->andFilterWhere(['like', 'text_fr', $this->text_fr])
            ->andFilterWhere(['like', 'text_it', $this->text_it])
            ->andFilterWhere(['like', 'text_de', $this->text_de])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}

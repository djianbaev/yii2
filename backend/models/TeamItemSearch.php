<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TeamItem;

/**
 * TeamItemSearch represents the model behind the search form of `backend\models\TeamItem`.
 */
class TeamItemSearch extends TeamItem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fio_ru', 'fio_en', 'fio_fr', 'fio_it', 'fio_de', 'position_ru', 'position_en', 'position_fr', 'position_it', 'position_de', 'image', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'fio_ru', $this->fio_ru])
            ->andFilterWhere(['like', 'fio_en', $this->fio_en])
            ->andFilterWhere(['like', 'fio_fr', $this->fio_fr])
            ->andFilterWhere(['like', 'fio_it', $this->fio_it])
            ->andFilterWhere(['like', 'fio_de', $this->fio_de])
            ->andFilterWhere(['like', 'position_ru', $this->position_ru])
            ->andFilterWhere(['like', 'position_en', $this->position_en])
            ->andFilterWhere(['like', 'position_fr', $this->position_fr])
            ->andFilterWhere(['like', 'position_it', $this->position_it])
            ->andFilterWhere(['like', 'position_de', $this->position_de])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}

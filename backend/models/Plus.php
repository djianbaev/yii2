<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plus".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 */
class Plus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'title_fr' => 'Title Fr',
            'title_it' => 'Title It',
            'title_de' => 'Title De',
            'text_ru' => 'Text Ru',
            'text_en' => 'Text En',
            'text_fr' => 'Text Fr',
            'text_it' => 'Text It',
            'text_de' => 'Text De',
        ];
    }
}

<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "moments_item".
 *
 * @property int $id
 * @property int $moment_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 * @property int $price
 * @property string $duration
 * @property string $location
 * @property string $slug
 */
class MomentsItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moments_item';
    }

    public $file, $fileUpdate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file','moment_id', 'name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image', 'price', 'duration', 'location', 'slug'], 'required'],
            [['moment_id', 'price'], 'integer'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image', 'duration', 'location', 'slug'], 'string', 'max' => 255],
            [['file', 'fileUpdate'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'moment_id' => 'Moment ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'text_ru' => 'Описание Ru',
            'text_en' => 'Описание En',
            'text_fr' => 'Описание Fr',
            'text_it' => 'Описание It',
            'text_de' => 'Описание De',
            'image' => 'Изображение( 16х9 )',
            'fileUpdate' => 'Изображение( 16х9 )',
            'file' => 'Изображение( 16х9 )',
            'price' => 'Цена',
            'duration' => 'Длительность',
            'location' => 'Местопровидения',
            'slug' => 'Slug',
        ];
    }
}

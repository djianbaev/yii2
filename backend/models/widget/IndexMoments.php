<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "index_moments".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 * @property string $image
 */
class IndexMoments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_moments';
    }

    public $fileUpdate;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de', 'image'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'image'], 'string', 'max' => 255],
            [['fileUpdate'],'file','extensions'=>'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Название Ru',
            'name_en' => 'Название En',
            'name_fr' => 'Название Fr',
            'name_it' => 'Название It',
            'name_de' => 'Название De',
            'text_ru' => 'Короткий Текст Ru',
            'text_en' => 'Короткий Текст En',
            'text_fr' => 'Короткий Текст Fr',
            'text_it' => 'Короткий Текст It',
            'text_de' => 'Короткий Текст De',
            'image' => 'Изображение(квадратное)',
            'fileUpdate' => 'Изображение(квадратное)',
        ];
    }
}

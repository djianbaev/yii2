<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "index_slider".
 *
 * @property int $id
 * @property string $image
 */
class IndexSlider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_slider';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение(1920x840)',
            'file' => 'Изображение(1920x840)',
        ];
    }
}

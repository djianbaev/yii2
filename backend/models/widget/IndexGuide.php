<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "index_guide".
 *
 * @property int $id
 * @property string $quote_ru
 * @property string $quote_en
 * @property string $quote_fr
 * @property string $quote_it
 * @property string $quote_de
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $button_ru
 * @property string $button_en
 * @property string $button_fr
 * @property string $button_it
 * @property string $button_de
 * @property string $image
 */
class IndexGuide extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_guide';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_ru', 'quote_en', 'quote_fr', 'quote_it', 'quote_de', 'title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'button_ru', 'button_en', 'button_fr', 'button_it', 'button_de', 'image'], 'required'],
            [['quote_ru', 'quote_en', 'quote_fr', 'quote_it', 'quote_de'], 'string'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'button_ru', 'button_en', 'button_fr', 'button_it', 'button_de', 'image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quote_ru' => 'Цитаты Ru',
            'quote_en' => 'Цитаты En',
            'quote_fr' => 'Цитаты Fr',
            'quote_it' => 'Цитаты It',
            'quote_de' => 'Цитаты De',
            'title_ru' => 'Заголовок Ru',
            'title_en' => 'Заголовок En',
            'title_fr' => 'Заголовок Fr',
            'title_it' => 'Заголовок It',
            'title_de' => 'Заголовок De',
            'button_ru' => 'Кнопка Ru',
            'button_en' => 'Кнопка En',
            'button_fr' => 'Кнопка Fr',
            'button_it' => 'Кнопка It',
            'button_de' => 'Кнопка De',
            'image' => 'Фон изоб-е (1920х1200)',
            'file' => 'Фон изоб-е (1920х1200)',
        ];
    }
}

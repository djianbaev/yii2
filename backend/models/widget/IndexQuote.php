<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "index_quote".
 *
 * @property int $id
 * @property string $author_ru
 * @property string $author_en
 * @property string $author_fr
 * @property string $author_it
 * @property string $author_de
 * @property string $text_ru
 * @property string $text_en
 * @property string $text_fr
 * @property string $text_it
 * @property string $text_de
 */
class IndexQuote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_quote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_ru', 'author_en', 'author_fr', 'author_it', 'author_de', 'text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'required'],
            [['text_ru', 'text_en', 'text_fr', 'text_it', 'text_de'], 'string'],
            [['author_ru', 'author_en', 'author_fr', 'author_it', 'author_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_ru' => 'Author Ru',
            'author_en' => 'Author En',
            'author_fr' => 'Author Fr',
            'author_it' => 'Author It',
            'author_de' => 'Author De',
            'text_ru' => 'Text Ru',
            'text_en' => 'Text En',
            'text_fr' => 'Text Fr',
            'text_it' => 'Text It',
            'text_de' => 'Text De',
        ];
    }
}

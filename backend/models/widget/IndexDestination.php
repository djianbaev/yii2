<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "index_destination".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_it
 * @property string $title_de
 * @property string $sub_title_ru
 * @property string $sub_title_en
 * @property string $sub_title_fr
 * @property string $sub_title_it
 * @property string $sub_title_de
 */
class IndexDestination extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'index_destination';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'sub_title_ru', 'sub_title_en', 'sub_title_fr', 'sub_title_it', 'sub_title_de'], 'required'],
            [['title_ru', 'title_en', 'title_fr', 'title_it', 'title_de', 'sub_title_ru', 'sub_title_en', 'sub_title_fr', 'sub_title_it', 'sub_title_de'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Заголовок Ru',
            'title_en' => 'Заголовок En',
            'title_fr' => 'Заголовок Fr',
            'title_it' => 'Заголовок It',
            'title_de' => 'Заголовок De',
            'sub_title_ru' => 'Пред Заголовок Ru',
            'sub_title_en' => 'Пред Заголовок En',
            'sub_title_fr' => 'Пред Заголовок Fr',
            'sub_title_it' => 'Пред Заголовок It',
            'sub_title_de' => 'Пред Заголовок De',
        ];
    }
}

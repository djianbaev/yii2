<?php

namespace backend\models\widget;

use Yii;

/**
 * This is the model class for table "moments_gallery".
 *
 * @property int $id
 * @property int $moment_item_id
 * @property string $image
 */
class MomentsGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moments_gallery';
    }
    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moment_item_id', 'image'], 'required'],
            [['moment_item_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png,jpg,jpeg', 'maxFiles' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'moment_item_id' => 'Moment Item ID',
            'image' => 'Image',
        ];
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_it
 * @property string $name_de
 * @property int $number
 * @property string $action
 * @property int $status
 * @property string $slug
 * @property string $banner
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'number', 'action', 'status', 'slug', 'banner'], 'required'],
            [['number', 'status'], 'integer'],
            [['file'], 'file', 'extensions' => 'jpeg,jpg,png'],
            [['name_ru', 'name_en', 'name_fr', 'name_it', 'name_de', 'action', 'slug', 'banner'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Наименование Ru',
            'name_en' => 'Наименование En',
            'name_fr' => 'Наименование Fr',
            'name_it' => 'Наименование It',
            'name_de' => 'Наименование De',
            'number' => 'Порядковый номер',
            'action' => 'Экшн',
            'status' => 'Статус',
            'slug' => 'Слаг',
            'banner' => 'Баннер (1920х380)',
            'file' => 'Баннер (1920х380)',
        ];
    }
}

<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru',
    'sourceLanguage' => 'ru_RU',
    'modules' => [],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl'=>'@frontend',
                'path' => 'ckeditor/images',
                'name' => 'Images'
            ],
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/adminpanel'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
//        'view' => [
//            'theme' => [
//                'pathMap' => [
//                    '@backend/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
//                ],
//            ],
//        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'auth' => 'site/login',
                'ckeditor/upload' => 'about/upload',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                'city/<controller:\w+>/<action:\w+>/<id:\d+>' => 'city/<controller>/<action>',
                'info/<controller:\w+>/<action:\w+>/<id:\d+>' => 'info/<controller>/<action>',
                'tour/<controller:\w+>/<action:\w+>/<id:\d+>' => 'tour/<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];

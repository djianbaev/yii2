<?php

namespace backend\controllers\tour;

use backend\models\tour\Calendar;
use backend\models\tour\TourCalendar;
use backend\models\tour\TourDay;
use backend\models\tour\TourGallery;
use Yii;
use backend\models\tour\Tour;
use backend\models\tour\TourSearch;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'create', 'delete',
                            'gallery', 'gallerydelete',
                            'cals',
                            'days', 'daycreate', 'dayupdate', 'daydelete', 'dayview'
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tour model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tour();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/tour/');
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($dir . $img_name . '.' . $model->file->extension);
            $model->image = '/res/images/tour/' . $img_name . '.' . $model->file->extension;
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@frontend' . '/web/res/images/tour/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            if (UploadedFile::getInstance($model, 'fileUpdate')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/tour/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/tour/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                    $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                    $model->image = '/res/images/tour/' . $img_name . '.' . $model->fileUpdate->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /*** calendar ***/

    public function actionCals($id)
    {
        $model = $this->findModel($id);
        $rowC = Calendar::find()->asArray()->all();

        $query = new Query();
        $query->select([
            'c.id as cal_id',
            'c.name_ru as name_ru',
            'tc.value as value',
            'tc.id as tour_cal_id',
        ])->from(['c' => 'calendar'])
            ->leftJoin(['tc' => 'tour_calendar'], 'tc.calendar_id=c.id')
            ->where(['tc.tour_id' => $id])
            ->all();
        $command = $query->createCommand();
        $rowD = $command->queryAll();

        if (Yii::$app->request->post()) {
            if (empty($rowD)) {
                foreach ($rowC as $item) {
                    $db = new TourCalendar();
                    $db->tour_id = $id;
                    $db->calendar_id = $item['id'];
                    $db->value = Yii::$app->request->post('status_' . $item['id']);
                    $db->save(false);
                }
                return $this->refresh();
            } else {
                foreach ($rowD as $item) {
                    $db = TourCalendar::find()->where(['id' => $item['cal_id']])->one();
                    $db->value = Yii::$app->request->post('status_' . $item['cal_id']);
                    $db->save(false);
                }
                return $this->refresh();
            }
        }


        return $this->render('cals', [
            'model' => $model,
            'rowC' => $rowC,
            'rowD' => $rowD
        ]);
    }


    /*** days ***/

    public function actionDays($id)
    {
        $model = $this->findModel($id);
        $days = TourDay::find()->where(['tour_id' => $id])->asArray()->all();
        return $this->render('days', [
            'model' => $model,
            'days' => $days
        ]);
    }

    public function actionDaycreate($id)
    {
        $model = $this->findModel($id);
        $day_model = new TourDay();
        if ($day_model->load(Yii::$app->request->post())) {
            $day_model->tour_id = $id;
            $day_model->save(false);
            return $this->redirect(['dayview', 'id' => $id, 'did' => $day_model->id]);
        }
        return $this->render('day_create', [
            'model' => $model,
            'day_model' => $day_model
        ]);
    }

    public function actionDayview($id, $did)
    {
        $model = $this->findModel($id);
        $day_model = TourDay::findOne(['id' => $did]);

        return $this->render('day_view', [
            'model' => $model,
            'day_model' => $day_model
        ]);
    }


    public function actionDayupdate($id, $did)
    {
        $model = $this->findModel($id);
        $day_model = TourDay::findOne(['id' => $did]);
        if ($day_model->load(Yii::$app->request->post())) {
            $day_model->save(false);
            return $this->redirect(['dayview', 'id' => $id, 'did' => $day_model->id]);
        }
        return $this->render('day_update', [
            'model' => $model,
            'day_model' => $day_model
        ]);
    }

    public function actionDaydelete($id, $did)
    {
        $day = TourDay::deleteAll(['id' => $did]);

        return $this->redirect(['days', 'id' => $id]);
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $gallery = new TourGallery();
        $db_images = TourGallery::find()->where(['tour_id' => $id])->all();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/tour/');
        if ($gallery->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $gallery->file = UploadedFile::getInstances($gallery, 'file');
            $i = 0;
            foreach ($gallery->file as $file) {
                $i++;
                $file->saveAs($dir . $img_name . '-' . $i . '.' . $file->extension);
                $db_new = new TourGallery();
                $db_new->tour_id = $id;
                $db_new->image = '/res/images/tour/' . $img_name . '-' . $i . '.' . $file->extension;
                $db_new->save(false);
            }
            return $this->refresh();
        }
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'db_images' => $db_images
        ]);
    }

    public function actionGallerydelete($id, $gid)
    {
        $image = TourGallery::findOne(['id' => $gid]);
        if ($image) {
            if (Yii::getAlias('@frontend' . '/web' . $image->image)) {
                try {
                    unlink(Yii::getAlias('@frontend' . '/web' . $image->image));
                    if ($image->delete()) {
                        return $this->redirect(['gallery', 'id' => $id]);
                    }

                } catch (\Exception $e) {
                    if ($image->delete()) {
                        return $this->redirect(['gallery', 'id' => $id]);
                    }
                }
            } else {
                if ($image->delete()) {
                    return $this->redirect(['gallery', 'id' => $id]);
                }
            }
        } else {
            return $this->redirect(['gallery', 'id' => $id]);
        }
    }
}

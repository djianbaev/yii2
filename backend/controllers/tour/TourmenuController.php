<?php

namespace backend\controllers\tour;

use Yii;
use backend\models\tour\TourMenu;
use backend\models\tour\TourMenuSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TourmenuController implements the CRUD actions for TourMenu model.
 */
class TourmenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TourMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->findModel(1);

        return $this->render('index', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing TourMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel(1);
        $dir = Yii::getAlias('@frontend' . '/web/res/images/tour/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            if(UploadedFile::getInstance($model,'fileUpdate')){
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                        $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                        $model->image = '/res/images/tour/'.$img_name.'.'.$model->fileUpdate->extension;
                        if($model->save(false)){
                            return $this->redirect(['index']);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                        $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                        $model->image = '/res/images/tour/'.$img_name.'.'.$model->fileUpdate->extension;
                        if($model->save(false)){
                            return $this->redirect(['index']);
                        }
                    }
                }else{
                    $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                    $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                    $model->image = '/res/images/tour/'.$img_name.'.'.$model->fileUpdate->extension;
                    if($model->save(false)){
                        return $this->redirect(['index']);
                    }
                }
            }else{
                if($model->save(false)){
                    return $this->redirect(['index']);
                }
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the TourMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TourMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TourMenu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

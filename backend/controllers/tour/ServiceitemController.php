<?php

namespace backend\controllers\tour;

use backend\models\tour\Service;
use backend\models\tour\ServiceGallery;
use Yii;
use backend\models\tour\ServiceItem;
use backend\models\tour\ServiceItemSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ServiceitemController implements the CRUD actions for ServiceItem model.
 */
class ServiceitemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view','create','delete','gallery','gallerydelete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServiceItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ServiceItem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServiceItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServiceItem();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/service/');
        $services = Service::find()->all();
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($dir . $img_name . '.' . $model->file->extension);
            $model->image = '/res/images/service/' . $img_name . '.' . $model->file->extension;
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'services'=>$services
        ]);
    }

    /**
     * Updates an existing ServiceItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $services = Service::find()->all();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/service/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            if (UploadedFile::getInstance($model, 'fileUpdate')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/service/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/service/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                    $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                    $model->image = '/res/images/service/' . $img_name . '.' . $model->fileUpdate->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'services'=>$services
        ]);
    }

    /**
     * Deletes an existing ServiceItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ServiceItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServiceItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $gallery = new ServiceGallery();
        $db_images = ServiceGallery::find()->where(['service_item_id' => $id])->orderBy(['id' => SORT_DESC])->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/service/');
        if ($gallery->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $gallery->file = UploadedFile::getInstances($gallery, 'file');
            $i = 0;
            foreach ($gallery->file as $file) {
                $i++;
                $file->saveAs($dir . $img_name . '-' . $i . '.' . $file->extension);
                $db_new = new ServiceGallery();
                $db_new->service_item_id = $id;
                $db_new->image = '/res/images/service/' . $img_name . '-' . $i . '.' . $file->extension;
                $db_new->save(false);
            }
            return $this->refresh();
        }
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'db_images' => $db_images
        ]);
    }

    public function actionGallerydelete($id, $gid)
    {
        $image = ServiceGallery::findOne(['id' => $gid]);
        if ($image) {
            if (Yii::getAlias('@frontend' . '/web' . $image->image)){
                unlink(Yii::getAlias('@frontend' . '/web' . $image->image));
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }else{
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }
        }else{
            return $this->redirect(['gallery','id'=>$id]);
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use backend\models\Video;
use backend\models\VideoSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','delete','tomain','offmain'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Video models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Video();
        $datas = Video::find()->orderBy(['id'=>SORT_DESC])->asArray()->all();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->refresh();
        }
        return $this->render('index', [
            'model'=>$model,
            'datas'=>$datas
        ]);
    }

    /**
     * Deletes an existing Video model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Video model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Video the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionTomain($id)
    {
        $model = $this->findModel($id);
        $model->main = 1;
        $model->save(false);
        return $this->redirect(['index']);
    }
    public function actionOffmain($id)
    {
        $model = $this->findModel($id);
        $model->main = 0;
        $model->save(false);
        return $this->redirect(['index']);
    }
}

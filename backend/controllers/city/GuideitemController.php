<?php

namespace backend\controllers\city;

use backend\models\city\City;
use backend\models\city\GuideGallery;
use backend\models\city\GuideMenu;
use Yii;
use backend\models\city\GuideItem;
use backend\models\city\GuideItemSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GuideitemController implements the CRUD actions for GuideItem model.
 */
class GuideitemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'create', 'delete', 'gallery', 'gallerydelete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'gallerydelete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GuideItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuideItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GuideItem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GuideItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GuideItem();
        $cities = City::find()->all();
        $menus = GuideMenu::find()->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/objects/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($dir . $img_name . '.' . $model->file->extension);
            $model->image = '/res/images/city/objects/' . $img_name . '.' . $model->file->extension;
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'cities' => $cities,
            'menus' => $menus
        ]);
    }

    /**
     * Updates an existing GuideItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $cities = City::find()->all();
        $menus = GuideMenu::find()->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/objects/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            if (UploadedFile::getInstance($model, 'fileUpdate')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/city/objects/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/city/objects/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                    $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                    $model->image = '/res/images/city/objects/' . $img_name . '.' . $model->fileUpdate->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'cities' => $cities,
            'menus' => $menus
        ]);
    }

    /**
     * Deletes an existing GuideItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GuideItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuideItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GuideItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $gallery = new GuideGallery();
        $db_images = GuideGallery::find()->where(['guide_item_id' => $id])->orderBy(['id' => SORT_DESC])->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/objects/g/');
        if ($gallery->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $gallery->file = UploadedFile::getInstances($gallery, 'file');
            $i = 0;
            foreach ($gallery->file as $file) {
                $i++;
                $file->saveAs($dir . $img_name . '-' . $i . '.' . $file->extension);
                $db_new = new GuideGallery();
                $db_new->guide_item_id = $id;
                $db_new->image = '/res/images/city/objects/g/' . $img_name . '-' . $i . '.' . $file->extension;
                $db_new->save(false);
            }
            return $this->refresh();
        }
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'db_images' => $db_images
        ]);
    }

    public function actionGallerydelete($id, $gid)
    {
        $image = GuideGallery::findOne(['id' => $gid]);
        if ($image) {
            if (Yii::getAlias('@frontend' . '/web' . $image->image)){
                unlink(Yii::getAlias('@frontend' . '/web' . $image->image));
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }else{
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }
        }else{
            return $this->redirect(['gallery','id'=>$id]);
        }
    }
}

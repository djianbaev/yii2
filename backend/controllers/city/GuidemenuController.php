<?php

namespace backend\controllers\city;

use Yii;
use backend\models\city\GuideMenu;
use backend\models\city\GuideMenuSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GuidemenuController implements the CRUD actions for GuideMenu model.
 */
class GuidemenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GuideMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuideMenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GuideMenu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GuideMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GuideMenu();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/menu/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($dir . $img_name . '.' . $model->file->extension);
            $model->image = '/res/images/city/menu/' . $img_name . '.' . $model->file->extension;

            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GuideMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/menu/');
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');

            if(UploadedFile::getInstance($model,'fileUpdate')){
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                        $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                        $model->image = '/res/images/city/menu/'.$img_name.'.'.$model->fileUpdate->extension;
                        if($model->save(false)){
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                        $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                        $model->image = '/res/images/city/menu/'.$img_name.'.'.$model->fileUpdate->extension;
                        if($model->save(false)){
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }else{
                    $model->fileUpdate = UploadedFile::getInstance($model,'fileUpdate');
                    $model->fileUpdate->saveAs($dir.$img_name.'.'.$model->fileUpdate->extension);
                    $model->image = '/res/images/city/menu/'.$img_name.'.'.$model->fileUpdate->extension;
                    if($model->save(false)){
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }else{
                if($model->save(false)){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing GuideMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
            try {
                unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                if($model->delete()){
                    return $this->redirect(['index']);
                }

            } catch (\Exception $e) {
                if($model->delete()){
                    return $this->redirect(['index']);
                }
            }
        }else{
            if($model->delete()){
                return $this->redirect(['index']);
            }
        }

    }

    /**
     * Finds the GuideMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuideMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GuideMenu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers\city;

use backend\models\city\CityGallery;
use Yii;
use backend\models\city\City;
use backend\models\city\CitySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'create', 'delete', 'gallery', 'gallerydelete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'gallerydelete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $map_name = uniqid('lifevibes');

            $model->f_image = UploadedFile::getInstance($model, 'f_image');
            $model->f_image->saveAs($dir . $img_name . '.' . $model->f_image->extension);
            $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image->extension;

            $model->f_map = UploadedFile::getInstance($model, 'f_map');
            $model->f_map->saveAs($dir . $map_name . '.' . $model->f_map->extension);
            $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map->extension;

            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/');
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');
            $map_name = uniqid('lifevibes');

            if (!UploadedFile::getInstance($model, 'f_image_update') && !UploadedFile::getInstance($model, 'f_map_update')) {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } elseif (UploadedFile::getInstance($model, 'f_image_update') && !UploadedFile::getInstance($model, 'f_map_update')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                        $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                        $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                        $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                        $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                    $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                    $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } elseif (!UploadedFile::getInstance($model, 'f_image_update') && UploadedFile::getInstance($model, 'f_map_update')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->map)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->map));
                        $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                        $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                        $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                        $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                        $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                    $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                    $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } elseif (UploadedFile::getInstance($model, 'f_image_update') && UploadedFile::getInstance($model, 'f_map_update')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image) && Yii::getAlias('@frontend' . '/web' . $model->map)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->map));

                        $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                        $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                        $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;

                        $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                        $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                        $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;

                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                        $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                        $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;

                        $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                        $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                        $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;

                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->f_image_update = UploadedFile::getInstance($model, 'f_image_update');
                    $model->f_image_update->saveAs($dir . $img_name . '.' . $model->f_image_update->extension);
                    $model->image = '/res/images/city/' . $img_name . '.' . $model->f_image_update->extension;

                    $model->f_map_update = UploadedFile::getInstance($model, 'f_map_update');
                    $model->f_map_update->saveAs($dir . $map_name . '.' . $model->f_map_update->extension);
                    $model->map = '/res/images/city/' . $map_name . '.' . $model->f_map_update->extension;

                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }
        }

        return $this->render('update', ['model' => $model,]);
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $gallery = new CityGallery();
        $db_images = CityGallery::find()->where(['city_id' => $id])->orderBy(['id' => SORT_DESC])->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/city/objects/g/');
        if ($gallery->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $gallery->file = UploadedFile::getInstances($gallery, 'file');
            $i = 0;
            foreach ($gallery->file as $file) {
                $i++;
                $file->saveAs($dir . $img_name . '-' . $i . '.' . $file->extension);
                $db_new = new CityGallery();
                $db_new->city_id = $id;
                $db_new->image = '/res/images/city/objects/g/' . $img_name . '-' . $i . '.' . $file->extension;
                $db_new->save(false);
            }
            return $this->refresh();
        }
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'db_images' => $db_images
        ]);
    }

    public function actionGallerydelete($id, $gid)
    {
        $image = CityGallery::findOne(['id' => $gid]);
        if ($image) {
            if (Yii::getAlias('@frontend' . '/web' . $image->image)){
                unlink(Yii::getAlias('@frontend' . '/web' . $image->image));
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }else{
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }
        }else{
            return $this->redirect(['gallery','id'=>$id]);
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use backend\models\TeamMenu;
use backend\models\TeamMenuSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TeammenuController implements the CRUD actions for TeamMenu model.
 */
class TeammenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TeamMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(1),
        ]);
    }



    /**
     * Updates an existing TeamMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel(1);
        $dir = Yii::getAlias('@frontend'.'/web/res/images/about/');
        if ($model->load(Yii::$app->request->post())) {
            if(UploadedFile::getInstance($model,'file')){
                $img_name = uniqid('lifevibes');

                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->file = UploadedFile::getInstance($model,'file');
                        $model->file->saveAs($dir.$img_name.'.'.$model->file->extension);
                        $model->image = '/res/images/about/'.$img_name.'.'.$model->file->extension;
                        if($model->save(false)){
                            return $this->redirect(['index']);
                        }

                    } catch (\Exception $e) {
                        $model->file = UploadedFile::getInstance($model,'file');
                        $model->file->saveAs($dir.$img_name.'.'.$model->file->extension);
                        $model->image = '/res/images/about/'.$img_name.'.'.$model->file->extension;
                        if($model->save(false)){
                            return $this->redirect(['index']);
                        }
                    }
                }else{
                    $model->file = UploadedFile::getInstance($model,'file');
                    $model->file->saveAs($dir.$img_name.'.'.$model->file->extension);
                    $model->image = '/res/images/about/'.$img_name.'.'.$model->file->extension;
                    if($model->save(false)){
                        return $this->redirect(['index']);
                    }
                }

            }else{
                if($model->save(false)){
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TeamMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TeamMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeamMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeamMenu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

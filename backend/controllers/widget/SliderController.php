<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 13.04.2020
 * Time: 22:39
 */
namespace backend\controllers\widget;

use backend\models\widget\IndexSlider;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex(){
        $model = new IndexSlider();
        $models = IndexSlider::find()->all();
        $dir = Yii::getAlias('@frontend'.'/web/res/images/main-slider/');
        if($model->load(Yii::$app->request->post())){
            $img_name = uniqid('lifevibes');
            $model->file = UploadedFile::getInstance($model,'file');
            $model->file->saveAs($dir.$img_name.'.'.$model->file->extension);
            $model->image = '/res/images/main-slider/'.$img_name.'.'.$model->file->extension;
            if($model->save(false)){
                return $this->refresh();
            }
        }
        return $this->render('index',[
            'model'=>$model,
            'models'=>$models
        ]);
    }

    public function actionDelete($id)
    {
        $model = IndexSlider::findOne(['id'=>$id]);
        if ($model) {
            if (Yii::getAlias('@frontend' . '/web' . $model->image)){
                unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                if($model->delete()){
                    return $this->redirect(['index']);
                }
            }else{
                if($model->delete()){
                    return $this->redirect(['index']);
                }
            }
        }else{
            return $this->redirect(['index']);
        }
    }
}
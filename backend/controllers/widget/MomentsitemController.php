<?php

namespace backend\controllers\widget;

use backend\models\widget\IndexMoments;
use backend\models\widget\MomentsGallery;
use Yii;
use backend\models\widget\MomentsItem;
use backend\models\widget\MomentsItemSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MomentsitemController implements the CRUD actions for MomentsItem model.
 */
class MomentsitemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view','create','delete','gallery','gallerydelete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'gallerydelete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MomentsItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MomentsItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MomentsItem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MomentsItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MomentsItem();
        $dir = Yii::getAlias('@frontend' . '/web/res/images/moment/obj/');
        $moments = IndexMoments::find()->where(['id'=>[1,3]])->all();
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($dir . $img_name . '.' . $model->file->extension);
            $model->image = '/res/images/moment/obj/' . $img_name . '.' . $model->file->extension;
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'moments'=>$moments
        ]);
    }

    /**
     * Updates an existing MomentsItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@frontend' . '/web/res/images/moment/obj/');
        $moments = IndexMoments::find()->where(['id'=>[1,3]])->all();
        if ($model->load(Yii::$app->request->post())) {

            $img_name = uniqid('lifevibes');

            if (UploadedFile::getInstance($model, 'fileUpdate')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/moment/obj/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/moment/obj/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                    $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                    $model->image = '/res/images/moment/obj/' . $img_name . '.' . $model->fileUpdate->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'moments'=>$moments
        ]);
    }

    /**
     * Deletes an existing MomentsItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MomentsItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MomentsItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MomentsItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $gallery = new MomentsGallery();
        $db_images = MomentsGallery::find()->where(['moment_item_id' => $id])->orderBy(['id' => SORT_DESC])->all();

        $dir = Yii::getAlias('@frontend' . '/web/res/images/moment/obj/');
        if ($gallery->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');
            $gallery->file = UploadedFile::getInstances($gallery, 'file');
            $i = 0;
            foreach ($gallery->file as $file) {
                $i++;
                $file->saveAs($dir . $img_name . '-' . $i . '.' . $file->extension);
                $db_new = new MomentsGallery();
                $db_new->moment_item_id = $id;
                $db_new->image = '/res/images/moment/obj/' . $img_name . '-' . $i . '.' . $file->extension;
                $db_new->save(false);
            }
            return $this->refresh();
        }
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'db_images' => $db_images
        ]);
    }


    public function actionGallerydelete($id, $gid)
    {
        $image = MomentsGallery::findOne(['id' => $gid]);
        if ($image) {
            if (Yii::getAlias('@frontend' . '/web' . $image->image)){
                unlink(Yii::getAlias('@frontend' . '/web' . $image->image));
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }else{
                if($image->delete()){
                    return $this->redirect(['gallery','id'=>$id]);
                }
            }
        }else{
            return $this->redirect(['gallery','id'=>$id]);
        }
    }
}

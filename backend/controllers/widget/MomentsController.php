<?php

namespace backend\controllers\widget;

use Yii;
use backend\models\widget\IndexMoments;
use backend\models\widget\IndexMomentsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MomentsController implements the CRUD actions for IndexMoments model.
 */
class MomentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','update','view'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IndexMoments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndexMomentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IndexMoments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Updates an existing IndexMoments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dir = Yii::getAlias('@frontend'.'/web/res/images/moment/');
        if ($model->load(Yii::$app->request->post())) {
            $img_name = uniqid('lifevibes');

            if (UploadedFile::getInstance($model, 'fileUpdate')) {
                if (Yii::getAlias('@frontend' . '/web' . $model->image)) {
                    try {
                        unlink(Yii::getAlias('@frontend' . '/web' . $model->image));
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/moment/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }

                    } catch (\Exception $e) {
                        $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                        $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                        $model->image = '/res/images/moment/' . $img_name . '.' . $model->fileUpdate->extension;
                        if ($model->save(false)) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                } else {
                    $model->fileUpdate = UploadedFile::getInstance($model, 'fileUpdate');
                    $model->fileUpdate->saveAs($dir . $img_name . '.' . $model->fileUpdate->extension);
                    $model->image = '/res/images/moment/' . $img_name . '.' . $model->fileUpdate->extension;
                    if ($model->save(false)) {
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }



    /**
     * Finds the IndexMoments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IndexMoments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IndexMoments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

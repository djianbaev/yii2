<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 17.04.2020
 * Time: 1:15
 */

namespace backend\controllers;


use backend\models\user\Userdata;
use common\models\User;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class ClientController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $datas = [];
        $users = User::find()->where(['!=', 'email', 'admin@life-vibes.com'])->orderBy(['id' => SORT_DESC])->asArray()->all();


        foreach ($users as $user) {
            $datas[$user['id']]=[
                'id'=>$user['id'], 'username'=>$user['username'], 'email'=>$user['email'],
            ];
        }

//        echo '<pre>';
//        print_r($datas);
//        exit();
        $provider = new ArrayDataProvider([
            'allModels' => $datas,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'datas' => $datas,
            'provider' => $provider
        ]);
    }

    public function actionView($id)
    {
        $userdata = Userdata::find()->where(['user_id' => $id])->asArray()->one();
        $user = User::find()->where(['id' => $id])->andFilterWhere(['!=', 'email', 'admin@life-vibes.com'])->one();

        return $this->render('view', [
            'user' => $user,
            'userdata' => $userdata
        ]);
    }

}
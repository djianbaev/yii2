<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<style>
    .table-bordered {
        border: 1px solid #dee2e6;
    }

    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
    }

    .table-bordered thead td, .table-bordered thead th {
        border-bottom-width: 2px;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }

    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid #dee2e6;
    }
</style>
<div class="verify-email">

    <h1>Заявка</h1>

    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Имя</th>
            <td><?php echo $data['name'] ?></td>
        </tr>
        <tr>
            <th>Фамилия</th>
            <td><?php echo $data['surname'] ?></td>
        </tr>
        <tr>
            <th>Эл.адрес</th>
            <td><?php echo $data['email'] ?></td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td><?php echo $data['phone'] ?></td>
        </tr>
        <tr>
            <th>Страна</th>
            <td><?php echo $data['country'] ?></td>
        </tr>
        <tr>
            <th>Город</th>
            <td><?php echo $data['city'] ?></td>
        </tr>
        <tr>
            <th>Почтовый индекс</th>
            <td><?php echo $data['zipcode'] ?></td>
        </tr>
        <tr>
            <th>Пол</th>
            <td><?php echo $data['gender'] ?></td>
        </tr>
        <tr>
            <th>Страна (отправления)</th>
            <td><?php echo $data['d_country'] ?></td>
        </tr>
        <tr>
            <th>Город (отправления)</th>
            <td><?php echo $data['d_city'] ?></td>
        </tr>
        <tr>
            <th>Аэропорт (отправления)</th>
            <td><?php echo $data['d_airport'] ?></td>
        </tr>
        <tr>
            <th>Знаете ли вы точные даты вашего путешествия?</th>
            <td><?php if ($data['d_dates'] == 'no') {
                    echo 'нет';
                } else {
                    echo 'Да';
                } ?></td>
        </tr>
        <tr>
            <th>Дата от</th>
            <td><?php echo $data['d_dates_from'] ?></td>
        </tr>
        <tr>
            <th>Дата до</th>
            <td><?php echo $data['d_dates_to'] ?></td>
        </tr>
        <tr>
            <th>Взрослые кол-во</th>
            <td><?php echo $data['adults'] ?></td>
        </tr>
        <tr>
            <th>Дети кол-во</th>
            <td><?php echo $data['kids'] ?></td>
        </tr>
        <tr>
            <th>Способ свзяи</th>
            <td><?php echo $data['connect'] ?></td>
        </tr>
        <tr>
            <th>Удобное время для свзяи</th>
            <td><?php echo $data['connect_time'] ?></td>
        </tr>
        <tr>
            <th>Я СОГЛАСЕН ПРИНИМАТЬ РЕКЛАМНУЮ РАССЫЛКУ</th>
            <td><?php echo $data['agree_1'] ?></td>
        </tr>
        <tr>
            <th>Я ОЗНАКОМЛЕН И ПРИНИМАЮ ОБЩИЕ УСЛОВИЯ ИСПОЛЬЗОВАНИЯ САЙТА</th>
            <td><?php echo $data['agree_2'] ?></td>
        </tr>
        <tr>
            <th>Я СОГЛАСЕН ПРИНИМАТЬ ИНФОРМАЦИЮ О ТУРАХ, СТАТЬИ БЛОГА ЧЕРЕЗ EMAIL</th>
            <td><?php echo $data['agree_3'] ?></td>
        </tr>

        <tr>
            <th>Куда вы собираетесь отправиться</th>
            <td><?php echo $data['uwant'] ?></td>
        </tr>
        <tr>
            <th>Ваше путешествие</th>
            <td><?php echo $data['team'] ?></td>
        </tr>

        <tr>
            <th>Согласны ли вы присоединиться к существующей группе?</th>
            <td><?php echo $data['join'] ?></td>
        </tr>
        <tr>
            <th>Есть ли путешествующие с ограниченными возможностями?</th>
            <td><?php echo $data['invalid'] ?></td>
        </tr>
        <tr>
            <th>На какой вы стадии вашего планирования путешествия?</th>
            <td><?php echo $data['plan'] ?></td>
        </tr>

        <tr>
            <th>Какой уровень комфорта жилья вы хотите выбрать?</th>
            <td><?php echo $data['house'] ?></td>
        </tr>
        <tr>
            <th>Для вашего питания, хотели бы вы, чтобы туроператор вам предложил</th>
            <td><?php echo $data['food'] ?></td>
        </tr>
        <tr>
            <th>Для ваших передвижений, во время вашего путешествия, вы бы хотели</th>
            <td><?php echo $data['transport'] ?></td>
        </tr>
        <tr>
            <th>Желаете ли вы быть сопровождены гидом?</th>
            <td><?php echo $data['guide'] ?></td>
        </tr>
        <tr>
            <th>Ваш гид должен говорить на</th>
            <td><?php echo $data['lang'].' '.$data['langother'].' ' ?></td>
        </tr>
        <tr>
            <th>КУЛЬТУРНОЕ ДОСТОЯНИЕ</th>
            <td><?php echo $data['culture']?></td>
        </tr>
        <tr>
            <th>СПОРТ И ОТДЫХ</th>
            <td><?php echo $data['sport']?></td>
        </tr>
        <tr>
            <th>РЕЛАКС</th>
            <td><?php echo $data['relax']?></td>
        </tr>
        <tr>
            <th>Расскажите нам о вашем проекте путешествие</th>
            <td><?php echo $data['project']?></td>
        </tr>
        <tr>
            <th>Ваш примерный бюджет и количество дней</th>
            <td><?php echo $data['budget'].' '.$data['budgetdays']?></td>
        </tr>
        <tr>
            <th>Как вы узнали о нас?</th>
            <td><?php echo $data['source']?></td>
        </tr>


        </tbody>
    </table>

</div>

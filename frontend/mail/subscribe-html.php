<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<style>
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
    }
    .table-bordered thead td, .table-bordered thead th {
        border-bottom-width: 2px;
    }
    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }
    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }
    .table-bordered td, .table-bordered th {
        border: 1px solid #dee2e6;
    }
</style>
<div class="verify-email">

    <h1>Новая подписка</h1>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Имя</th>
            <th scope="col">Эл.почта</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $data['name']?></td>
            <td><?php echo $data['email']?></td>
        </tr>
        </tbody>
    </table>

</div>

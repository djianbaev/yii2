<?php

namespace frontend\controllers;

use backend\models\city\City;
use backend\models\city\CityGallery;
use backend\models\city\GuideGallery;
use backend\models\city\GuideItem;
use backend\models\city\GuideMenu;
use backend\models\Contact;
use backend\models\Footer;
use backend\models\FooterItem;
use backend\models\info\InfoItem;
use backend\models\info\InfoMenu;
use backend\models\Menu;
use backend\models\review\ReviewIndex;
use backend\models\review\ReviewService;
use backend\models\review\ReviewTour;
use backend\models\tour\Calendar;
use backend\models\tour\Service;
use backend\models\tour\ServiceGallery;
use backend\models\tour\ServiceItem;
use backend\models\tour\Tour;
use backend\models\tour\TourCalendar;
use backend\models\tour\TourDay;
use backend\models\tour\TourGallery;
use backend\models\user\Userbook;
use backend\models\user\Userdata;
use backend\models\Video;
use backend\models\widget\IndexMoments;
use backend\models\widget\MomentsGallery;
use backend\models\widget\MomentsItem;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use function PHPSTORM_META\map;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidArgumentException;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\View;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $video = Video::find()->where(['main'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->one();

        return $this->render('index',[
            'video'=>$video,
        ]);
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            $model->username = '';

            $this->layout = 'mainLogin';
            return $this->render('account/login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect('/welcome-new-client');
        }
        $this->layout = 'mainLogin';
        return $this->render('account/signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return $this->redirect(['/email-for-reset-send']);
            }
        }

        $this->layout = 'mainLogin';
        return $this->render('account/requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->redirect(['login']);
        }

        $this->layout = 'mainLogin';
        return $this->render('account/resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                return $this->redirect(['/account']);
            }
        }
        return $this->redirect(['/failed-verify']);
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
//    public function actionResendVerificationEmail()
//    {
//        $model = new ResendVerificationEmailForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail()) {
//                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
//                return $this->goHome();
//            }
//            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
//        }
//
//        return $this->render('account/resendVerificationEmail', [
//            'model' => $model
//        ]);
//    }







    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = Contact::find()->asArray()->one();
        $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
        return $this->render('contact',[
            'model'=>$model
        ]);

    }

    public function actionAjaxcontact(){
        if(Yii::$app->request->isAjax){
            $data = [];
            $data['name'] = strip_tags(Yii::$app->request->post('name'));
            $data['email'] = strip_tags(Yii::$app->request->post('email'));
            $data['subject'] = strip_tags(Yii::$app->request->post('subject'));
            $data['message'] = strip_tags(Yii::$app->request->post('message'));
            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'contact-html', 'text' => 'contact-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Contact Us on Life Vibes')
                ->send();

            return 200;

        }
    }

    public function actionSubscribe()
    {
        if(Yii::$app->request->isAjax){
            $data = [];
            $data['name'] = strip_tags(Yii::$app->request->post('name'));
            $data['email'] = strip_tags(Yii::$app->request->post('email'));

            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'subscribe-html', 'text' => 'subscribe-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Subscribe on Life Vibes')
                ->send();

            if($mail){
                return 200;
            }else{
                return 400;
            }
        }
    }


    public function actionTourbooking()
    {

        if(Yii::$app->request->post()){
            $data = [];
            $data['id'] = strip_tags(Yii::$app->request->post('book-tour'));
            $data['name'] = strip_tags(Yii::$app->request->post('book-name'));
            $data['email'] = strip_tags(Yii::$app->request->post('book-email'));
            $data['adults'] = strip_tags(Yii::$app->request->post('book-adults'));
            $data['kids'] = strip_tags(Yii::$app->request->post('book-kids'));

            $tour = Tour::find()->where(['id'=>$data['id']])->asArray()->one();
            if(!empty($tour)){
                $data['tour'] = $tour['name_ru'];

                if(!Yii::$app->user->isGuest){
                    $userbook = new Userbook();
                    $userbook->user_id = Yii::$app->user->identity->id;
                    $userbook->booking_ru = $tour['name_ru'];
                    $userbook->booking_en = $tour['name_en'];
                    $userbook->booking_fr = $tour['name_fr'];
                    $userbook->booking_it = $tour['name_it'];
                    $userbook->booking_de = $tour['name_de'];
                    $userbook->created_date = date('d.m.Y');
                    $userbook->save(false);
                }

            }else{
                $data['tour'] = 'Попытка взлома';
            }

            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'book-tour-html', 'text' => 'book-tour-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Booking Tour on Life Vibes')
                ->send();



            return $this->render('tour_book');
        }
    }
    public function actionServicebooking()
    {

        if(Yii::$app->request->post()){
            $data = [];
            $data['id'] = strip_tags(Yii::$app->request->post('book-service'));
            $data['name'] = strip_tags(Yii::$app->request->post('book-name'));
            $data['email'] = strip_tags(Yii::$app->request->post('book-email'));
            $data['adults'] = strip_tags(Yii::$app->request->post('book-adults'));
            $data['kids'] = strip_tags(Yii::$app->request->post('book-kids'));

            $service = ServiceItem::find()->where(['id'=>$data['id']])->asArray()->one();
            if(!empty($service)){
                $data['tour'] = $service['name_ru'];

                if(!Yii::$app->user->isGuest){
                    $userbook = new Userbook();
                    $userbook->user_id = Yii::$app->user->identity->id;
                    $userbook->booking_ru = $service['name_ru'];
                    $userbook->booking_en = $service['name_en'];
                    $userbook->booking_fr = $service['name_fr'];
                    $userbook->booking_it = $service['name_it'];
                    $userbook->booking_de = $service['name_de'];
                    $userbook->created_date = date('d.m.Y');
                    $userbook->save(false);
                }

            }else{
                $data['tour'] = 'Попытка взлома';
            }

            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'book-tour-html', 'text' => 'book-tour-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Booking Service on Life Vibes')
                ->send();



            return $this->render('tour_book');
        }
    }
    public function actionMomentbooking()
    {

        if(Yii::$app->request->post()){
            $data = [];
            $data['id'] = strip_tags(Yii::$app->request->post('book-moment'));
            $data['name'] = strip_tags(Yii::$app->request->post('book-name'));
            $data['email'] = strip_tags(Yii::$app->request->post('book-email'));
            $data['adults'] = strip_tags(Yii::$app->request->post('book-adults'));
            $data['kids'] = strip_tags(Yii::$app->request->post('book-kids'));

            $moment = MomentsItem::find()->where(['id'=>$data['id']])->asArray()->one();
            if(!empty($moment)){
                $data['tour'] = $moment['name_ru'];

                if(!Yii::$app->user->isGuest){
                    $userbook = new Userbook();
                    $userbook->user_id = Yii::$app->user->identity->id;
                    $userbook->booking_ru = $moment['name_ru'];
                    $userbook->booking_en = $moment['name_en'];
                    $userbook->booking_fr = $moment['name_fr'];
                    $userbook->booking_it = $moment['name_it'];
                    $userbook->booking_de = $moment['name_de'];
                    $userbook->created_date = date('d.m.Y');
                    $userbook->save(false);
                }

            }else{
                $data['tour'] = 'Попытка взлома';
            }

            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'book-tour-html', 'text' => 'book-tour-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Booking Moments on Life Vibes')
                ->send();



            return $this->render('tour_book');
        }
    }

    /*** tours ***/

    public function actionTours()
    {
        $menu = Menu::find()->where(['action' => 'menu/experience'])->asArray()->one();
        $models = Tour::find()->asArray()->all();

        $datas = [];
        $services = Service::find()->asArray()->all();
        foreach ($services as $service) {
            $items = ServiceItem::find()->where(['service_id' => $service['id']])->limit(3)->asArray()->all();
            if (!empty($items)) {
                $datas[$service['id']] = $items;
            }
        }

        return $this->render('tour/index', [
            'menu' => $menu,
            'models' => $models,
            'services' => $services,
            'datas' => $datas
        ]);
    }

    public function actionTourview($tour_slug)
    {
        $tour_slug = strip_tags($tour_slug);
        $menu = Menu::find()->where(['action' => 'menu/experience'])->asArray()->one();
        $model = Tour::find()->where(['slug' => $tour_slug])->asArray()->one();
        if (!empty($model)) {
            $days = TourDay::find()->where(['tour_id' => $model['id']])->asArray()->all();
            $images = TourGallery::find()->where(['tour_id' => $model['id']])->asArray()->all();
            $calendar = [];
            $rowC = Calendar::find()->asArray()->all();
            foreach ($rowC as $record) {
                $calendar[$record['id']] = $record;
            }
            $months = TourCalendar::find()->where(['tour_id' => $model['id']])->asArray()->all();

            $reviews = ReviewTour::find()->where(['tour_id'=>$model['id'],'status'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->all();

            $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');

            $userdata = [];
            if (!Yii::$app->user->isGuest){
                $userdata = Userdata::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
                $userdata['email'] = Yii::$app->user->identity->email;
            }else{
                $userdata = [];
            }
            return $this->render('tour/view', [
                'menu' => $menu,
                'model' => $model,
                'calendar' => $calendar,
                'months' => $months,
                'days' => $days,
                'images' => $images,
                'reviews'=>$reviews,
                'userdata'=>$userdata
            ]);
        }

    }




    public function actionService($service_id)
    {
        $service_id = strip_tags($service_id);
        if (is_numeric($service_id)) {
            $menu = Menu::find()->where(['action' => 'menu/experience'])->asArray()->one();
            $service = Service::find()->where(['id' => $service_id])->asArray()->one();
            if (!empty($service)) {
                $datas = ServiceItem::find()->where(['service_id' => $service_id])->asArray()->all();
                return $this->render('service/index', [
                    'menu' => $menu,
                    'service' => $service,
                    'datas' => $datas
                ]);
            }
        }
    }


    public function actionServiceitem($service_item_id)
    {
        $service_item_id = strip_tags($service_item_id);
        if (is_numeric($service_item_id)) {
            $menu = Menu::find()->where(['action' => 'menu/experience'])->asArray()->one();
            $item = ServiceItem::find()->where(['id' => $service_item_id])->asArray()->one();
            if (!empty($item)) {
                $service = Service::find()->where(['id' => $item['service_id']])->asArray()->one();
                $images = ServiceGallery::find()->where(['service_item_id' => $item['id']])->asArray()->all();

                $reviews = ReviewService::find()->where(['service_item_id'=>$item['id'],'status'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->all();

                $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');

                $userdata = [];
                if (!Yii::$app->user->isGuest){
                    $userdata = Userdata::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
                    $userdata['email'] = Yii::$app->user->identity->email;
                }else{
                    $userdata = [];
                }
                return $this->render('service/view', [
                    'menu' => $menu,
                    'service' => $service,
                    'item' => $item,
                    'images' => $images,
                    'reviews'=>$reviews,
                    'userdata'=>$userdata
                ]);
            }
        }
    }


    /*** moments ***/

    public function actionMoment()
    {
        $moment = IndexMoments::find()->where(['id' => 1])->asArray()->one();
        $models = MomentsItem::find()->where(['moment_id' => 1])->asArray()->all();

        return $this->render('moment/moment_index', [
            'moment' => $moment,
            'models' => $models
        ]);
    }

    public function actionMomentview($moment_slug)
    {
        $moment = IndexMoments::find()->where(['id' => 1])->asArray()->one();
        $model = MomentsItem::find()->where(['moment_id' => 1, 'slug' => $moment_slug])->asArray()->one();
        if (!empty($model)) {
            $images = MomentsGallery::find()->where(['moment_item_id' => $model['id']])->asArray()->all();
            $userdata = [];
            if (!Yii::$app->user->isGuest){
                $userdata = Userdata::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
                $userdata['email'] = Yii::$app->user->identity->email;
            }else{
                $userdata = [];
            }
            return $this->render('moment/moment_view', [
                'moment' => $moment,
                'model' => $model,
                'images' => $images,
                'userdata'=>$userdata
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }


    public function actionCuisine()
    {
        $moment = IndexMoments::find()->where(['id' => 3])->asArray()->one();
        $models = MomentsItem::find()->where(['moment_id' => 3])->asArray()->all();

        return $this->render('moment/cuisine_index', [
            'moment' => $moment,
            'models' => $models
        ]);
    }

    public function actionCuisineview($cuisine_slug)
    {
        $moment = IndexMoments::find()->where(['id' => 3])->asArray()->one();
        $model = MomentsItem::find()->where(['moment_id' => 3, 'slug' => $cuisine_slug])->asArray()->one();
        if (!empty($model)) {
            $images = MomentsGallery::find()->where(['moment_item_id' => $model['id']])->asArray()->all();
            $userdata = [];
            if (!Yii::$app->user->isGuest){
                $userdata = Userdata::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
                $userdata['email'] = Yii::$app->user->identity->email;
            }else{
                $userdata = [];
            }
            return $this->render('moment/cuisine_view', [
                'moment' => $moment,
                'model' => $model,
                'images' => $images,
                'userdata'=>$userdata
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }


    public function actionCity($city_id)
    {
        if (is_numeric($city_id)) {
            $model = City::find()->where(['id' => $city_id])->asArray()->one();
            if (!empty($model)) {

                $menu = Menu::find()->where(['action' => 'menu/guide'])->asArray()->one();
                $images = CityGallery::find()->where(['city_id' => $city_id])->asArray()->all();

                $rowG = GuideMenu::find()->where(['status' => 1])->orderBy(['number' => SORT_ASC])->asArray()->all();

                return $this->render('city/city_view', [
                    'model' => $model,
                    'menu' => $menu,
                    'images' => $images,
                    'rowG'=>$rowG
                ]);
            }
        }
    }




    public function actionFooter($footer_id){
        $id = strip_tags($footer_id);
        if(is_numeric($id)){
            $model = FooterItem::find()->where(['id'=>$id])->asArray()->one();
            if(!empty($model)){
                return $this->render('footer_item',[
                    'model'=>$model
                ]);
            }
        }
    }



    /*** application ***/
    public function actionApplication()
    {
        if (Yii::$app->request->isPost) {

            $data = [];

            $data['name'] = strip_tags(Yii::$app->request->post('name'));
            $data['surname'] = strip_tags(Yii::$app->request->post('surname'));
            $data['email'] = strip_tags(Yii::$app->request->post('email'));
            $data['phone'] = strip_tags(Yii::$app->request->post('phone'));
            $data['country'] = strip_tags(Yii::$app->request->post('country'));
            $data['city'] = strip_tags(Yii::$app->request->post('city'));
            $data['zipcode'] = strip_tags(Yii::$app->request->post('zipcode'));

            $gender = strip_tags(Yii::$app->request->post('gender'));
            switch ($gender) {
                case 'male':
                    $data['gender'] = 'Мужчина';
                    break;
                case 'female':
                    $data['gender'] = 'Женщина';
                    break;
                case 'other':
                    $data['gender'] = 'Не хочу указывать';
                    break;
                default:
                    $data['gender'] = 'Не хочу указывать';
                    break;
            }

            $data['d_country'] = strip_tags(Yii::$app->request->post('d_country'));
            $data['d_city'] = strip_tags(Yii::$app->request->post('d_city'));
            $data['d_airport'] = strip_tags(Yii::$app->request->post('d_airport'));
            $data['d_dates'] = strip_tags(Yii::$app->request->post('d_dates'));
            $data['d_dates_from'] = strip_tags(Yii::$app->request->post('d_dates_from'));
            $data['d_dates_to'] = strip_tags(Yii::$app->request->post('d_dates_to'));
            $data['d_days'] = strip_tags(Yii::$app->request->post('d_days'));

            $data['adults'] = strip_tags(Yii::$app->request->post('adults'));
            $data['kids'] = strip_tags(Yii::$app->request->post('kids'));

            $data['connect'] = '';
            if (isset($_POST['connect'])) {
                foreach ($_POST['connect'] as $connect) {
                    $data['connect'] = $data['connect'] . ' ' . $connect;
                }
            } else {
                $data['connect'] = 'Не указано';
            }

            $data['connect_time'] = strip_tags(Yii::$app->request->post('connect_time'));

            $data['agree_2'] = 'Да';
            (isset($_POST['agree_1'])) ? $data['agree_1'] = 'Да' : $data['agree_1'] = 'Нет';
            (isset($_POST['agree_3'])) ? $data['agree_3'] = 'Да' : $data['agree_3'] = 'Нет';

            $data['uwant'] = strip_tags(Yii::$app->request->post('uwant'));

            $data['team'] = '';
            if (isset($_POST['team'])) {
                foreach ($_POST['team'] as $team) {
                    $data['team'] = $data['team'] . ' ' . $team;
                }
            } else {
                $data['team'] = 'Не указано';
            }

            $join = Yii::$app->request->post('join');
            ($join[0] == 'yes') ? $data['join'] = 'Да' : $data['join'] = 'Нет';

            $invalid = Yii::$app->request->post('invalid');
            ($invalid[0] == 'yes') ? $data['invalid'] = 'Да' : $data['invalid'] = 'Нет';


            $plan = Yii::$app->request->post('plan');
            switch ($plan[0]) {
                case 1:
                    $data['plan'] = 'Мое направление уже выбрано, я хочу быстрее забронировать мое путешествие';
                    break;
                case 2:
                    $data['plan'] = 'Мое направление уже выбрано, я начинаю организовать мое путешествие';
                    break;
                case 3:
                    $data['plan'] = 'Я нуждаюсь в большей информации, чтобы определиться в моем решением';
                    break;
                default:
                    $data['plan'] = 'Я нуждаюсь в большей информации, чтобы определиться в моем решением';
                    break;
            }


            $house = Yii::$app->request->post('house');
            switch ($house[0]) {
                case 1:
                    $data['house'] = 'Нормальное и не дорогое';
                    break;
                case 2:
                    $data['house'] = 'Шарм и комфорт';
                    break;
                case 3:
                    $data['house'] = 'Люкс';
                    break;
                default:
                    $data['house'] = 'Нормальное и не дорогое';
                    break;
            }

            $food = Yii::$app->request->post('food');
            switch ($food[0]) {
                case 1:
                    $data['food'] = 'Завтрак с жильем';
                    break;
                case 2:
                    $data['food'] = 'Полупансион';
                    break;
                case 3:
                    $data['food'] = 'Полный пансион';
                    break;
                default:
                    $data['food'] = 'Выборочно, обеды и ужины в необыкновенных местах';
                    break;
            }

            $transport = Yii::$app->request->post('transport');
            switch ($transport[0]) {
                case 1:
                    $data['transport'] = 'Иметь организованный трансфер (трансферы в аэропорт и вокзал)';
                    break;
                case 2:
                    $data['transport'] = 'Иметь машину с водителем, предоставленную агентством';
                    break;
                case 3:
                    $data['transport'] = 'Аренда автомобиля ';
                    break;
                case 4:
                    $data['transport'] = 'Частичная организация транспорта (поезда, перелеты, машина)';
                    break;
                default:
                    $data['transport'] = 'Вы желаете передвигаться сами ';
                    break;
            }


            $guide = Yii::$app->request->post('guide');
            switch ($guide[0]) {
                case 1:
                    $data['guide'] = 'Да';
                    break;
                case 2:
                    $data['guide'] = 'Нет';
                    break;
                case 3:
                    $data['guide'] = 'Частично (в определенных городах)';
                    break;
                default:
                    $data['guide'] = 'Частично (в определенных городах)';
                    break;
            }


            if (isset($_POST['lang'])) {
                $data['lang'] = strip_tags(Yii::$app->request->post('lang'));
            } else {
                if (!empty(Yii::$app->request->post('langother'))) {
                    $data['lang'] = Yii::$app->request->post('langother');
                } else {
                    $data['lang'] = 'Не выбран';
                }
            }

            if(isset($_POST['langother'])){
                $data['langother'] = strip_tags(Yii::$app->request->post('langother'));
            }else{
                $data['langother'] = '';
            }

            if (isset($_POST['culture'])) {
                $data['culture'] = '';
                $culture = Yii::$app->request->post('culture');
                foreach ($culture as $item) {
                    switch ($item) {
                        case 1:
                            $data['culture'] = $data['culture'] . '-Музеи ';
                            break;
                        case 2:
                            $data['culture'] = $data['culture'] . '-Галереи ';
                            break;
                        case 3:
                            $data['culture'] = $data['culture'] . '-Исторические места ';
                            break;
                        case 4:
                            $data['culture'] = $data['culture'] . '-Национальные парки ';
                            break;

                    }
                }
            } else {
                $data['culture'] = 'Не выбрано';
            }


            if (isset($_POST['sport'])) {
                $data['sport'] = '';
                $sport = Yii::$app->request->post('sport');
                foreach ($sport as $item) {
                    switch ($item) {
                        case 1:
                            $data['sport'] = $data['sport'] . '-Трекинги ';
                            break;
                        case 2:
                            $data['sport'] = $data['sport'] . '-Пешие прогулки ';
                            break;
                        case 3:
                            $data['sport'] = $data['sport'] . '-Городское путешествие ';
                            break;
                        case 4:
                            $data['sport'] = $data['sport'] . '-Фотография ';
                            break;

                    }
                }
            } else {
                $data['sport'] = 'Не выбрано';
            }


            if (isset($_POST['relax'])) {
                $data['relax'] = '';
                $relax = Yii::$app->request->post('relax');
                foreach ($relax as $item) {
                    switch ($item) {
                        case 1:
                            $data['relax'] = $data['relax'] . '-Гастрономия ';
                            break;
                        case 2:
                            $data['relax'] = $data['relax'] . '-Спектакли ';
                            break;
                        case 3:
                            $data['relax'] = $data['relax'] . '-Ремесленничество ';
                            break;
                        case 4:
                            $data['relax'] = $data['relax'] . '-Живопись ';
                            break;

                    }
                }
            } else {
                $data['relax'] = 'Не выбрано';
            }

            $data['project'] = strip_tags(Yii::$app->request->post('project'));
            $data['budget'] = strip_tags(Yii::$app->request->post('budget'));
            $data['budgetdays'] = strip_tags(Yii::$app->request->post('budgetdays'));


            if (isset($_POST['source'])) {
                $data['source'] = '';
                $source = Yii::$app->request->post('source');
                foreach ($source as $item) {
                    switch ($item) {
                        case 1:
                            $data['source'] = $data['source'] . '-Google поиск ';
                            break;
                        case 2:
                            $data['source'] = $data['source'] . '-Yandex поиск ';
                            break;
                        case 3:
                            $data['source'] = $data['source'] . '-Facebook ';
                            break;
                        case 4:
                            $data['source'] = $data['source'] . '-Instagram ';
                            break;
                        case 5:
                            $data['source'] = $data['source'] . '-Twitter ';
                            break;
                        default:
                            $data['source'] = $data['source'] . '-Прочие ';
                            break;

                    }
                }
            } else {
                $data['source'] = 'Не выбрано';
            }


            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'application-html', 'text' => 'application-text'],
                    ['data' => $data]
                )
                ->setFrom([Yii::$app->params['adminEmail'] => 'Life Vibes Travel Agency'])
                ->setTo(Yii::$app->params['bookingEmail'])
                ->setSubject('Application on Life Vibes')
                ->send();

            return $this->render('application_send');

        } else {
           throw new NotFoundHttpException();

        }

    }










    public function actionReviewtour()
    {
        if(Yii::$app->request->isAjax){

            $data = [];
            $data['fullname'] = strip_tags(Yii::$app->request->post('fullname'));
            $data['email'] = strip_tags(Yii::$app->request->post('email'));
            $data['review'] = strip_tags(Yii::$app->request->post('review'));
            $data['tour'] = strip_tags(Yii::$app->request->post('tour'));

            $db_tour = Tour::find()->where(['id'=>$data['tour']])->one();
            if($db_tour){
                $uploadDir = Yii::getAlias('@frontend'.'/web/res/images/review/');
                $img_name = uniqid('lifevibes');
                $avatar_status = 0;

                if(UploadedFile::getInstanceByName('avatar')){
                    $avatar_status = 1;
                    $file = UploadedFile::getInstanceByName('avatar');
                    $model = DynamicModel::validateData(['avatar' => $file], [
                        [['avatar'], 'file', 'extensions' => 'jpg,png,jpeg','maxSize'=>'1048576'],
                    ]);
                    if ($model->hasErrors()) {
                        $avatar_status = 0;

                    } else {
                        if($file->saveAs($uploadDir.$img_name.'.'.$file->extension)){
                            $avatar_status = 1;
                        }else{
                            $avatar_status = 0;
                        }
                    }
                }else{
                    $avatar_status = 0;
                }


                if($avatar_status){
                    $db = new ReviewTour();
                    $db->tour_id = $db_tour->id;
                    $db->image = '/res/images/review/'.$img_name.'.'.$file->extension;
                    $db->fullname = $data['fullname'];
                    $db->email = $data['email'];
                    $db->text = $data['review'];
                    $db->status = 0;
                    $db->created_date = date('d.m.Y',time());
                    if($db->save(false)){
                        return 200;
                    }
                }else{
                    $db = new ReviewTour();
                    $db->tour_id = $db_tour->id;
                    $db->image = '/res/images/review/avatar.jpg';
                    $db->fullname = $data['fullname'];
                    $db->email = $data['email'];
                    $db->text = $data['review'];
                    $db->status = 0;
                    $db->created_date = date('d.m.Y',time());
                    if($db->save(false)){
                        return 200;
                    }
                }

            }else{
                return 404;
            }


        }
    }


    public function actionReviewservice()
    {
        if(Yii::$app->request->isAjax){

            $data = [];
            $data['fullname'] = strip_tags(Yii::$app->request->post('fullname'));
            $data['email'] = strip_tags(Yii::$app->request->post('email'));
            $data['review'] = strip_tags(Yii::$app->request->post('review'));
            $data['service'] = strip_tags(Yii::$app->request->post('service'));

            $db_service = ServiceItem::find()->where(['id'=>$data['service']])->one();
            if($db_service){
                $uploadDir = Yii::getAlias('@frontend'.'/web/res/images/review/');
                $img_name = uniqid('lifevibes');
                $avatar_status = 0;

                if(UploadedFile::getInstanceByName('avatar')){
                    $avatar_status = 1;
                    $file = UploadedFile::getInstanceByName('avatar');
                    $model = DynamicModel::validateData(['avatar' => $file], [
                        [['avatar'], 'file', 'extensions' => 'jpg,png,jpeg','maxSize'=>'1048576'],
                    ]);
                    if ($model->hasErrors()) {
                        $avatar_status = 0;

                    } else {
                        if($file->saveAs($uploadDir.$img_name.'.'.$file->extension)){
                            $avatar_status = 1;
                        }else{
                            $avatar_status = 0;
                        }
                    }
                }else{
                    $avatar_status = 0;
                }


                if($avatar_status){
                    $db = new ReviewService();
                    $db->service_item_id = $db_service->id;
                    $db->image = '/res/images/review/'.$img_name.'.'.$file->extension;
                    $db->fullname = $data['fullname'];
                    $db->email = $data['email'];
                    $db->text = $data['review'];
                    $db->status = 0;
                    $db->created_date = date('d.m.Y',time());
                    if($db->save(false)){
                        return 200;
                    }
                }else{
                    $db = new ReviewService();
                    $db->service_item_id = $db_service->id;
                    $db->image = '/res/images/review/avatar.jpg';
                    $db->fullname = $data['fullname'];
                    $db->email = $data['email'];
                    $db->text = $data['review'];
                    $db->status = 0;
                    $db->created_date = date('d.m.Y',time());
                    if($db->save(false)){
                        return 200;
                    }
                }

            }else{
                return 404;
            }


        }
    }





















    public function actionSearch($words){
        $lang = Yii::$app->language;
        $query = strip_tags($words);

        /*** guide ***/
        $row_guide = GuideItem::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'text_'.$lang, $query])
            ->asArray()->all();


        /***  tours ***/
        $row_tours = Tour::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'descr_'.$lang, $query])
            ->asArray()->all();


        /*** services ***/
        $row_services = ServiceItem::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'text_'.$lang, $query])
            ->asArray()->all();


        /*** info ***/
        $row_info = InfoItem::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'text_'.$lang, $query])
            ->asArray()->all();


        /*** cities ***/
        $row_cities = City::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->asArray()->all();

        /*** moments ***/
        $row_moments = MomentsItem::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'text_'.$lang, $query])
            ->asArray()->all();

        /*** footers ***/
        $row_footers = FooterItem::find()
            ->andFilterWhere(['like','name_'.$lang,$query])
            ->orFilterWhere(['like', 'text_'.$lang, $query])
            ->asArray()->all();



        return $this->render('search/index',[
            'row_guide'=>$row_guide,
            'row_tours'=>$row_tours,
            'row_services'=>$row_services,
            'row_info'=>$row_info,
            'row_cities'=>$row_cities,
            'row_moments'=>$row_moments,
            'row_footers'=>$row_footers,
            'query'=>$query
        ]);
    }


    public function actionSearchitem($type,$id){
        if(is_numeric($id)){
            $type = strip_tags($type);
            switch ($type){
                case 'guide':
                    $menu = Menu::find()->where(['action'=>'menu/guide'])->asArray()->one();
                    $model = GuideItem::find()->where(['id'=>$id])->asArray()->one();
                    if (!empty($model)){
                        $images = GuideGallery::find()->where(['guide_item_id'=>$id])->asArray()->all();
                        return $this->render('search/view_guide',[
                            'model'=>$model,
                            'menu'=>$menu,
                            'images'=>$images
                        ]);
                    }
                    break;

                case 'tour':
                    $menu = Menu::find()->where(['action'=>'menu/experience'])->asArray()->one();
                    $model = Tour::find()->where(['id'=>$id])->asArray()->one();
                    if(!empty($model)){
                        $days = TourDay::find()->where(['tour_id' => $model['id']])->asArray()->all();
                        $images = TourGallery::find()->where(['tour_id' => $model['id']])->asArray()->all();
                        $calendar = [];
                        $rowC = Calendar::find()->asArray()->all();
                        foreach ($rowC as $record) {
                            $calendar[$record['id']] = $record;
                        }
                        $months = TourCalendar::find()->where(['tour_id' => $model['id']])->asArray()->all();

                        $reviews = ReviewTour::find()->where(['tour_id'=>$model['id'],'status'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->all();

                        $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                        $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                        $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');

                        return $this->render('search/view_tour', [
                            'menu' => $menu,
                            'model' => $model,
                            'calendar' => $calendar,
                            'months' => $months,
                            'days' => $days,
                            'images' => $images,
                            'reviews'=>$reviews
                        ]);
                    }
                    break;

                case 'service':
                    $menu = Menu::find()->where(['action'=>'menu/experience'])->asArray()->one();
                    $item = ServiceItem::find()->where(['id'=>$id])->asArray()->one();
                    if(!empty($item)){
                        $service = Service::find()->where(['id' => $item['service_id']])->asArray()->one();
                        $images = ServiceGallery::find()->where(['service_item_id' => $item['id']])->asArray()->all();

                        $reviews = ReviewService::find()->where(['service_item_id'=>$item['id'],'status'=>1])->orderBy(['id'=>SORT_DESC])->asArray()->all();

                        $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                        $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                        $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');

                        return $this->render('search/view_service', [
                            'menu' => $menu,
                            'service' => $service,
                            'item' => $item,
                            'images' => $images,
                            'reviews'=>$reviews
                        ]);
                    }
                    break;

                case 'info':
                    $model = Menu::find()->where(['action' => 'menu/regionalinfo'])->asArray()->one();
                    $item = InfoItem::find()->where(['id' => $id])->asArray()->one();
                    if (!empty($item)) {
                        return $this->render('search/view_info', [
                            'model' => $model,
                            'item' => $item
                        ]);

                    }
                    break;

                case 'city':
                    $model = City::find()->where(['id' => $id])->asArray()->one();
                    if (!empty($model)) {

                        $menu = Menu::find()->where(['action' => 'menu/guide'])->asArray()->one();
                        $images = CityGallery::find()->where(['city_id' => $id])->asArray()->all();

                        $rowG = GuideMenu::find()->where(['status' => 1])->orderBy(['number' => SORT_ASC])->asArray()->all();

                        return $this->render('search/view_city', [
                            'model' => $model,
                            'menu' => $menu,
                            'images' => $images,
                            'rowG'=>$rowG
                        ]);
                    }
                    break;

                case 'moment':
                    $model = MomentsItem::find()->where(['id' => $id])->asArray()->one();
                    if (!empty($model)) {
                        $images = MomentsGallery::find()->where(['moment_item_id' => $model['id']])->asArray()->all();
                        return $this->render('search/view_moment', [
                            'model' => $model,
                            'images' => $images
                        ]);
                    } else {
                        throw new NotFoundHttpException();
                    }
                    break;

                case 'footer':
                    $model = FooterItem::find()->where(['id'=>$id])->asArray()->one();
                    if(!empty($model)){
                        return $this->render('search/view_footer',[
                            'model'=>$model
                        ]);
                    }
                    break;
            }
        }
    }
}

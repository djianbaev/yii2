<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 22:16
 */

namespace frontend\controllers;


use backend\models\user\Userbook;
use backend\models\user\Userdata;
use frontend\models\ResendVerificationEmailForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ClientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'actions' => ['welcome', 'failedverify', 'emailresetpass'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }


    public function actionWelcome()
    {
        return $this->render('welcome');
    }

    public function actionFailedverify()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return $this->redirect(['welcome']);
            }

        }
        return $this->render('failed_verify', [
            'model' => $model
        ]);

    }

    public function actionEmailresetpass()
    {
        return $this->render('email_reset_pass');
    }


    public function actionIndex()
    {
        $data = Userdata::find()->where(['user_id' => Yii::$app->user->id])->asArray()->one();
        $books = Userbook::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id'=>SORT_DESC])->asArray()->all();
        return $this->render('index', [
            'data' => $data,
            'books'=>$books
        ]);
    }

    public function actionCreate()
    {
        $model = new Userdata();
        if (Yii::$app->request->post()) {
            $model->user_id = Yii::$app->user->identity->id;
            $model->name = strip_tags(Yii::$app->request->post('name'));
            $model->surname = strip_tags(Yii::$app->request->post('surname'));
            $model->bday = strip_tags(Yii::$app->request->post('bday'));
            $model->phone = strip_tags(Yii::$app->request->post('phone'));
            $model->country = strip_tags(Yii::$app->request->post('country'));
            $model->city = strip_tags(Yii::$app->request->post('city'));
            $model->save(false);

            return $this->redirect(['index']);
        }
        $this->view->registerJsFile('/res/js/mask.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        return $this->render('create');
    }

    public function actionUpdate()
    {
        $model = Userdata::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        if (Yii::$app->request->post()) {

            $model->name = strip_tags(Yii::$app->request->post('name'));
            $model->surname = strip_tags(Yii::$app->request->post('surname'));
            $model->bday = strip_tags(Yii::$app->request->post('bday'));
            $model->phone = strip_tags(Yii::$app->request->post('phone'));
            $model->country = strip_tags(Yii::$app->request->post('country'));
            $model->city = strip_tags(Yii::$app->request->post('city'));
            $model->save(false);

            return $this->redirect(['index']);
        }
        $this->view->registerJsFile('/res/js/mask.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        return $this->render('update', [
            'model' => $model
        ]);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:20
 */

namespace frontend\controllers;


use backend\models\About;
use backend\models\city\City;
use backend\models\city\GuideGallery;
use backend\models\city\GuideItem;
use backend\models\city\GuideMenu;
use backend\models\info\InfoItem;
use backend\models\info\InfoMenu;
use backend\models\Menu;
use backend\models\Plus;
use backend\models\PlusItem;
use backend\models\Team;
use backend\models\TeamItem;
use backend\models\tour\Service;
use backend\models\tour\ServiceItem;
use backend\models\tour\Tour;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MenuController extends Controller
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $slug = Yii::$app->request->get('menu_slug');
        $model = Menu::find()->where(['slug' => $slug])->asArray()->one();
        if (!empty($model)) {
            switch ($model['action']) {
                case 'menu/about':
                    $rowA = About::find()->asArray()->one();
                    $rowP = Plus::find()->asArray()->one();
                    $rowPI = PlusItem::find()->asArray()->all();
                    $rowT = Team::find()->asArray()->one();
                    $rowTI = TeamItem::find()->asArray()->all();

                    return $this->render('page_about', [
                        'model' => $model,
                        'rowA' => $rowA,
                        'rowP' => $rowP,
                        'rowPI' => $rowPI,
                        'rowT' => $rowT,
                        'rowTI' => $rowTI
                    ]);
                    break;
                case 'menu/guide':
                    $rowG = GuideMenu::find()->where(['status' => 1])->orderBy(['number' => SORT_ASC])->asArray()->all();

                    $cities = City::find()->where(['status' => 1])->asArray()->all();
                    $menus = GuideMenu::find()->where(['status' => 1])->asArray()->all();
                    $datas = [];

//                    foreach ($cities as $city) {
//                        $datas[$city['id']]['city'] = $city;
//                        foreach ($menus as $menu){
//                            $items = GuideItem::find()->where(['status'=>1,'guide_menu_id'=>$menu['id'],'city_id'=>$city['id']])->asArray()->limit(3)->all();
//                            $datas[$city['id']][$menu['id']] = $menu;
//                            $datas[$city['id']][$menu['id']]['items'] = $items;
//                        }
//                    }


                    return $this->render('page_guide', [
                        'model' => $model,
                        //'datas' => $datas,
                        //'cities' => $cities,
                        //'menus' => $menus,
                        'rowG' => $rowG
                    ]);
                    break;
                case 'menu/experience':
                    $tours = Tour::find()->limit(4)->asArray()->all();

                    $datas = [];
                    $services = Service::find()->asArray()->all();
                    foreach ($services as $service){
                        $items = ServiceItem::find()->where(['service_id'=>$service['id']])->limit(3)->asArray()->all();
                        if(!empty($items)){
                            $datas[$service['id']] = $items;
                        }
                    }

                    return $this->render('page_exp',[
                        'model'=>$model,
                        'tours'=>$tours,
                        'services'=>$services,
                        'datas'=>$datas
                    ]);
                    break;
                case 'menu/regionalinfo':
                    $rowI = InfoMenu::find()->where(['status' => 1])->orderBy(['number' => SORT_ASC])->asArray()->all();
                    return $this->render('page_info', [
                        'model' => $model,
                        'rowI' => $rowI
                    ]);
                    break;
                case 'menu/application':
                    $lang = Yii::$app->language;
                    switch ($lang){
                        case 'ru':
                            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
                            return $this->render('application/ru',[
                                'model'=>$model
                            ]);
                            break;
                        case 'en':
                            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
                            return $this->render('application/en',[
                                'model'=>$model
                            ]);
                            break;
                        case 'fr':
                            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
                            return $this->render('application/fr',[
                                'model'=>$model
                            ]);
                            break;
                        case 'it':
                            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
                            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
                            return $this->render('application/it',[
                                'model'=>$model
                            ]);
                            break;
                    }

                    break;
            }
        } else {
            throw new NotFoundHttpException();
        }
    }





    /**** about ***/

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUs()
    {
        $model = Menu::find()->where(['action' => 'menu/about'])->asArray()->one();
        if (!empty($model)) {
            $rowA = About::find()->asArray()->one();
            return $this->render('page_about_us', [
                'model' => $model,
                'rowA' => $rowA,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionExcellence()
    {
        $model = Menu::find()->where(['action' => 'menu/about'])->asArray()->one();
        if (!empty($model)) {
            $rowP = Plus::find()->asArray()->one();
            $rowPI = PlusItem::find()->asArray()->all();
            return $this->render('page_about_excellence', [
                'model' => $model,
                'rowP' => $rowP,
                'rowPI' => $rowPI,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionOurteam()
    {
        $model = Menu::find()->where(['action' => 'menu/about'])->asArray()->one();
        if (!empty($model)) {
            $rowT = Team::find()->asArray()->one();
            $rowTI = TeamItem::find()->asArray()->all();
            return $this->render('page_about_ourteam', [
                'model' => $model,
                'rowT' => $rowT,
                'rowTI' => $rowTI,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /*** guide ***/
    public function actionHeaderguide()
    {
        $menu_slug = Yii::$app->request->get('menu_slug');
        $guide_slug = Yii::$app->request->get('guide_slug');

        $model = Menu::find()->where(['slug' => $menu_slug, 'action' => 'menu/guide'])->asArray()->one();
        if (!empty($model)) {
            $guide = GuideMenu::find()->where(['slug' => $guide_slug])->asArray()->one();
            if (!empty($guide)) {

                /*** filter ***/
                if (Yii::$app->request->get('city')) {
                    $get_cities = Yii::$app->request->get('city');
                    $items = [];
                    $f_cities = [];
                    foreach ($get_cities as $get_city) {
                        if (is_numeric($get_city)) {
                            $db = GuideItem::find()->where(['guide_menu_id' => $guide['id'], 'city_id' => $get_city])->orderBy(['top' => SORT_DESC])->asArray()->all();
                        }
                        $items[$get_city] = $db;
                    }

                    $cities = City::find()->where(['status' => 1])->asArray()->all();
                    foreach ($cities as $record){
                        $f_cities[$record['id']] = $record;
                    }

                    return $this->render('page_guide_items_header_filter', [
                        'model' => $model,
                        'guide' => $guide,
                        'items' => $items,
                        'f_cities' => $f_cities,
                        'cities' => $cities
                    ]);
                }
                /**** end filter ***/
                $items = GuideItem::find()->where(['guide_menu_id' => $guide['id']])->orderBy(['top' => SORT_DESC])->asArray()->all();
                $cities = City::find()->where(['status' => 1])->asArray()->all();

                return $this->render('page_guide_items_header', [
                    'model' => $model,
                    'guide' => $guide,
                    'items' => $items,
                    'cities' => $cities
                ]);
            }
        }

    }

    public function actionGuideitems()
    {
        $menu_slug = Yii::$app->request->get('menu_slug');
        $guide_slug = Yii::$app->request->get('guide_slug');
        $city_id = Yii::$app->request->get('city_id');
        if (is_numeric($city_id)) {
            $model = Menu::find()->where(['slug' => $menu_slug, 'action' => 'menu/guide'])->asArray()->one();
            if (!empty($model)) {
                $guide = GuideMenu::find()->where(['slug' => $guide_slug])->asArray()->one();
                if (!empty($guide)) {
                    $city = City::find()->where(['id' => $city_id])->asArray()->one();
                    if (!empty($city)) {
                        $items = GuideItem::find()->where(['guide_menu_id' => $guide['id'], 'city_id' => $city_id])->orderBy(['top' => SORT_DESC])->asArray()->all();
                        return $this->render('page_guide_items', [
                            'model' => $model,
                            'guide' => $guide,
                            'items' => $items,
                            'city' => $city
                        ]);

                    }
                }
            }
        }

    }


    public function actionGuideitem()
    {
        $menu_slug = Yii::$app->request->get('menu_slug');
        $guide_slug = Yii::$app->request->get('guide_slug');
        $item_slug = Yii::$app->request->get('item_slug');

        $model = Menu::find()->where(['slug' => $menu_slug, 'action' => 'menu/guide'])->asArray()->one();
        if (!empty($model)) {
            $guide = GuideMenu::find()->where(['slug' => $guide_slug])->asArray()->one();
            if (!empty($guide)) {
                $item = GuideItem::find()->where(['guide_menu_id' => $guide['id'], 'slug' => $item_slug])->asArray()->one();
                if (!empty($item)) {
                    $images = GuideGallery::find()->where(['guide_item_id' => $item['id']])->orderBy(['id' => SORT_DESC])->asArray()->all();
                    return $this->render('page_guide_item', [
                        'model' => $model,
                        'guide' => $guide,
                        'item' => $item,
                        'images' => $images
                    ]);
                }
            }
        }
    }


    /*** info ***/

    public function actionInfoitem()
    {
        $menu_slug = Yii::$app->request->get('menu_slug');
        $info_slug = Yii::$app->request->get('info_slug');

        $model = Menu::find()->where(['slug' => $menu_slug, 'action' => 'menu/regionalinfo'])->asArray()->one();
        if (!empty($model)) {
            $info = InfoMenu::find()->where(['slug' => $info_slug])->asArray()->one();
            if (!empty($info)) {
                $item = InfoItem::find()->where(['info_menu_id' => $info['id']])->asArray()->one();
                if (!empty($item)) {
                    return $this->render('page_info_item', [
                        'model' => $model,
                        'item' => $item
                    ]);

                }
            }
        }

    }
}
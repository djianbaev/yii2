<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 22:40
 */

/* @var $this yii\web\View*/

$lang = Yii::$app->language;

$this->title = 'Добро пожаловать';
?>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846a261e3d7.jpg);"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">

            </div>
        </div>
    </div>
</section>

<section class="services-section-four">
    <div class="auto-container">
        <div class="inner-container">
            <div class="title-box">
                <h2>Добро пожаловать<br>Мы рады видеть вас в наших рядах</h2>
            </div>
            <div class="service-list">
                <ul class="clearfix">
                    <li><span class="icon flaticon-interface-1"></span> Регистрация</li>
                    <li><span class="icon flaticon-interface-1"></span> Отправка соообщения</li>
                    <li><span class="icon flaticon-delete-cross"></span> Подтверждение</li>
                </ul>
            </div>
            <div class="text">Для завершения регистрации , просим Вас проверить указанный вами почтовый адрес</div>
            <div class="link-box">

            </div>
        </div>
    </div>
</section>

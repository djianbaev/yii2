<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 22:40
 */

use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$lang = Yii::$app->language;

$this->title = 'Произошла ошибка';
?>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846a261e3d7.jpg);"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">

            </div>
        </div>
    </div>
</section>

<section class="services-section-four">
    <div class="auto-container">
        <div class="inner-container">
            <div class="title-box">
                <h2>Произошла ошибка!<br></h2>
            </div>
            <div class="service-list">
                <ul class="clearfix">
                    <li><span class="icon flaticon-interface-1"></span> Регистрация</li>
                    <li><span class="icon flaticon-interface-1"></span> Отправка соообщения</li>
                    <li><span class="icon flaticon-delete-cross"></span> Подтверждение</li>
                </ul>
            </div>
            <div class="text">Произошла ошибка во время подтверждения аккаунта. Пожалуйста запросите ссылку для
                подтверждения повторно!
            </div>
            <div class="default-form comment-form">
                <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>
                <div class="row clearfix">

                    <div class="col-md-4"></div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <?= $form->field($model, 'email')->textInput(['placeholder'=>'youremail@mail.com','class'=>'text-center'])->label(false) ?>

                        <div class="link-box">
                            <button type="submit" class="theme-btn btn-style-five" style="background: #FFFFFF;">
                                <div class="btn-title">Запросить!</div>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-4"></div>

                </div>
                <?php ActiveForm::end(); ?>
            </div>



        </div>
    </div>
</section>



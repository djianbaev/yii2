<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 22:04
 */

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = 'Аккаунт';
?>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846a261e3d7.jpg);"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">

            </div>
        </div>
    </div>
</section>


<section class="yacht-tours-container yacht-details">
    <div class="auto-container">
        <div class="main-title"><h3>Аккаунт</h3></div>

        <ul class="nav nav-tabs" id="accountTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru">Ваши
                    данные</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en">История
                    бронирования</a>
            </li>
        </ul>

        <div class="tab-content lv-tab" id="accountTabs">
            <div class="tab-pane fade in active show" id="ru" role="tabpanel" aria-labelledby="ru-tab">
                <div class="auto-container">

                    <div class="row clearfix">

                        <div class="specs-column col-xl-5 col-lg-6 col-md-12 cl-sm-12" style="margin-top: 50px;">
                            <div class="inner" style="padding-left: unset;">
                                <div class="title-box clearfix">
                                    <h4>Основное</h4>
                                    <div class="options clearfix">
                                        <?php if (!empty($data)): ?>
                                            <a href="/edit-my-data" class="theme-btn print-it"><span
                                                        class="icon flaticon-edit-document"></span> Изменить</a>
                                        <?php else: ?>
                                            <a href="/create-my-data" class="theme-btn print-it"><span
                                                        class="icon flaticon-edit-document"></span> Изменить</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="features-list">
                                    <ul>
                                        <li class="clearfix"><span class="ttl">Username/Login</span><span
                                                    class="dtl"><?php echo Yii::$app->user->identity->username; ?></span>
                                        </li>
                                        <li class="clearfix"><span class="ttl">Email</span><span
                                                    class="dtl"><?php echo Yii::$app->user->identity->email ?></span>
                                        </li>
                                        <?php if (!empty($data)): ?>
                                            <li class="clearfix"><span class="ttl">Имя</span><span
                                                        class="dtl"><?php echo $data['name'] ?></span></li>
                                            <li class="clearfix"><span class="ttl">Фамилия</span><span
                                                        class="dtl"><?php echo $data['surname'] ?></span></li>
                                            <li class="clearfix"><span class="ttl">Год Рождения</span><span
                                                        class="dtl"><?php echo $data['bday'] ?></span></li>
                                            <li class="clearfix"><span class="ttl">Страна</span><span
                                                        class="dtl"><?php echo $data['country'] ?></span></li>
                                            <li class="clearfix"><span class="ttl">Город</span><span
                                                        class="dtl"><?php echo $data['city'] ?></span></li>
                                            <li class="clearfix"><span class="ttl">Номер Телефона</span><span
                                                        class="dtl"><?php echo $data['phone'] ?></span></li>
                                        <?php else: ?>
                                            <li class="clearfix"><span class="ttl">Имя</span><span class="dtl">-</span>
                                            </li>
                                            <li class="clearfix"><span class="ttl">Фамилия</span><span
                                                        class="dtl">-</span></li>
                                            <li class="clearfix"><span class="ttl">Год Рождения</span><span class="dtl">-</span>
                                            </li>
                                            <li class="clearfix"><span class="ttl">Страна</span><span
                                                        class="dtl">-</span></li>
                                            <li class="clearfix"><span class="ttl">Город</span><span
                                                        class="dtl">-</span></li>
                                            <li class="clearfix"><span class="ttl">Номер Телефона</span><span
                                                        class="dtl">-</span></li>
                                        <?php endif; ?>


                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane fade in" id="en" role="tabpanel" aria-labelledby="en-tab">
                <div class="row">
                    <div class="news-list">
                        <div class="news-block-five">
                            <div class="inner-box">
                                <div class="row clearfix">
                                    <?php if (!empty($books)): ?>
                                        <?php foreach ($books as $book): ?>
                                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                                <div class="inner" style="min-height: unset;">
                                                    <div class="content">
                                                        <div class="meta-info">
                                                            <ul class="clearfix">
                                                                <li>
                                                                    <span class="icon fa fa-calendar-check"></span>
                                                                    <a href="#"><?php echo $book['created_date']?></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <h3><?php echo $book['booking_'.$lang]?></h3>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
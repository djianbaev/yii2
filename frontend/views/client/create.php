<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 16.04.2020
 * Time: 18:24
 */
/* @var $this yii\web\View*/

$lang = Yii::$app->language;
$this->title = 'Изменить данные';
?>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846a261e3d7.jpg);"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Изменить данные</h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Главная</a></li>
                        <li><a href="/account">Аккаунт</a></li>
                        <li>Изменить данные</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-section">
    <div class="auto-container">

        <div class="contact-container">
            <div class="row clearfix">

                <div class="form-column col-lg-12 col-md-12 col-sm-12">
                    <div class="inner wow fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                        <div class="sec-title">
                            <h3 style="text-transform: uppercase;">Ваши данные не будут доступны для посторонних лиц</h3>
                        </div>
                        <div class="default-form contact-form">
                            <form method="post" id="client-data-form">
                                <input type="hidden" name="_csrf-frontend" value="AaWKkvBRF-FQ8v6OdHa__JQBJuRFD6PF8DV7R-IrTIdQlN3GtBN5lQ-Cnbk8PteMzGxCg3Zj6q64fyRwpXsEwA==">
                                <div class="row clearfix">
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Имя</div>
                                        <div class="field-inner">
                                            <input type="text" name="name" placeholder="Иван" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Фамилия</div>
                                        <div class="field-inner">
                                            <input type="text" name="surname" placeholder="Иванов" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Дата рождения</div>
                                        <div class="field-inner">
                                            <input type="text" name="bday" id="bday" placeholder="01/01/1993" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Номер телефона</div>
                                        <div class="field-inner">
                                            <input type="text" name="phone" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Страна</div>
                                        <div class="field-inner">
                                            <input type="text" name="country" placeholder="Россия" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <div class="field-label">Город</div>
                                        <div class="field-inner">
                                            <input type="text" name="city" placeholder="Москва" required="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="theme-btn btn-style-three"><span class="btn-title">Отправить</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<?php
$js = <<< JS
$('#bday').mask('00/00/0000');
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>
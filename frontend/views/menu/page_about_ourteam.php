<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $rowT['title_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $rowT['title_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><a href="<?php echo Url::to(['menu/index','menu_slug'=>$model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a></li>
                        <li><?php echo $rowT['title_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<!--team-->
<section class="team-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2><?php echo $rowT['title_' . $lang] ?></h2>
            <div class="text"><?php echo $rowT['text_' . $lang] ?></div>
        </div>

        <div class="team-box">
            <div class="row clearfix">
                <?php if (!empty($rowTI)): ?>
                    <?php $sec = 0; ?>
                    <?php for ($i = 0; $i < count($rowTI); $i++): ?>
                        <?php $sec += 500; ?>

                        <div class="team-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms"
                             data-wow-duration="<?php echo (1000+$sec);?>ms">
                            <div class="inner-box">
                                <div class="image-box">
                                    <a href="#"><img src="<?php echo $rowTI[$i]['image']?>" alt="" title=""></a>
                                </div>
                                <div class="lower-box clearfix">
                                    <div class="lower-content">
                                        <h4><a href="#"><?php echo str_replace(' ','<br>',$rowTI[$i]['fio_'.$lang]);?></a></h4>
                                        <div class="designation"><?php echo $rowTI[$i]['position_'.$lang]?></div>
                                        <div class="designation"><i class="flaticon-envelope"></i> <?php echo $rowTI[$i]['email']?></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>


    </div>
</section>
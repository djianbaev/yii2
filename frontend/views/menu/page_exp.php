<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 10.04.2020
 * Time: 0:41
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
    <section class="page-banner">
        <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <h1><?php echo $model['name_' . $lang] ?></h1>
                    <div class="page-nav">
                        <ul class="bread-crumb clearfix">
                            <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                            <li><?php echo $model['name_' . $lang] ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="activities-section">
        <div class="auto-container">
            <div class="sec-title centered with-border">
                <div class="upper-text"><?php echo Yii::$app->params['expTitle'][$lang]?></div>
                <h2><?php echo Yii::$app->params['expSubTitle'][$lang]?></h2>
                <div class="separator"></div>
            </div>
            <div class="carousel-outer">
                <div class="activity-carousel owl-theme owl-carousel">

                    <?php foreach ($tours as $tour): ?>
                        <div class="activity-block-two">
                            <div class="inner-box clearfix">
                                <div class="image-layer"
                                     style="background-image:url(<?php echo $tour['image'] ?>);"></div>

                                <div class="image-box">
                                    <a href="<?php echo Url::to(['site/tourview','tour_slug'=>$tour['slug']])?>"><img src="<?php echo $tour['image'] ?>" alt="" title=""></a>
                                </div>

                                <div class="content-box">
                                    <div class="content">
                                        <h4><a href="<?php echo Url::to(['site/tourview','tour_slug'=>$tour['slug']])?>"><?php echo $tour['name_' . $lang] ?></a></h4>
                                        <div class="info">
                                            <ul class="clearfix">
                                                <li>
                                                    <span class="icon flaticon-wall-clock"></span><?php echo $tour['days_night_' . $lang] ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="links-box clearfix">
                                        <div class="link">
                                            <a href="<?php echo Url::to(['site/tourview','tour_slug'=>$tour['slug']])?>" class="theme-btn btn-style-two"><span
                                                        class="btn-title"><?php echo Yii::$app->params['expBook'][$lang]?></span></a>
                                        </div>
                                    </div>
                                    <div class="pricing">
                                        <div class="price">
                                            <div class="price-title"><?php echo Yii::$app->params['expPriceFrom'][$lang]?></div>
                                            <div class="unit"><span>$<?php echo $tour['price'] ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>


                </div>
            </div>

            <div class="see-all clearfix">
                <a href="/tours-packages" class="theme-btn btn-style-five">
                    <div class="btn-title"><?php echo Yii::$app->params['expAllTour'][$lang]?></div>
                </a>
            </div>
        </div>
    </section>

<?php if (!empty($datas)): ?>
    <?php foreach ($services as $service): ?>
        <?php if (array_key_exists($service['id'], $datas)): ?>
            <section class="destinations-two">


                <div class="auto-container">
                    <div class="sec-title centered">
                        <h2><?php echo $service['name_' . $lang] ?></h2>
                    </div>
                    <div class="popular-packages">
                        <div class="title-row clearfix">
                            <div class="link-box">
                                <a href="<?php echo Url::to(['site/service','service_id'=>$service['id']])?>" class="default-link">
                                    <span class="icon flaticon-logout"></span>
                                    <span class="link-text"><?php echo Yii::$app->params['wShowAll'][$lang]?></span>
                                </a>
                            </div>
                        </div>
                        <div class="packages-box">
                            <div class="row clearfix">

                                <?php foreach ($datas[$service['id']] as $data): ?>
                                    <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp"
                                         data-wow-delay="0ms"
                                         data-wow-duration="1500ms">
                                        <div class="inner-box clearfix">
                                            <div class="image-box">
                                                <img src="<?php echo $data['image']?>" alt="" title="">
                                                <div class="hover-box">
                                                    <div class="hover-link">
                                                        <a href="<?php echo Url::to(['site/serviceitem','service_item_id'=>$data['id']])?>" class="default-link">
                                                            <span class="icon flaticon-logout"></span>
                                                            <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="content-box">
                                                <div class="content">
                                                    <div class="package-title">
                                                        <a href="<?php echo Url::to(['site/serviceitem','service_item_id'=>$data['id']])?>"><?php echo $data['name_'.$lang]?></a>
                                                    </div>
                                                    <div class="info">
                                                        <ul class="clearfix">
                                                            <li><span class="icon flaticon-wall-clock"></span>
                                                                <?php echo $data['date']?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="links-box clearfix">
                                                        <div class="pricing">
                                                            <div class="price">
                                                                <div class="price-title"></div>
                                                                <div class="unit"><span>$<?php echo $data['price']?></span></div>
                                                            </div>
                                                        </div>
                                                        <div class="link">
                                                            <div class="info-btn-box">
                                                                <div class="theme-btn info-btn"><span
                                                                            class="icon flaticon-information-1"></span>
                                                                </div>
                                                                <div class="info-panel">
                                                                    <div class="panel-inner">
                                                                        <ul class="panel-list">
                                                                            <?php echo $data['intro_'.$lang]?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
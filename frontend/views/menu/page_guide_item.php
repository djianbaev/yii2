<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $item['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $item['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><a href="<?php echo Url::to(['menu/index','menu_slug'=>$model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a></li>
                        <li><a href="<?php echo Url::to(['menu/headerguide','menu_slug'=>$model['slug'],'guide_slug'=>$guide['slug']]) ?>">
                                <?php echo $guide['name_' . $lang] ?></a></li>
                        <li><?php echo $item['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <div class="content-side col-lg-12 col-md-12 col-sm-12" style="margin-bottom: unset;">
                <div class="blog-content">
                    <div class="news-post-details">

                        <!--News Block-->
                        <div class="news-block-six">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure class="image">
                                        <img src="<?php echo $item['image']?>" alt="" title="">
                                    </figure>
                                </div>
                                <div class="content-box">
                                    <div class="inner">
                                        <div class="content lv-content">
                                            <h3><?php echo $item['name_'.$lang]?></h3>
                                            <div class="text">
                                                <?php echo $item['text_'.$lang]?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<?php if(!empty($images)):?>
<section class="portfolio-section portfolio-two-column" style="padding: unset;">
    <div class="auto-container">
        <div class="row clearfix">
            <?php foreach ($images as $image):?>
            <div class="gallery-block-two col-lg-4 col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image">
                            <a class="lightbox-image" href="<?php echo $image['image']?>"><img src="<?php echo $image['image']?>" alt=""></a>
                        </figure>
                        <div class="zoom-btn">
                            <a class="lightbox-image zoom-link" href="<?php echo $image['image']?>" data-fancybox="gallery"><span class="icon flaticon-expand"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>

    </div>
</section>
<?php endif;?>
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="destinations-two">
    <div class="auto-container">
        <div class="results-row">
            <div class="title-row clearfix">
                <div class="sec-title">
                    <h2><?php echo $model['name_' . $lang] ?></h2>
                </div>
            </div>
            <div class="row clearfix">
                <?php if (!empty($rowI)): ?>
                    <?php foreach ($rowI as $info): ?>
                        <div class="rental-block col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure class="image">
                                        <a href="<?php echo Url::to(['menu/infoitem','menu_slug'=>$model['slug'],'info_slug'=>$info['slug']])?>"><img
                                                    src="<?php echo $info['image']?>" alt=""
                                                    title=""></a>
                                    </figure>

                                </div>
                                <div class="title-box">
                                    <div class="more-link-box">
                                        <a href="<?php echo Url::to(['menu/infoitem','menu_slug'=>$model['slug'],'info_slug'=>$info['slug']])?>" class="theme-btn">
                                            <?php echo Yii::$app->params['mLearnMore'][$lang]?>
                                        </a>
                                    </div>
                                    <h4><a href="<?php echo Url::toRoute(['menu/infoitem','menu_slug'=>$model['slug'],'info_slug'=>$info['slug']])?>"><?php echo $info['name_'.$lang]?></a></h4>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </div>
    </div>
</section>
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
<style>
    .accordion-box .block .acc-btn {
        position: relative;
        font-size: 40px;
        line-height: 150px;
        font-weight: 600;
        cursor: pointer;
        padding: 15px 0px 10px;
        padding-right: 20px;
        padding-left: 80px;
        color: #d4ae25;
        text-transform: uppercase;
        background-color: #ffffff;
        -webkit-transition: all 500ms ease;
        -moz-transition: all 500ms ease;
        -ms-transition: all 500ms ease;
        -o-transition: all 500ms ease;
        transition: all 500ms ease;
    }

    .accordion-box .block .acc-btn:before {
        position: absolute;
        left: 0;
        top: 15px;
        text-align: center;
        font-size: 20px;
        line-height: 30px;
        color: #a2a6ab;
        font-weight: 400;
        font-family: "Flaticon";
        content: unset;
    }
</style>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="destinations-two">
    <div class="auto-container">
        <div class="popular-packages">
            <div class="title-row clearfix">
                <img src="/res/images/uzbekistan-map.png" class="img-responsive"/>
            </div>
        </div>
    </div>
</section>


<section class="destinations-two">
    <div class="auto-container">

        <div class="included-material">
            <div class="included-box">
                <div class="accordion-box">

                    <?php for ($i = 0; $i < count($cities); $i++): ?>

                        <div class="accordion block">
                            <div class="acc-btn"
                                 style="background: url(<?php echo $cities[$i]['map'] ?>);background-repeat: no-repeat;background-size: contain;">
                                <?php echo $cities[$i]['name_' . $lang] ?>
                                <div class="icon flaticon-down-arrow"></div>
                            </div>
                            <div class="acc-content">
                                <div class="content">
                                    <div class="auto-container">

                                        <?php if (!empty($menus)): ?>
                                            <div class="popular-packages">
                                                <div class="packages-box">
                                                    <div class="row clearfix">
                                                        <?php foreach ($menus as $menu): ?>

                                                            <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms"
                                                                 data-wow-duration="1500ms">
                                                                <div class="inner-box clearfix">
                                                                    <div class="image-box">
                                                                        <img src="<?php echo $menu['image']?>" alt="" title="">
                                                                        <div class="hover-box">
                                                                            <div class="hover-link">
                                                                                <a href="<?php echo Url::to(['menu/guideitems','menu_slug'=>$model['slug'],'city_id'=>$cities[$i]['id'],'guide_slug'=>$menu['slug']])?>" class="default-link">
                                                                                    <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="content-box">
                                                                        <div class="content">
                                                                            <div class="package-title text-center">
                                                                                <a href="<?php echo Url::to(['menu/guideitems','menu_slug'=>$model['slug'],'city_id'=>$cities[$i]['id'],'guide_slug'=>$menu['slug']])?>"><?php echo $menu['name_'.$lang]?></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php endfor; ?>


                </div>


            </div>
        </div>


    </div>
</section>




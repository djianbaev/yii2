<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $guide['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $city['name_'.$lang].'-'.$guide['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><a href="<?php echo Url::to(['menu/index','menu_slug'=>$model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a></li>
                        <li><?php echo $city['name_'.$lang].'-'.$guide['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="destinations-two">
    <div class="auto-container">
        <div class="popular-packages">
            <div class="title-row clearfix">
                <div class="sec-title">
                    <h2><?php echo $city['name_'.$lang].'-'.$guide['name_' . $lang] ?></strong></h2>
                </div>
            </div>
            <div class="packages-box">
                <div class="row clearfix">
                    <?php if (!empty($items)): ?>
                        <?php for ($i = 0; $i < count($items); $i++): ?>
                            <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="<?php echo ($i*300)?>ms"
                                 data-wow-duration="1500ms">
                                <div class="inner-box clearfix">
                                    <div class="image-box">
                                        <img src="<?php echo $items[$i]['image']?>" alt="" title="">
                                        <div class="hover-box">
                                            <div class="hover-link">
                                                <a href="<?php echo Url::to(['menu/guideitem','menu_slug'=>$model['slug'],'guide_slug'=>$guide['slug'],'item_slug'=>$items[$i]['slug']])?>" class="default-link">
                                                    <span class="icon flaticon-logout"></span>
                                                    <span class="link-text">Показать всё</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="content-box">
                                        <div class="content">
                                            <div class="package-title text-center">
                                                <a href="<?php echo Url::to(['menu/guideitem','menu_slug'=>$model['slug'],'guide_slug'=>$guide['slug'],'item_slug'=>$items[$i]['slug']])?>"><?php echo $items[$i]['name_'.$lang]?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
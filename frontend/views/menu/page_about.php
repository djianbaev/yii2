<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */
/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!--about-->

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2><?php  $rowA['title_' . $lang] ?></h2>
            <div class="text"><?php echo $rowA['text_' . $lang] ?></div>
        </div>
        <div class="image-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <img src="<?php echo $rowA['image'] ?>" alt="">
        </div>
    </div>
</section>


<!--features-->

<section class="features-section">
    <div class="auto-container">
        <div class="row clearfix">
            <?php if (!empty($rowPI)): ?>
                <?php foreach ($rowPI as $plus): ?>
                    <div class="feature-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="<?php echo $plus['image'] ?>"/>
                            </div>
                            <div class="content">
                                <h4><?php echo $plus['name_' . $lang] ?></h4>
                                <div class="text"><?php echo $plus['text_' . $lang] ?></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>


<!--team-->
<section class="team-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2><?php echo $rowT['title_' . $lang] ?></h2>
        </div>

        <div class="team-box">
            <div class="row clearfix">
                <?php if (!empty($rowTI)): ?>
                    <?php for ($i = 0; $i < count($rowTI); $i++): ?>
                        <div class="team-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="<?php echo ($i*300)?>ms"
                             data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="image-box">
                                    <a href="#"><img src="<?php echo $rowTI[$i]['image']?>" alt="" title=""></a>
                                </div>
                                <div class="lower-box clearfix">
                                    <div class="lower-content">
                                        <h4><a href="#"><?php echo str_replace(' ','<br>',$rowTI[$i]['fio_'.$lang]);?></a></h4>
                                        <div class="designation"><?php echo $rowTI[$i]['position_'.$lang]?></div>
                                        <div class="designation"><i class="flaticon-envelope"></i> <?php echo $rowTI[$i]['email']?></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>


    </div>
</section>

<?php echo \frontend\components\SubscribeWidget::widget();?>
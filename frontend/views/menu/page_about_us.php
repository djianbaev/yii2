<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 03.04.2020
 * Time: 14:40
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $rowA['title_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $rowA['title_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><a href="<?php echo Url::to(['menu/index','menu_slug'=>$model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a></li>
                        <li><?php echo $rowA['title_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!--about-->

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2><?php  $rowA['title_' . $lang] ?></h2>
            <div class="text"><?php echo $rowA['text_' . $lang] ?></div>
        </div>
        <div class="image-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <img src="<?php echo $rowA['image'] ?>" alt="">
        </div>
    </div>
</section>


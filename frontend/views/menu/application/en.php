<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 11.04.2020
 * Time: 22:31
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
<style>
    label {
        font-size: 18px;
        font-weight: 500;
    }

    input[type="radio"] {

    }

    .dates-input {
        display: none;
    }

    .operator-time, .div-form-more {
        display: none;
    }

    #form_more {
        color: #d4ae25;
        cursor: pointer;
    }

    h4 {
        margin-top: 20px;
    }

    th {
        text-transform: uppercase;
    }
</style>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Home</a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2>Fill out the questionnaire and tell us about your wishes in 2 minutes,
                and get the possible travel options.
            </h2>
        </div>
        <div class="default-form comment-form">
            <form action="/send-application" method="post" id="application-form">
                <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>" />

                <h4>Your coordinates</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="name" placeholder="First Name" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="surname" placeholder="Last Name" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="Email" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Phone Number" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="country" placeholder="Living Country" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="city" placeholder="City" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="zipcode" placeholder="Postal Code" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="male" id="male" required>
                        <label for="male">Male</label>
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="female" id="female">
                        <label for="female">Female</label>
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="other" id="othergender">
                        <label for="othergender">Do not indicate</label>
                    </div>
                </div>

                <h4>Place of departure</h4>
                <div class="row clearfix">
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_country" placeholder="Country" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_city" placeholder="City" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_airport" placeholder="Airport" required="">
                    </div>
                </div>

                <h4>Do you know the exact dates of your trip?</h4>
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-6 form-group">
                        <input type="radio" name="d_dates" id="d_dates_yes" value="yes"/>
                        <label for="d_dates_yes">Yes</label>
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_from" placeholder="From">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_to" placeholder="Until">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" name="d_days" placeholder="Total amount of days ">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="radio" name="d_dates" id="d_dates_no" value="no"/>
                        <label for="d_dates_no">No</label>
                    </div>

                </div>

                <h4>Number of travelers </h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="adults" placeholder="Adults" required>
                    </div>
                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="kids" placeholder="Kids">
                    </div>

                </div>


                <h4>How do you want us to get in touch with you?</h4>
                <div class="row clearfix">

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_phone" value="phone">
                        <label for="check_phone">Through the phone call </label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_email" value="email">
                        <label for="check_email">Through E-mail </label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_whatsapp" value="whatsapp">
                        <label for="check_whatsapp">Through WhatsApp</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_viber" value="viber">
                        <label for="check_viber">Through Viber</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_imo" value="imo">
                        <label for="check_imo">Through Imo</label>
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="connect_time" placeholder="Your hourly preferences (14:00)" required>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_1" id="agree_1">
                        <label for="agree_1">I agree to receive the newsletter</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_2" id="agree_2" required>
                        <label for="agree_2">I have read and accept the <a href="#">general terms</a> of use of the website</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_3" id="agree_3">
                        <label for="agree_3">I agree to accept tour information, blog articles via Email</label>
                    </div>
                </div>


                <h4>For more information, click on <span id="form_more">MORE</span></h4>
                <br><br>

                <div class="div-form-more">
                    <h4>Where are you planning to travel?</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <label>*If you want combined tour package in Central Asia, mention the country and cities</label>
                            <input type="text" name="uwant" placeholder="Country, Cities">
                        </div>


                    </div>


                    <h4>Your journey </h4>
                    <div class="row clearfix">

                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_couple" value="couple">
                            <label for="check_couple">As a couple</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_family" value="family">
                            <label for="check_family">With family</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_friends" value="friends">
                            <label for="check_friends">With friends</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_group" value="group">
                            <label for="check_group">In a group (Club, Association…)</label>
                        </div>
                    </div>


                    <h4>Do you agree to join the existing group? </h4>
                    <h5>*Travel in group could be more chip</h5>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="join[]" id="join_yes" value="yes" required/>
                            <label for="join_yes">Yes</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="join[]" id="join_no" value="no"/>
                            <label for="join_no">No</label>
                        </div>

                    </div>

                    <h4>Are there any travelers with disabilities? </h4>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="invalid[]" id="invalid_yes" value="yes" required/>
                            <label for="invalid_yes">Yes</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="invalid[]" id="invalid_no" value="no"/>
                            <label for="invalid_no">No</label>
                        </div>

                    </div>


                    <h4>At what stage are you planning your trip?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_1" value="1" required/>
                            <label for="plan_1">My destination has already been picked, I want to book my trip faster</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_2" value="2"/>
                            <label for="plan_2">My destination is already chosen, I'm starting to organize my journey </label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_3" value="3"/>
                            <label for="plan_3">I need more information about travel</label>
                        </div>

                    </div>


                    <h4>What level of comfort do you want to choose?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_1" value="1" required/>
                            <label for="house_1">Middle and not expensive</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_2" value="2"/>
                            <label for="house_2">Charm and comfort</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_3" value="3"/>
                            <label for="house_3">Lux </label>
                        </div>
                    </div>


                    <h4>For your meals, would you like the tour operator to offer you</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_1" value="1" required/>
                            <label for="food_1">Accommodation with breakfast</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_2" value="2"/>
                            <label for="food_2">Half board </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_3" value="3"/>
                            <label for="food_3">Full board</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_4" value="4"/>
                            <label for="food_4">Selectively lunches and dinners in extraordinary places </label>
                        </div>
                    </div>


                    <h4>For your movements during your journey would you like </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_1" value="1" required/>
                            <label for="transport_1">Have an organized transfer ( transfers to airport and train station)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_2" value="2"/>
                            <label for="transport_2">Have a car with a driver provided by the agency</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_3" value="3"/>
                            <label for="transport_3">Rental car</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_4" value="4"/>
                            <label for="transport_4">Partially</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_5" value="5"/>
                            <label for="transport_5">You would like move by yourself </label>
                        </div>
                    </div>

                    <h4>Would you like to be accompanied by a guide?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_yes" value="yes" required/>
                            <label for="guide_yes">Yes</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_no" value="no"/>
                            <label for="guide_no">No</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_intime" value="intime"/>
                            <label for="guide_intime">Partially (in certain cities)</label>
                        </div>
                    </div>


                    <h4>Your guide must speak</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_ru" value="ru"/>
                            <label for="lang_ru">Russian</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_en" value="en"/>
                            <label for="lang_en">English  </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_fr" value="fr"/>
                            <label for="lang_fr">French</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_it" value="it"/>
                            <label for="lang_it">Italian</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_de" value="de"/>
                            <label for="lang_de">German</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="text" name="langother" placeholder="Others"/>
                        </div>
                    </div>



                    <h4>What are your wishes </h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">Cultural heritage</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_1" value="1">
                                    <label for="culture_1">Museums</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_2" value="2">
                                    <label for="culture_2">Galleries</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_3" value="3">
                                    <label for="culture_3">Historical places</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_4" value="4">
                                    <label for="culture_4">National parks</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">Sport and recreation </h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_1" value="1">
                                    <label for="sport_1">Tracking </label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_2" value="2">
                                    <label for="sport_2">Walks around </label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_3" value="3">
                                    <label for="sport_3">City trip</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_4" value="4">
                                    <label for="sport_4">Photos</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25; text-align: center;">Relax</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_1" value="1">
                                    <label for="relax_1">Gastronomy</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_2" value="2">
                                    <label for="relax_2">Performances</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_3" value="3">
                                    <label for="relax_3">Handicraft</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_4" value="4">
                                    <label for="relax_4">Painting/Art</label>
                                </div>
                            </div>
                        </div>


                    </div>


                    <h4>Tell us about your travel project</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <textarea type="text" name="project" placeholder="Type here..."></textarea>
                        </div>

                    </div>


                    <h4>Your approximate budget </h4>
                    <div class="row clearfix">

                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budget" placeholder="5000$">
                        </div>
                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budgetdays" placeholder="12(days)">
                        </div>

                    </div>


                    <h4>How did you find out about us?</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_1" value="1">
                            <label for="source_1">Google</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_2" value="2">
                            <label for="source_2">Yandex</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_3" value="3">
                            <label for="source_3">Facebook</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_4" value="4">
                            <label for="source_4">Instagram</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_5" value="5">
                            <label for="source_5">Twitter</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_6" value="6">
                            <label for="source_6">Others</label>
                        </div>
                    </div>

                </div>




                <div class="col-md-12 col-sm-12 form-group" style="margin-top: 20px;">
                    <button type="submit" class="col-md-12 col-sm-12 theme-btn btn-style-five"
                            style="background: #ffffff;">
                        <span class="btn-title">LET’S GET STARTED</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php
$js = <<< JS
$('input:radio[name="d_dates"]').change(function () {
    var inputs = $('.dates-input');    
    if($(this).is(':checked') && $(this).val()=='yes'){
            inputs.css('display','block');
        }else {
            inputs.css('display','none');
        }
    });
$('#form_more').on('click',function() {
  var more = $('.div-form-more');
  more.css('display','block');
});

$('#application-form').on('submit',function(e) {
  swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
});

JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

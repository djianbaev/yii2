<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 11.04.2020
 * Time: 22:31
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
<style>
    label {
        font-size: 18px;
        font-weight: 500;
    }

    input[type="radio"] {

    }

    .dates-input {
        display: none;
    }

    .operator-time, .div-form-more {
        display: none;
    }

    #form_more {
        color: #d4ae25;
        cursor: pointer;
    }

    h4 {
        margin-top: 20px;
    }

    th {
        text-transform: uppercase;
    }
</style>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Pagina iniziale</a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2>Compili il questionario e comunicateci i vostri desideri in 2 minuti, e otterrete le possibili opzioni di viaggio</h2>
        </div>
        <div class="default-form comment-form">
            <form action="/it/send-application" method="post" id="application-form">
                <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>" />

                <h4>Le vostre coordinate</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="name" placeholder="Nome" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="surname" placeholder="Cognome" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="E -mail" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Numero di telefono" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="country" placeholder="Paese" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="city" placeholder="Città" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="zipcode" placeholder="Codice postale" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="male" id="male" required>
                        <label for="male">Maschile</label>
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="female" id="female">
                        <label for="female">Femminile</label>
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="other" id="othergender">
                        <label for="othergender">Non voglio menzionare</label>
                    </div>
                </div>

                <h4>Luogo di partenza</h4>
                <div class="row clearfix">
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_country" placeholder="Paese" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_city" placeholder="Città" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_airport" placeholder="Aeroporto" required="">
                    </div>
                </div>

                <h4>Conosce le date esatte del Suo viaggio ?</h4>
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-6 form-group">
                        <input type="radio" name="d_dates" id="d_dates_yes" value="yes"/>
                        <label for="d_dates_yes">Sì</label>
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_from" placeholder="Dal ">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_to" placeholder="all ">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" name="d_days" placeholder="Quanti giorni">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="radio" name="d_dates" id="d_dates_no" value="no"/>
                        <label for="d_dates_no">No </label>
                    </div>

                </div>

                <h4>Il numero di viaggiatori </h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="adults" placeholder="Adulti" required>
                    </div>
                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="kids" placeholder="Bambini">
                    </div>

                </div>


                <h4>Come vuole che ci mettiamo in contatto con Lei?</h4>
                <div class="row clearfix">

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_phone" value="phone">
                        <label for="check_phone">Telefono </label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_email" value="email">
                        <label for="check_email">Эл.почта</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_whatsapp" value="whatsapp">
                        <label for="check_whatsapp">WhatsApp</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_viber" value="viber">
                        <label for="check_viber">Viber</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_imo" value="imo">
                        <label for="check_imo">IMO</label>
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="connect_time" placeholder="Le Sue preferenze orarie (14:00)" required>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_1" id="agree_1">
                        <label for="agree_1">Accetto la newsletter</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_2" id="agree_2" required>
                        <label for="agree_2">Ho letto e accetto le condizioni generali di <a href="#">utilizzo del sito</a></label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_3" id="agree_3">
                        <label for="agree_3">Accetto informazioni sul tour, articoli sul blog via e-mail</label>
                    </div>
                </div>


                <h4>Per maggiori informazioni, cliccare su <span id="form_more">PIÙ</span></h4>
                <br><br>

                <div class="div-form-more">
                    <h4>Dove Lei preferisce  andare?</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <label>*Se vuole un viaggio combinato dei paesi dell'Asia centrale, scriveteci il paese e la città</label>
                            <input type="text" name="uwant" placeholder="Paesi, città">
                        </div>


                    </div>


                    <h4>Il Suo viaggio </h4>
                    <div class="row clearfix">

                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_couple" value="couple">
                            <label for="check_couple">in tandem</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_family" value="family">
                            <label for="check_family">Con la famiglia</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_friends" value="friends">
                            <label for="check_friends">Con gli amici</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_group" value="group">
                            <label for="check_group">In gruppo (club, associazione...)</label>
                        </div>
                    </div>


                    <h4>Accetta di unirsi al gruppo esistente? </h4>
                    <h5>*viaggiare in gruppo riduce notevolmente il prezzo</h5>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="join[]" id="join_yes" value="yes" required/>
                            <label for="join_yes">Sì</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="join[]" id="join_no" value="no"/>
                            <label for="join_no">No</label>
                        </div>

                    </div>

                    <h4>Ci sono viaggiatori con disabilità? </h4>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="invalid[]" id="invalid_yes" value="yes" required/>
                            <label for="invalid_yes">Sì</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="invalid[]" id="invalid_no" value="no"/>
                            <label for="invalid_no">No</label>
                        </div>

                    </div>


                    <h4>In quale fase è il Suo viaggio? </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_1" value="1" required/>
                            <label for="plan_1">La mia destinazione è già stata scelta, voglio prenotare il mio viaggio più velocemente</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_2" value="2"/>
                            <label for="plan_2">La mia destinazione è già stata scelta, sto iniziando a organizzare il mio viaggio...</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_3" value="3"/>
                            <label for="plan_3">Ho bisogno di maggiori informazioni per prendere la mia decisione</label>
                        </div>

                    </div>


                    <h4>Quale livello di comfort vuole scegliere?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_1" value="1" required/>
                            <label for="house_1">Normale e non costoso</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_2" value="2"/>
                            <label for="house_2">Сomfort</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_3" value="3"/>
                            <label for="house_3">Lux</label>
                        </div>
                    </div>


                    <h4>Per il Suo cibo, vuole che il tour operatore le offra:</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_1" value="1" required/>
                            <label for="food_1">Colazione con alloggio</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_2" value="2"/>
                            <label for="food_2">Mezza pensione</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_3" value="3"/>
                            <label for="food_3">Pensione completa</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_4" value="4"/>
                            <label for="food_4">Selettivamente, i pranzi e le cene nei luoghi esclusivi</label>
                        </div>
                    </div>


                    <h4>Per gli spostamenti, durante il Suo viaggio, Lei preferisce</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_1" value="1" required/>
                            <label for="transport_1">Avere un transfer organizzato (transfer nel’aeroporto e la statione)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_2" value="2"/>
                            <label for="transport_2">Avere un'auto con autista fornito dall'agenzia</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_3" value="3"/>
                            <label for="transport_3">Noleggiare un'auto senza conducente</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_4" value="4"/>
                            <label for="transport_4">Organizzazione parziale dei trasporti (treni, voli, auto)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_5" value="5"/>
                            <label for="transport_5">Muoversi da solo</label>
                        </div>
                    </div>

                    <h4>Preferisce essere accompagnato/a da una guida?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_yes" value="yes" required/>
                            <label for="guide_yes">Sì</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_no" value="no"/>
                            <label for="guide_no">No</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_intime" value="intime"/>
                            <label for="guide_intime">Parzialmente (in alcune città)</label>
                        </div>
                    </div>


                    <h4>La Sua guida deve parlare</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_ru" value="ru"/>
                            <label for="lang_ru">Russo</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_en" value="en"/>
                            <label for="lang_en">Inglese </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_fr" value="fr"/>
                            <label for="lang_fr">Francese</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_it" value="it"/>
                            <label for="lang_it">Italiano</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_de" value="de"/>
                            <label for="lang_de">Tedesca</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="text" name="langother" placeholder="Immettere la lingua desiderata se non è nell'elenco"/>
                        </div>
                    </div>



                    <h4>Quali sono i Suoi interessi</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">TESORO CULTURALE</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_1" value="1">
                                    <label for="culture_1">Musei</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_2" value="2">
                                    <label for="culture_2">Gallerie</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_3" value="3">
                                    <label for="culture_3">Siti storici</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_4" value="4">
                                    <label for="culture_4">Parchi Nazionali</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">SPORT E RIPOSO</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_1" value="1">
                                    <label for="sport_1">Trekking</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_2" value="2">
                                    <label for="sport_2">Escursioni</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_3" value="3">
                                    <label for="sport_3">Viaggio in città</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_4" value="4">
                                    <label for="sport_4">Foto</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25; text-align: center;">RELAX</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_1" value="1">
                                    <label for="relax_1">Gastronomia</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_2" value="2">
                                    <label for="relax_2">Occhiali</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_3" value="3">
                                    <label for="relax_3">artigianato</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_4" value="4">
                                    <label for="relax_4">Pittura</label>
                                </div>
                            </div>
                        </div>


                    </div>


                    <h4>Racconti il Suo progetto di viaggio</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <textarea type="text" name="project" placeholder="Inserisci il testo"></textarea>
                        </div>

                    </div>


                    <h4>Qual è il Suo budget approssimativo </h4>
                    <div class="row clearfix">

                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budget" placeholder="5000">
                        </div>
                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budgetdays" placeholder="12(giorni)">
                        </div>

                    </div>


                    <h4>Come ha saputo di noi?</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_1" value="1">
                            <label for="source_1">Google</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_2" value="2">
                            <label for="source_2">Yandex</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_3" value="3">
                            <label for="source_3">Facebook</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_4" value="4">
                            <label for="source_4">Instagram</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_5" value="5">
                            <label for="source_5">Twitter</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_6" value="6">
                            <label for="source_6">Altri</label>
                        </div>
                    </div>

                </div>




                <div class="col-md-12 col-sm-12 form-group" style="margin-top: 20px;">
                    <button type="submit" class="col-md-12 col-sm-12 theme-btn btn-style-five"
                            style="background: #ffffff;">
                        <span class="btn-title">FATTO!</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php
$js = <<< JS
$('input:radio[name="d_dates"]').change(function () {
    var inputs = $('.dates-input');    
    if($(this).is(':checked') && $(this).val()=='yes'){
            inputs.css('display','block');
        }else {
            inputs.css('display','none');
        }
    });
$('#form_more').on('click',function() {
  var more = $('.div-form-more');
  more.css('display','block');
});

$('#application-form').on('submit',function(e) {
  swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

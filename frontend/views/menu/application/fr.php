<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 11.04.2020
 * Time: 22:31
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
<style>
    label {
        font-size: 18px;
        font-weight: 500;
    }

    input[type="radio"] {

    }

    .dates-input {
        display: none;
    }

    .operator-time, .div-form-more {
        display: none;
    }

    #form_more {
        color: #d4ae25;
        cursor: pointer;
    }

    h4 {
        margin-top: 20px;
    }

    th {
        text-transform: uppercase;
    }
</style>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Page d'accueil</a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2>Écrivez votre projet et vos envies en 2 minutes
                et recevez en direct et gratuitement les devis des agences locales
            </h2>
        </div>
        <div class="default-form comment-form">
            <form action="/fr/send-application" method="post" id="application-form">
                <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>" />

                <h4>Vos coordonnées</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="name" placeholder="Nom " required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="surname" placeholder="Prénom " required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="Email " required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Numéro de téléphone" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="country" placeholder="Pays " required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="city" placeholder="Ville " required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="zipcode" placeholder="Code postal " required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="male" id="male" required>
                        <label for="male">Masculin</label>
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="female" id="female">
                        <label for="female">Féminin</label>
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="other" id="othergender">
                        <label for="othergender">Je ne veux pas mentionner</label>
                    </div>
                </div>

                <h4>D’où venez-vous ?</h4>
                <div class="row clearfix">
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_country" placeholder="Pays" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_city" placeholder="Ville " required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_airport" placeholder="Aéroport" required="">
                    </div>
                </div>

                <h4>Connaissez-vous les dates exactes de votre voyage ?</h4>
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-6 form-group">
                        <input type="radio" name="d_dates" id="d_dates_yes" value="yes"/>
                        <label for="d_dates_yes">Oui</label>
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_from" placeholder="de ">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_to" placeholder="à">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" name="d_days" placeholder="Combien de jours ">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="radio" name="d_dates" id="d_dates_no" value="no"/>
                        <label for="d_dates_no">Non</label>
                    </div>

                </div>

                <h4>Quel est le profil des participants ?</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="adults" placeholder="Adultes" required>
                    </div>
                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="kids" placeholder="Les enfants">
                    </div>

                </div>


                <h4>Comment préférez-vous être contacté ?</h4>
                <div class="row clearfix">

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_phone" value="phone">
                        <label for="check_phone">Téléphone</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_email" value="email">
                        <label for="check_email">Email</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_whatsapp" value="whatsapp">
                        <label for="check_whatsapp">WhatsApp</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_viber" value="viber">
                        <label for="check_viber">Viber</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_imo" value="imo">
                        <label for="check_imo">IMO</label>
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="connect_time" placeholder="Vos préférences horaires et la date(14:00)" required>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_1" id="agree_1">
                        <label for="agree_1">J’accepte de recevoir des publicités commerciales de tour opérateur LIFE VIBES</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_2" id="agree_2" required>
                        <label for="agree_2">J'ai pris connaissance et accepte <a href="#">les conditions générales</a> d'utilisation et les mentions légales du site</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_3" id="agree_3">
                        <label for="agree_3">J’accepte de recevoir les derniers articles de blog et la newsletter LIFE VIBES par mail</label>
                    </div>
                </div>


                <h4>Pour plus d’information appuyez sur <span id="form_more">PLUS</span></h4>
                <br><br>

                <div class="div-form-more">
                    <h4>Où souhaitez-vous partir ?</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <label>*Si vous voulez le voyage combinez en Asie Centrale, écrivez les pays et les villes que vous voulez visitez</label>
                            <input type="text" name="uwant" placeholder="villes ...">
                        </div>


                    </div>


                    <h4>Vous voyagez</h4>
                    <div class="row clearfix">

                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_couple" value="couple">
                            <label for="check_couple">En couple</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_family" value="family">
                            <label for="check_family">En famille</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_friends" value="friends">
                            <label for="check_friends">Entre amis</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_group" value="group">
                            <label for="check_group">En groupe (club, association, CE...)</label>
                        </div>
                    </div>


                    <h4>Accepteriez-vous de rejoindre un groupe déjà constitué ? </h4>
                    <h5>*Le voyage en groupe coute moins cher</h5>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="join[]" id="join_yes" value="yes" required/>
                            <label for="join_yes">Oui</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="join[]" id="join_no" value="no"/>
                            <label for="join_no">Non</label>
                        </div>

                    </div>

                    <h4>Y a-t-il un participant ou plus à mobilité réduite ?</h4>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="invalid[]" id="invalid_yes" value="yes" required/>
                            <label for="invalid_yes">Oui</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="invalid[]" id="invalid_no" value="no"/>
                            <label for="invalid_no">Non</label>
                        </div>

                    </div>


                    <h4>Où en êtes-vous dans votre projet ?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_1" value="1" required/>
                            <label for="plan_1">Ma destination est choisie, je souhaite réserver rapidement mon voyage</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_2" value="2"/>
                            <label for="plan_2">Ma destination est choisie, je commence à organiser mon voyage</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_3" value="3"/>
                            <label for="plan_3">J'ai besoin de plus d'informations pour m'aider à prendre une décision</label>
                        </div>

                    </div>


                    <h4>Quel est le niveau de confort souhaité pour vos hébergements ?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_1" value="1" required/>
                            <label for="house_1">Bien et pas cher</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_2" value="2"/>
                            <label for="house_2">Charme et confort</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_3" value="3"/>
                            <label for="house_3">Luxe </label>
                        </div>
                    </div>


                    <h4>Pour vos repas, souhaitez-vous que l’agence vous propose </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_1" value="1" required/>
                            <label for="food_1">Petit-déjeuner avec l'hébergement</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_2" value="2"/>
                            <label for="food_2">Demi-pension</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_3" value="3"/>
                            <label for="food_3">Pension complète</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_4" value="4"/>
                            <label for="food_4">Occasionnellement, des déjeuners ou dîners dans des lieux incontournables</label>
                        </div>
                    </div>


                    <h4>Pour vos déplacements pendant votre voyage, vous souhaitez </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_1" value="1" required/>
                            <label for="transport_1">Avoir des transferts organisés (transfert à l’aéroport et à la gare)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_2" value="2"/>
                            <label for="transport_2">Avoir un véhicule avec le chauffeur mis à disposition par l'agence</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_3" value="3"/>
                            <label for="transport_3">Location de voiture sans chauffeur </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_4" value="4"/>
                            <label for="transport_4">Organisation partielle des transports (trains, vols, voiture)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_5" value="5"/>
                            <label for="transport_5">Vous déplacer par vos propres moyens</label>
                        </div>
                    </div>

                    <h4>Souhaitez-vous être accompagné d’un guide ?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_yes" value="yes" required/>
                            <label for="guide_yes">Oui</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_no" value="no"/>
                            <label for="guide_no">Non</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_intime" value="intime"/>
                            <label for="guide_intime">Ponctuellement sur certaines étapes de votre voyage</label>
                        </div>
                    </div>


                    <h4>Ваш гид должен говорить на </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_ru" value="ru"/>
                            <label for="lang_ru">Russophone</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_en" value="en"/>
                            <label for="lang_en">Anglophone</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_fr" value="fr"/>
                            <label for="lang_fr">Francophone</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_it" value="it"/>
                            <label for="lang_it">Italophone</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_de" value="de"/>
                            <label for="lang_de">Germanophone </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="text" name="langother" placeholder="Autre"/>
                        </div>
                    </div>



                    <h4>Quelles sont vos envies </h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">Culture & patrimoine</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_1" value="1">
                                    <label for="culture_1">Musées</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_2" value="2">
                                    <label for="culture_2">Sites historiques</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_3" value="3">
                                    <label for="culture_3">Parcs nationaux</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_4" value="4">
                                    <label for="culture_4">Visites urbaines</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">Sport & loisirs</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_1" value="1">
                                    <label for="sport_1">Randonnée</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_2" value="2">
                                    <label for="sport_2">Sensations fortes</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_3" value="3">
                                    <label for="sport_3">Plongée / snorkeling</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_4" value="4">
                                    <label for="sport_4">Photographie</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25; text-align: center;">Détente & bien-être</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_1" value="1">
                                    <label for="relax_1">Gastronomie</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_2" value="2">
                                    <label for="relax_2">Spectacles</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_3" value="3">
                                    <label for="relax_3">Аrtisanat</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_4" value="4">
                                    <label for="relax_4">Art</label>
                                </div>
                            </div>
                        </div>


                    </div>


                    <h4>Parlez-nous de votre projet de voyage </h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <textarea type="text" name="project" placeholder="Saisissez du texte"></textarea>
                        </div>

                    </div>


                    <h4>Votre budget approximatif par personne pour toute la durée du séjour </h4>
                    <div class="row clearfix">

                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budget" placeholder="5000(budget)">
                        </div>
                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budgetdays" placeholder="12(jours)">
                        </div>

                    </div>


                    <h4>Comment avez-vous connu de nous</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_1" value="1">
                            <label for="source_1">Google</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_2" value="2">
                            <label for="source_2">Yandex</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_3" value="3">
                            <label for="source_3">Facebook</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_4" value="4">
                            <label for="source_4">Instagram</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_5" value="5">
                            <label for="source_5">Twitter</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_6" value="6">
                            <label for="source_6">Les autres</label>
                        </div>
                    </div>

                </div>




                <div class="col-md-12 col-sm-12 form-group" style="margin-top: 20px;">
                    <button type="submit" class="col-md-12 col-sm-12 theme-btn btn-style-five"
                            style="background: #ffffff;">
                        <span class="btn-title">C'est parti!</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php
$js = <<< JS
$('input:radio[name="d_dates"]').change(function () {
    var inputs = $('.dates-input');    
    if($(this).is(':checked') && $(this).val()=='yes'){
            inputs.css('display','block');
        }else {
            inputs.css('display','none');
        }
    });
$('#form_more').on('click',function() {
  var more = $('.div-form-more');
  more.css('display','block');
});

$('#application-form').on('submit',function(e) {
  swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
});

JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

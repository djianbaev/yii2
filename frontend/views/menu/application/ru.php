<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 11.04.2020
 * Time: 22:31
 */

use yii\helpers\Url;

/* @var $this \yii\web\View */

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>
<style>
    label {
        font-size: 18px;
        font-weight: 500;
    }

    input[type="radio"] {

    }

    .dates-input {
        display: none;
    }

    .operator-time, .div-form-more {
        display: none;
    }

    #form_more {
        color: #d4ae25;
        cursor: pointer;
    }

    h4 {
        margin-top: 20px;
    }

    th {
        text-transform: uppercase;
    }
</style>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/">Главная</a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2>Заполните вопросник и расскажите нам о ваших пожеланиях за 2 минуты, и получите, возможные варианты
                путешествий</h2>
        </div>
        <div class="default-form comment-form">
            <form action="/ru/send-application" method="post" id="application-form">
                <input type="hidden" name="<?php echo Yii::$app->request->csrfParam ?>"
                       value="<?php echo Yii::$app->request->csrfToken ?>"/>

                <h4>Ваши координаты</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="name" placeholder="Имя" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="surname" placeholder="Фамилия" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="email" name="email" placeholder="Эл.почта" required="">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="text" name="phone" placeholder="Номер телефона" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="country" placeholder="Страна" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="city" placeholder="Город" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="zipcode" placeholder="Почтовый индекс" required="">
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="male" id="male" required>
                        <label for="male">Мужчина</label>
                    </div>

                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="female" id="female">
                        <label for="female">Женщина</label>
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="radio" name="gender" value="other" id="othergender">
                        <label for="othergender">Не хочу указывать</label>
                    </div>
                </div>

                <h4>Место отправления</h4>
                <div class="row clearfix">
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_country" placeholder="Страна" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_city" placeholder="Город" required="">
                    </div>
                    <div class="col-md-4 col-sm-12 form-group">
                        <input type="text" name="d_airport" placeholder="Аэропорт" required="">
                    </div>
                </div>

                <h4>Знаете ли вы точные даты вашего путешествия?</h4>
                <div class="row clearfix">
                    <div class="col-md-3 col-sm-6 form-group">
                        <input type="radio" name="d_dates" id="d_dates_yes" value="yes"/>
                        <label for="d_dates_yes">Да</label>
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_from" placeholder="Выбрать дату от">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" class="datepicker" name="d_dates_to" placeholder="Выбрать дату до">
                    </div>

                    <div class="col-md-3 col-sm-6 form-group dates-input">
                        <input type="text" name="d_days" placeholder="Кол-во дней">
                    </div>

                    <div class="col-md-6 col-sm-12 form-group">
                        <input type="radio" name="d_dates" id="d_dates_no" value="no"/>
                        <label for="d_dates_no">Нет</label>
                    </div>

                </div>

                <h4>Количество путеществующих</h4>
                <div class="row clearfix">

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="adults" placeholder="Взрослые" required>
                    </div>
                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="kids" placeholder="Дети">
                    </div>

                </div>


                <h4>Как вы хотите, чтобы мы с вами связались?</h4>
                <div class="row clearfix">

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_phone" value="phone">
                        <label for="check_phone">Телефон</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_email" value="email">
                        <label for="check_email">Эл.почта</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_whatsapp" value="whatsapp">
                        <label for="check_whatsapp">WhatsApp</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_viber" value="viber">
                        <label for="check_viber">Viber</label>
                    </div>

                    <div class="col-md-4 col-sm-6 form-group check-block">
                        <input type="checkbox" name="connect[]" id="check_imo" value="imo">
                        <label for="check_imo">IMO</label>
                    </div>

                    <div class="col-md-6 col-sm-6 form-group">
                        <input type="text" name="connect_time" placeholder="Ваши часовые предпочтения(14:00)" required>
                    </div>

                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_1" id="agree_1">
                        <label for="agree_1">Я согласен принимать рекламную рассылку</label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_2" id="agree_2" required>
                        <label for="agree_2">Я ознакомлен и принимаю общие <a href="#">условия использования
                                сайта</a></label>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group check-block">
                        <input type="checkbox" name="agree_3" id="agree_3">
                        <label for="agree_3">Я согласен принимать информацию о турах, статьи блога через Email</label>
                    </div>
                </div>


                <h4>Для более детальной информации кликните на <span id="form_more">ДЕТАЛЬНО</span></h4>
                <br><br>

                <div class="div-form-more">
                    <h4>Куда вы собираетесь отправиться</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <label>*Если вы хотите комбинированный тур по странам средней Азии, ведите в графе страну и
                                города</label>
                            <input type="text" name="uwant" placeholder="Страны, города">
                        </div>


                    </div>


                    <h4>Ваше путешествие </h4>
                    <div class="row clearfix">

                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_couple" value="couple">
                            <label for="check_couple">В паре</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_family" value="family">
                            <label for="check_family">С семьей</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_friends" value="friends">
                            <label for="check_friends">С друзьями</label>
                        </div>
                        <div class="col-md-3 col-sm-6 form-group check-block">
                            <input type="checkbox" name="team[]" id="check_group" value="group">
                            <label for="check_group">В группе(клуб, ассоциация…)</label>
                        </div>
                    </div>


                    <h4>Согласны ли вы присоединиться к существующей группе? </h4>
                    <h5>*Путишествие в группе значительно снижает цену</h5>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="join[]" id="join_yes" value="yes" required/>
                            <label for="join_yes">Да</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="join[]" id="join_no" value="no"/>
                            <label for="join_no">Нет</label>
                        </div>

                    </div>

                    <h4>Есть ли путешествующие с ограниченными возможностями? </h4>
                    <div class="row clearfix">
                        <div class="col-md-3 col-sm-6 form-group">
                            <input type="radio" name="invalid[]" id="invalid_yes" value="yes" required/>
                            <label for="invalid_yes">Да</label>
                        </div>

                        <div class="col-md-6 col-sm-12 form-group">
                            <input type="radio" name="invalid[]" id="invalid_no" value="no"/>
                            <label for="invalid_no">Нет</label>
                        </div>

                    </div>


                    <h4>На какой вы стадии вашего планирования путешествия? </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_1" value="1" required/>
                            <label for="plan_1">Мое направление уже выбрано, я хочу быстрее забронировать мое
                                путешествие</label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_2" value="2"/>
                            <label for="plan_2">Мое направление уже выбрано, я начинаю организовать мое
                                путешествие </label>
                        </div>

                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="plan[]" id="plan_3" value="3"/>
                            <label for="plan_3">Я нуждаюсь в большей информации, чтобы определиться в моем
                                решением</label>
                        </div>

                    </div>


                    <h4>Какой уровень комфорта жилья вы хотите выбрать?</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_1" value="1" required/>
                            <label for="house_1">Нормальное и не дорогое</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_2" value="2"/>
                            <label for="house_2">Шарм и комфорт</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="house[]" id="house_3" value="3"/>
                            <label for="house_3">Люкс </label>
                        </div>
                    </div>


                    <h4>Для вашего питания, хотели бы вы, чтобы туроператор вам предложил</h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_1" value="1" required/>
                            <label for="food_1">Завтрак с жильем </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_2" value="2"/>
                            <label for="food_2">Полупансион </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_3" value="3"/>
                            <label for="food_3">Полный пансион</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="food[]" id="food_4" value="4"/>
                            <label for="food_4">Выборочно, обеды и ужины в необыкновенных местах </label>
                        </div>
                    </div>


                    <h4>Для ваших передвижений, во время вашего путешествия, вы бы хотели </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_1" value="1" required/>
                            <label for="transport_1">Иметь организованный трансфер (трансферы в аэропорт и
                                вокзал)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_2" value="2"/>
                            <label for="transport_2">Иметь машину с водителем, предоставленную агентством</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_3" value="3"/>
                            <label for="transport_3">Аренда автомобиля </label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_4" value="4"/>
                            <label for="transport_4">Частичная организация транспорта (поезда, перелеты, машина)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="transport[]" id="transport_5" value="5"/>
                            <label for="transport_5">Вы желаете передвигаться сами </label>
                        </div>
                    </div>

                    <h4>Желаете ли вы быть сопровождены гидом? </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_yes" value="yes" required/>
                            <label for="guide_yes">Да</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_no" value="no"/>
                            <label for="guide_no">Нет</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="guide[]" id="guide_intime" value="intime"/>
                            <label for="guide_intime">Частично (в определенных городах)</label>
                        </div>
                    </div>


                    <h4>Ваш гид должен говорить на </h4>
                    <div class="row clearfix">
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_ru" value="ru"/>
                            <label for="lang_ru">Русский</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_en" value="en"/>
                            <label for="lang_en">Английский</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_fr" value="fr"/>
                            <label for="lang_fr">Французский</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_it" value="it"/>
                            <label for="lang_it">Итальянский</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="radio" name="lang" id="lang_de" value="de"/>
                            <label for="lang_de">Немецкий</label>
                        </div>
                        <div class="col-md-12 col-sm-12 form-group">
                            <input type="text" name="langother"
                                   placeholder="Введите нужный язык если его нет в списке"/>
                        </div>
                    </div>


                    <h4>Какие ваши пожелания</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">КУЛЬТУРНОЕ ДОСТОЯНИЕ </h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_1" value="1">
                                    <label for="culture_1">Музеи</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_2" value="2">
                                    <label for="culture_2">Галереи</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_3" value="3">
                                    <label for="culture_3">Исторические места</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="culture[]" id="culture_4" value="4">
                                    <label for="culture_4">Национальные парки</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25;text-align: center;">СПОРТ И ОТДЫХ</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_1" value="1">
                                    <label for="sport_1">Трекинги</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_2" value="2">
                                    <label for="sport_2">Пешие прогулки</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_3" value="3">
                                    <label for="sport_3">Городское путешествие</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="sport[]" id="sport_4" value="4">
                                    <label for="sport_4">Фотография</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12" style="border: 1px solid #d4ae25;">
                            <h5 style="border-bottom: 1px solid #d4ae25; text-align: center;">РЕЛАКС</h5>
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_1" value="1">
                                    <label for="relax_1">Гастрономия</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_2" value="2">
                                    <label for="relax_2">Спектакли</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_3" value="3">
                                    <label for="relax_3">Ремесленничество</label>
                                </div>
                                <div class="col-md-12 col-sm-12 form-group check-block">
                                    <input type="checkbox" name="relax[]" id="relax_4" value="4">
                                    <label for="relax_4">Живопись</label>
                                </div>
                            </div>
                        </div>


                    </div>


                    <h4>Расскажите нам о вашем проекте путешествие</h4>
                    <div class="row clearfix">

                        <div class="col-md-12 col-sm-12 form-group">
                            <textarea type="text" name="project" placeholder="Введите текст"></textarea>
                        </div>

                    </div>


                    <h4>Ваш примерный бюджет и количество дней </h4>
                    <div class="row clearfix">

                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budget" placeholder="5000$">
                        </div>
                        <div class="col-md-6 col-sm-6 form-group">
                            <input type="text" name="budgetdays" placeholder="12">
                        </div>

                    </div>


                    <h4>Как вы узнали о нас?</h4>
                    <div class="row clearfix">

                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_1" value="1">
                            <label for="source_1">Google поиск</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_2" value="2">
                            <label for="source_2">Yandex поиск</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_3" value="3">
                            <label for="source_3">Facebook</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_4" value="4">
                            <label for="source_4">Instagram</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_5" value="5">
                            <label for="source_5">Twitter</label>
                        </div>
                        <div class="col-md-4 col-sm-6 form-group check-block">
                            <input type="checkbox" name="source[]" id="source_6" value="6">
                            <label for="source_6">Прочие</label>
                        </div>
                    </div>

                </div>


                <div class="col-md-12 col-sm-12 form-group" style="margin-top: 20px;">
                    <button type="submit" class="col-md-12 col-sm-12 theme-btn btn-style-five"
                            style="background: #ffffff;">
                        <span class="btn-title">ПОЕХАЛИ!</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php
$js = <<< JS
$('input:radio[name="d_dates"]').change(function () {
    var inputs = $('.dates-input');    
    if($(this).is(':checked') && $(this).val()=='yes'){
            inputs.css('display','block');
        }else {
            inputs.css('display','none');
        }
    });
$('#form_more').on('click',function() {
  var more = $('.div-form-more');
  more.css('display','block');
});

$('#application-form').on('submit',function(e) {
  swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
});

JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 11.04.2020
 * Time: 0:52
 */
use yii\helpers\Url;

$lang = Yii::$app->language;

$this->title = $service['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $menu['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $service['name_'.$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li>
                            <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $menu['slug']]) ?>"><?php echo $menu['name_' . $lang] ?></a>
                        </li>
                        <li><?php echo $service['name_'.$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="destinations-two">


    <div class="auto-container">
        <div class="sec-title centered">
            <h2><?php echo $service['name_' . $lang] ?></h2>
        </div>
        <div class="popular-packages">
            <div class="packages-box">
                <div class="row clearfix">

                    <?php foreach ($datas as $data): ?>
                        <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp"
                             data-wow-delay="0ms"
                             data-wow-duration="1500ms">
                            <div class="inner-box clearfix">
                                <div class="image-box">
                                    <img src="<?php echo $data['image']?>" alt="" title="">
                                    <div class="hover-box">
                                        <div class="hover-link">
                                            <a href="<?php echo Url::to(['site/serviceitem','service_item_id'=>$data['id']])?>" class="default-link">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="content-box">
                                    <div class="content">
                                        <div class="package-title">
                                            <a href="<?php echo Url::to(['site/serviceitem','service_item_id'=>$data['id']])?>"><?php echo $data['name_'.$lang]?></a>
                                        </div>
                                        <div class="info">
                                            <ul class="clearfix">
                                                <li><span class="icon flaticon-wall-clock"></span>
                                                    <?php echo $data['date']?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="links-box clearfix">
                                            <div class="pricing">
                                                <div class="price">
                                                    <div class="price-title"></div>
                                                    <div class="unit"><span>$<?php echo $data['price']?></span></div>
                                                </div>
                                            </div>
                                            <div class="link">
                                                <div class="info-btn-box">
                                                    <div class="theme-btn info-btn"><span
                                                            class="icon flaticon-information-1"></span>
                                                    </div>
                                                    <div class="info-panel">
                                                        <div class="panel-inner">
                                                            <ul class="panel-list">
                                                                <?php echo $data['intro_'.$lang]?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</section>
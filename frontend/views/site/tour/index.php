<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 10.04.2020
 * Time: 21:40
 */

use yii\helpers\Url;

$lang = Yii::$app->language;

$this->title = $menu['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $menu['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo Yii::$app->params['expAllTour'][$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li>
                            <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $menu['slug']]) ?>"><?php echo $menu['name_' . $lang] ?></a>
                        </li>
                        <li><?php echo Yii::$app->params['expAllTour'][$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="yacht-tours-container">
    <div class="auto-container">
        <div class="main-title"><h2><?php echo Yii::$app->params['expAllTour'][$lang]?></h2></div>

        <div class="content-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="content-inner">

                        <div class="results-row">
                            <?php if (!empty($models)): ?>
                                <?php foreach ($models as $model): ?>
                                    <div class="activity-block-two">
                                        <div class="inner-box clearfix">
                                            <div class="image-layer"
                                                 style="background-image:url(<?php echo $model['image'] ?>);"></div>

                                            <div class="image-box">
                                                <a href="<?php echo Url::to(['site/tourview','tour_slug'=>$model['slug']])?>"><img
                                                            src="<?php echo $model['image'] ?>" alt="" title=""></a>
                                            </div>

                                            <div class="content-box">
                                                <div class="content">
                                                    <h4>
                                                        <a href="<?php echo Url::to(['site/tourview','tour_slug'=>$model['slug']])?>"><?php echo $model['name_' . $lang] ?></a>
                                                    </h4>
                                                    <div class="info">
                                                        <ul class="clearfix">
                                                            <li><span class="icon flaticon-wall-clock"></span>
                                                                <?php echo $model['days_night_' . $lang] ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="links-box clearfix">
                                                    <div class="link">
                                                        <a href="<?php echo Url::to(['site/tourview','tour_slug'=>$model['slug']])?>"
                                                           class="theme-btn btn-style-two"><span class="btn-title"><?php echo Yii::$app->params['expBook'][$lang]?></span></a>
                                                    </div>
                                                </div>
                                                <div class="pricing">
                                                    <div class="price">
                                                        <div class="price-title"><?php echo Yii::$app->params['expPriceFrom'][$lang]?></div>
                                                        <div class="unit"><span>$<?php echo $model['price'] ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>


                <div class="widgets-side col-lg-4 col-md-6 col-sm-12">
                    <div class="widgets-content">
                        <?php if (!empty($datas)): ?>
                            <?php foreach ($services as $service): ?>
                                <?php if (array_key_exists($service['id'], $datas)): ?>
                                    <div class="tour-widget offers-widget">
                                        <div class="widget-inner">
                                            <div class="tour-widget-title">
                                                <h4><?php echo $service['name_' . $lang] ?></h4></div>

                                            <?php foreach ($datas[$service['id']] as $data): ?>
                                                <div class="post">
                                                    <div class="post-inner">
                                                        <div class="post-thumb">
                                                            <a href="#"><img src="<?php echo $data['image']?>"
                                                                             alt=""
                                                                             title=""></a>
                                                        </div>
                                                        <h5><a href="#"><?php echo $data['name_'.$lang]?></a></h5>
                                                        <div class="info"><span class="icon flaticon-wall-clock"></span>
                                                            <?php echo $data['duration']?> <?php echo Yii::$app->params['hours'][$lang]?>
                                                        </div>
                                                        <div class="price"><span class="new">$<?php echo $data['price']?></span></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
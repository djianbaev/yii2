<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 10.04.2020
 * Time: 22:44
 */

use yii\helpers\Url;

$lang = Yii::$app->language;

$this->title = $model['name_' . $lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $menu['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li>
                            <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $menu['slug']]) ?>"><?php echo $menu['name_' . $lang] ?></a>
                        </li>
                        <li><a href="/tours-packages"><?php echo Yii::$app->params['expAllTour'][$lang]?></a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="yacht-tours-container alternate-bg-two">
    <div class="auto-container">

        <div class="content-container">
            <div class="row clearfix">

                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="content-inner">
                        <!--Tour Details-->
                        <div class="tour-details">

                            <div class="info-title clearfix">
                                <div class="left-info">
                                    <h3><?php echo $model['name_' . $lang] ?></h3>
                                    <div class="info">
                                        <span class="icon flaticon-wall-clock"></span>
                                        <?php echo $model['days_night_' . $lang] ?>
                                    </div>
                                </div>
                            </div>

                            <div class="main-image">
                                <img src="<?php echo $model['image'] ?>" alt="" title="">
                            </div>

                            <div class="text-block">
                                <?php echo $model['descr_' . $lang] ?>
                            </div>

                            <div class="included-material">
                                <div class="included-box">
                                    <div class="accordion-box">
                                        <div class="accordion block">
                                            <div class="acc-btn"><?php echo Yii::$app->params['tourDetailed'][$lang]?>
                                                <div class="icon flaticon-down-arrow"></div>
                                            </div>
                                            <div class="acc-content">
                                                <div class="content">
                                                    <div class="text-block">
                                                        <?php echo $model['text_' . $lang] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="included-material">
                                <h4><?php echo $model['days_night_' . $lang] ?></h4>
                                <div class="included-box">
                                    <div class="accordion-box">
                                        <?php if (!empty($days)): ?>
                                            <?php foreach ($days as $day): ?>
                                                <div class="accordion block">
                                                    <div class="acc-btn"><?php echo $day['name_' . $lang] ?>
                                                        <div class="icon flaticon-down-arrow"></div>
                                                    </div>
                                                    <div class="acc-content">
                                                        <div class="content">
                                                            <div class="text">
                                                                <?php echo $day['text_' . $lang] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>

                            <div class="text-block" style="background: #FFFFFF;padding: 30px 30px;">
                                <?php echo $model['include_' . $lang] ?>
                            </div>
                        </div>
                        <div class="mixit-gallery filter-gallery">

                            <div class="filter-list row clearfix">
                                <?php if (!empty($images)): ?>
                                    <?php foreach ($images as $image): ?>
                                        <div class="gallery-block-two col-lg-4 col-md-4 col-sm-6 wow fadeInUp"
                                             data-wow-delay="0ms" data-wow-duration="1500ms">
                                            <div class="inner-box">
                                                <div class="image-box">
                                                    <figure class="image">
                                                        <a class="lightbox-image"
                                                           href="<?php echo $image['image'] ?>"><img
                                                                    src="<?php echo $image['image'] ?>" alt=""></a>
                                                    </figure>
                                                    <div class="zoom-btn">
                                                        <a class="lightbox-image zoom-link"
                                                           href="<?php echo $image['image'] ?>" data-fancybox="gallery"><span
                                                                    class="icon flaticon-expand"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>


                        </div>


                        <?php if (!empty($reviews)): ?>
                            <div class="comments-area">
                                <div class="group-title">
                                    <h4><?php echo Yii::$app->params['reviews'][$lang]?></h4>
                                </div>
                                <div class="comment-box">
                                    <?php foreach ($reviews as $review): ?>
                                        <div class="comment" style="margin-top: 10px;">
                                            <div class="author-thumb">
                                                <figure class="thumb">
                                                    <img src="<?php echo $review['image'] ?>"
                                                         alt="<?php echo $review['fullname'] ?>" style="border-radius: 10px;">
                                                </figure>
                                            </div>
                                            <div class="info">
                                                <span class="name"><?php echo $review['fullname'] ?></span>
                                                <span class="date"><?php echo $review['created_date'] ?></span>
                                            </div>
                                            <div class="text"><?php echo $review['text'] ?></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>


                        <div class="leave-comments">
                            <div class="group-title">
                                <h4><?php echo Yii::$app->params['reviewTitle'][$lang]?></h4>
                                <div class="instruction">* <?php echo Yii::$app->params['reviewSub1'][$lang]?></div>
                                <div class="instruction">* <?php echo Yii::$app->params['reviewSub2'][$lang]?></div>
                            </div>
                            <div class="default-form comment-form">
                                <form id="tour-review-form">
                                    <div class="row clearfix">
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>">
                                        <input type="hidden" name="tour" value="<?php echo $model['id']?>">
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <textarea name="review" placeholder="<?php echo Yii::$app->params['reviewText'][$lang]?>" required></textarea>
                                        </div>

                                        <div class="col-md-6 col-sm-12 form-group">
                                            <input type="text" name="fullname" placeholder="<?php echo Yii::$app->params['reviewName'][$lang]?>" required="">
                                        </div>

                                        <div class="col-md-6 col-sm-12 form-group">
                                            <input type="email" name="email" placeholder="<?php echo Yii::$app->params['reviewEmail'][$lang]?>" required="">
                                        </div>
                                        <div class="col-md-6 col-sm-12 form-group">
                                            <label for="fileTR" id="avatarInput"><?php echo Yii::$app->params['reviewFile'][$lang]?></label>
                                            <input type="file" name="avatar" placeholder="Фотография" id="fileTR" style="visibility:hidden;">
                                        </div>
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <button type="submit" class="theme-btn btn-style-five" id="rtSubmit">
                                                <span class="btn-title"><?php echo Yii::$app->params['reviewSend'][$lang]?></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="widgets-side col-lg-4 col-md-6 col-sm-12">

                    <div class="widgets-content">
                        <!--Widget-->
                        <div class="tour-widget single-booking-widget">
                            <div class="widget-inner">
                                <div class="upper-info clearfix">
                                    <div class="price-info">
                                        <div class="icon"><span class="flaticon-gas"></span></div>
                                        <div class="p-title"><?php echo Yii::$app->params['tourPrice'][$lang]?></div>
                                        <div class="p-amount">$<?php echo $model['price'] ?> <span>/ <?php echo Yii::$app->params['tourPerson'][$lang]?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="default-form main-booking-form">
                                    <form method="post" action="<?php echo Url::to(['site/tourbooking'])?>" id="tour-book-form">
                                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>">
                                        <input type="hidden" name="book-tour" value="<?php echo $model['id']?>">
                                        <h5><?php echo Yii::$app->params['expBook'][$lang]?></h5>
                                        <div class="form-group">
                                            <div class="field-inner">
                                                <?php if(empty($userdata)):?>
                                                <input type="text" name="book-name" placeholder="<?php echo Yii::$app->params['bookName'][$lang]?>" required=""
                                                       value="">
                                                <?php else:?>
                                                    <input type="text" name="book-name" placeholder="<?php echo Yii::$app->params['bookName'][$lang]?>" required=""
                                                           value="<?php echo $userdata['name']?>" readonly>
                                                <?php endif;?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="field-inner">
                                                <?php if(empty($userdata)):?>
                                                <input type="email" name="book-email" placeholder="<?php echo Yii::$app->params['bookEmail'][$lang]?>" required=""
                                                       value="">
                                                <?php else:?>
                                                    <input type="email" name="book-email" placeholder="<?php echo Yii::$app->params['bookEmail'][$lang]?>" required=""
                                                           value="<?php echo $userdata['email']?>" readonly>
                                                <?php endif;?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="field-inner">
                                                <input type="text" name="book-adults" placeholder="<?php echo Yii::$app->params['bookAdults'][$lang]?>" required=""
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="field-inner">
                                                <input type="text" name="book-kids" placeholder="<?php echo Yii::$app->params['bookKids'][$lang]?>"
                                                       value="">
                                            </div>
                                        </div>


                                        <br>


                                        <div class="form-group">
                                            <button type="submit" class="theme-btn"><span
                                                        class="btn-title"><?php echo Yii::$app->params['bookSubmit'][$lang]?></span>
                                            </button>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>

                    <?php if (!empty($months)): ?>
                        <div class="widgets-content">
                            <div class="tour-widget single-booking-widget">
                                <div class="widget-inner">
                                    <div class="upper-info clearfix">
                                        <div class="price-info">
                                            <div class="icon"><span class="flaticon-time-1"></span></div>
                                            <div class="p-amount"><?php echo Yii::$app->params['tourCalendar'][$lang]?></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php foreach ($months as $month): ?>
                                            <?php if ($month['value'] == 0): ?>
                                                <div class="col-md-4"
                                                     style="color: red;"><?php echo $calendar[$month['calendar_id']]['name_' . $lang] ?></div>
                                            <?php else: ?>
                                                <div class="col-md-4"
                                                     style="color: green;"><?php echo $calendar[$month['calendar_id']]['name_' . $lang] ?></div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</section>
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 14.04.2020
 * Time: 1:42
 */


/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;


$lang = Yii::$app->language;

$this->title = $model['name_'.$lang];
?>
<section class="page-banner">
    <div class="image-layer" style="background: #d4ae25;"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $moment['name_'.$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><a href="/more-uzbek-cuisine"><?php echo $moment['name_'.$lang]?></a></li>
                        <li><?php echo $model['name_'.$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <div class="content-side col-lg-8 col-md-12 col-sm-12" style="margin-bottom: unset;">
                <div class="blog-content">
                    <div class="news-post-details">

                        <!--News Block-->
                        <div class="news-block-six">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure class="image">
                                        <img src="<?php echo $model['image']?>" alt="" title="">
                                    </figure>
                                </div>
                                <div class="content-box">
                                    <div class="inner">
                                        <div class="content lv-content">
                                            <h3><?php echo $model['name_'.$lang]?></h3>
                                            <div class="text">
                                                <?php echo $model['text_'.$lang]?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="widgets-side col-lg-4 col-md-12 col-sm-12">

                <div class="widgets-content">
                    <!--Widget-->
                    <div class="tour-widget single-booking-widget">
                        <div class="widget-inner">
                            <div class="upper-info clearfix">
                                <div class="price-info">
                                    <div class="icon"><span class="flaticon-gas"></span></div>
                                    <div class="p-title"><?php echo Yii::$app->params['tourPrice'][$lang]?></div>
                                    <div class="p-amount">$<?php echo $model['price'] ?> <span>/ <?php echo Yii::$app->params['tourPerson'][$lang]?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="default-form main-booking-form">
                                <form method="post" action="<?php echo Url::to(['site/momentbooking'])?>" id="cuisine-book-form">
                                    <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>">
                                    <input type="hidden" name="book-moment" value="<?php echo $model['id']?>">
                                    <h5><?php echo Yii::$app->params['expBook'][$lang]?></h5>
                                    <div class="form-group">
                                        <div class="field-inner">
                                            <?php if(empty($userdata)):?>
                                                <input type="text" name="book-name" placeholder="<?php echo Yii::$app->params['bookName'][$lang]?>" required=""
                                                       value="">
                                            <?php else:?>
                                                <input type="text" name="book-name" placeholder="<?php echo Yii::$app->params['bookName'][$lang]?>" required=""
                                                       value="<?php echo $userdata['name']?>" readonly>
                                            <?php endif;?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="field-inner">
                                            <?php if(empty($userdata)):?>
                                                <input type="email" name="book-email" placeholder="<?php echo Yii::$app->params['bookEmail'][$lang]?>" required=""
                                                       value="">
                                            <?php else:?>
                                                <input type="email" name="book-email" placeholder="<?php echo Yii::$app->params['bookEmail'][$lang]?>" required=""
                                                       value="<?php echo $userdata['email']?>" readonly>
                                            <?php endif;?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="field-inner">
                                            <input type="text" name="book-adults" placeholder="<?php echo Yii::$app->params['bookAdults'][$lang]?>" required=""
                                                   value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="field-inner">
                                            <input type="text" name="book-kids" placeholder="<?php echo Yii::$app->params['bookKids'][$lang]?>"
                                                   value="">
                                        </div>
                                    </div>


                                    <br>


                                    <div class="form-group">
                                        <button type="submit" class="theme-btn"><span
                                                    class="btn-title"><?php echo Yii::$app->params['bookSubmit'][$lang]?></span>
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>


<?php if(!empty($images)):?>
    <section class="portfolio-section portfolio-two-column" style="padding: unset;">
        <div class="auto-container">
            <div class="row clearfix">
                <?php foreach ($images as $image):?>
                    <div class="gallery-block-two col-lg-4 col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image">
                                    <a class="lightbox-image" href="<?php echo $image['image']?>"><img src="<?php echo $image['image']?>" alt=""></a>
                                </figure>
                                <div class="zoom-btn">
                                    <a class="lightbox-image zoom-link" href="<?php echo $image['image']?>" data-fancybox="gallery"><span class="icon flaticon-expand"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>

        </div>
    </section>
<?php endif;?>


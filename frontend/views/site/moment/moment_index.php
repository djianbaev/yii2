<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 14.04.2020
 * Time: 1:42
 */


/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;


$lang = Yii::$app->language;

$this->title = $moment['name_'.$lang];
?>
<section class="page-banner">
    <div class="image-layer" style="background: #d4ae25;"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $moment['name_'.$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $moment['name_'.$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="destinations-two">


    <div class="auto-container">
        <div class="sec-title centered">
            <h2><?php echo $moment['name_' . $lang] ?></h2>
        </div>
        <div class="popular-packages">
            <div class="packages-box">
                <div class="row clearfix">

                    <?php foreach ($models as $data): ?>
                        <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp"
                             data-wow-delay="0ms"
                             data-wow-duration="1500ms">
                            <div class="inner-box clearfix">
                                <div class="image-box">
                                    <img src="<?php echo $data['image']?>" alt="" title="">
                                    <div class="hover-box">
                                        <div class="hover-link">
                                            <a href="<?php echo Url::to(['site/momentview','moment_slug'=>$data['slug']])?>" class="default-link">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="content-box">
                                    <div class="content">
                                        <div class="package-title text-center">
                                            <a href="<?php echo Url::to(['site/momentview','moment_slug'=>$data['slug']])?>"><?php echo $data['name_'.$lang]?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 14.04.2020
 * Time: 15:56
 */

use yii\helpers\Url;

/* @var $this yii\web\View*/

$lang = Yii::$app->language;

$this->title = $model['name_'.$lang];
?>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $menu['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_' . $lang] ?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $model['name_' . $lang] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="welcome-section">
    <div class="auto-container">
        <div class="title-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <h2><?php echo $model['name_' . $lang] ?></h2>
            <div class="text"><?php echo $model['text_' . $lang] ?></div>
        </div>
    </div>
</section>
<?php if(!empty($images)):?>
    <section class="portfolio-section portfolio-two-column" style="padding: unset;">
        <div class="auto-container">
            <div class="row clearfix">
                <?php foreach ($images as $image):?>
                    <div class="gallery-block-two col-lg-4 col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image">
                                    <a class="lightbox-image" href="<?php echo $image['image']?>"><img src="<?php echo $image['image']?>" alt=""></a>
                                </figure>
                                <div class="zoom-btn">
                                    <a class="lightbox-image zoom-link" href="<?php echo $image['image']?>" data-fancybox="gallery"><span class="icon flaticon-expand"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>

        </div>
    </section>
<?php endif;?>


<section class="destinations-two">
    <div class="auto-container">
        <div class="popular-packages">
            <div class="title-row clearfix">
                <div class="sec-title">
                    <h2><?php echo $menu['name_' . $lang] ?></strong></h2>
                </div>
            </div>
            <div class="packages-box">
                <div class="row clearfix">
                    <?php if (!empty($rowG)): ?>
                        <?php for ($i = 0; $i < count($rowG); $i++): ?>
                            <div class="package-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="<?php echo ($i*300)?>ms"
                                 data-wow-duration="1500ms">
                                <div class="inner-box clearfix">
                                    <div class="image-box">
                                        <img src="<?php echo $rowG[$i]['image']?>" alt="" title="">
                                        <div class="hover-box">
                                            <div class="hover-link">
                                                <a href="<?php echo Url::to(['menu/guideitems','menu_slug'=>$menu['slug'],'city_id'=>$model['id'],'guide_slug'=>$rowG[$i]['slug']])?>" class="default-link">
                                                    <span class="icon flaticon-logout"></span>
                                                    <span class="link-text"><?php echo Yii::$app->params['wShowAll'][$lang]?></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="content-box">
                                        <div class="content">
                                            <div class="package-title text-center">
                                                <a href="<?php echo Url::to(['menu/guideitems','menu_slug'=>$menu['slug'],'city_id'=>$model['id'],'guide_slug'=>$rowG[$i]['slug']])?>"><?php echo $rowG[$i]['name_'.$lang]?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

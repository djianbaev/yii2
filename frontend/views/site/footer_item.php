<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 14.04.2020
 * Time: 1:42
 */


/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;


$lang = Yii::$app->language;

$this->title = $model['name_'.$lang];
?>
<section class="page-banner">
    <div class="image-layer" style="background: #d4ae25;"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo $model['name_'.$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo $model['name_'.$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">

            <div class="content-side col-lg-12 col-md-12 col-sm-12" style="margin-bottom: unset;">
                <div class="blog-content">
                    <div class="news-post-details">
                        <!--News Block-->
                        <div class="news-block-six">
                            <div class="inner-box">

                                <div class="content-box">
                                    <div class="inner">
                                        <div class="content lv-content">
                                            <h3><?php echo $model['name_'.$lang]?></h3>
                                            <div class="text">
                                                <?php echo $model['text_'.$lang]?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="image-box">
                                    <figure class="image">
                                        <img src="<?php echo $model['image']?>" alt="" title="">
                                    </figure>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




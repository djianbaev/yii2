<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$lang = Yii::$app->language;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=<?php echo $lang;?>&amp;apikey=45a603d2-d135-481c-8416-e8967f818697" type="text/javascript"></script>


<section class="page-banner">
    <div class="image-layer" style="background-image:url(<?php echo $model['banner'] ?>);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo Yii::$app->params['hContact'][$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo Yii::$app->params['hContact'][$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-section">
    <div class="auto-container">

        <div class="info-container">
            <div class="row clearfix">
                <!--Info Block-->
                <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="icon-box">
                            <span class="flaticon-address"></span>
                        </div>
                        <div class="title-box">
                            <?php echo Yii::$app->params['cAddress'][$lang]?>
                        </div>
                        <div class="content-box">
                            <div class="text"><?php echo $model['address_'.$lang]?></div>
                        </div>
                    </div>
                </div>
                <!--Info Block-->
                <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="icon-box">
                            <span class="flaticon-smartphone-3"></span>
                        </div>
                        <div class="title-box">
                            <?php echo Yii::$app->params['cPhone'][$lang]?>
                        </div>
                        <div class="content-box">
                            <div class="text"><?php echo $model['phone']?></div>
                        </div>
                    </div>
                </div>
                <!--Info Block-->
                <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="icon-box">
                            <span class="flaticon-email-1"></span>
                        </div>
                        <div class="title-box">
                            <?php echo Yii::$app->params['cEmail'][$lang]?>
                        </div>
                        <div class="content-box">
                            <div class="text"><?php echo $model['email']?></div>
                        </div>
                    </div>
                </div>
                <!--Info Block-->
                <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <div class="icon-box">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="title-box">
                            <?php echo Yii::$app->params['cHours'][$lang]?>
                        </div>
                        <div class="content-box">
                            <div class="text"><?php echo $model['work_'.$lang]?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="contact-container">
            <div class="row clearfix">
                <!--Form-->
                <div class="form-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="sec-title">
                            <?php echo Yii::$app->params['cFormTitle'][$lang]?>
                        </div>
                        <div class="default-form">
                            <form method="post" id="contact-form">
                                <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>">
                                <div class="row clearfix">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <div class="field-label"><?php echo Yii::$app->params['wSubscribeFormName'][$lang]?></div>
                                        <div class="field-inner">
                                            <input type="text" name="name" placeholder="John Doe" required="" value="">
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <div class="field-label"><?php echo Yii::$app->params['wSubscribeFormEmail'][$lang]?></div>
                                        <div class="field-inner">
                                            <input type="email" name="email" placeholder="example@mail.com" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <div class="field-label"><?php echo Yii::$app->params['cFormSubject'][$lang]?></div>
                                        <div class="field-inner">
                                            <input type="text" name="subject" placeholder="" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <div class="field-label"><?php echo Yii::$app->params['cFormMessage'][$lang]?></div>
                                        <div class="field-inner">
                                            <textarea name="message" placeholder="<?php echo Yii::$app->params['cFormMessageText'][$lang]?>..." required></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" form="contact-form" class="theme-btn btn-style-three"><span class="btn-title"><?php echo Yii::$app->params['send'][$lang]?></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--Map-->
                <div class="map-column col-lg-6 col-md-12 col-sm-12">
                    <div id='map' style="height: 100%;"></div>
                </div>
            </div>
        </div>

    </div>
</section>

<?php
$js = <<< JS
 ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [39.6540185,66.9555563],
            zoom: 17
        }, {
            searchControlProvider: 'yandex#search'
        });


        // myMap.geoObjects
        //     .add(new ymaps.Placemark([39.682117,66.9050044], {
        //         preset: 'islands#icon',
        //         iconColor: '#31f971'
        //     }));
        
        lifevibes = new ymaps.Placemark([39.6540185,66.9555563], {
            
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#imageWithContent',
            // Своё изображение иконки метки.
            iconImageHref: '/res/images/logo1.png',
            // Размеры метки.
            iconImageSize: [100, 60],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-24, -24],
            // Смещение слоя с содержимым относительно слоя с картинкой.
            iconContentOffset: [15, 15],
           
        });
        myMap.geoObjects.add(lifevibes);

    }
JS;
$this->registerJs($js,\yii\web\View::POS_LOAD);
?>
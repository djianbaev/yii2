<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$lang = Yii::$app->language;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="fxt-template-animation fxt-template-layout10 has-animation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-12 fxt-bg-img" data-bg-image="/loginres/img/figure/bg10-l.jpg">
                <div class="fxt-header">
                    <a href="/" class="fxt-logo"><img src="/res/images/logo-white.png" alt="Life-Vibes"></a>
                    <h1><?php echo Yii::$app->params['lWelcome'][$lang]?></h1>
                    <p><?php echo Yii::$app->params['lNoProfile'][$lang]?></p>
                    <a href="/registration" class="fxt-btn-ghost"><?php echo Yii::$app->params['lRegistration'][$lang]?></a>
                </div>
            </div>
            <div class="col-md-6 col-12 fxt-bg-color">
                <div class="fxt-content">
                    <div class="fxt-form">
                        <h2><?php echo Yii::$app->params['lLogin'][$lang]?></h2>
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-3">
                                <?= $form->field($model, 'username')->textInput(['class'=>'form-control','placeholder'=>'user1234'])->label(false) ?>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-4">
                                <?= $form->field($model, 'password')->passwordInput(['id'=>'password','class'=>'form-control','placeholder'=>'********'])->label(false) ?>
                                <i toggle="#password" class="fa fa-fw fa-eye toggle-password field-icon"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-3">
                                <div class="fxt-checkbox-area">
                                    <a href="/forgot-password" class="switcher-text2"><?php echo Yii::$app->params['lForgot'][$lang]?></a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-5">
                                <div class="text-center">
                                    <button type="submit" class="fxt-btn-fill"><?php echo Yii::$app->params['hLogin'][$lang]?></button>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

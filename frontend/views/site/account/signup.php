<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$lang = Yii::$app->language;
$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="fxt-template-animation fxt-template-layout10 has-animation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-12 order-md-2 fxt-bg-img" data-bg-image="/loginres/img/figure/bg10-l.jpg">
                <div class="fxt-header">
                    <a href="/" class="fxt-logo"><img src="/res/images/logo-white.png" alt="Logo"></a>
                    <h1><?php echo Yii::$app->params['lWelcome'][$lang]?></h1>
                    <p><?php echo Yii::$app->params['lYesProfile'][$lang]?></p>
                    <a href="/login" class="fxt-btn-ghost"><?php echo Yii::$app->params['hLogin'][$lang]?></a>
                </div>
            </div>
            <div class="col-md-6 col-12 order-md-1 fxt-bg-color">
                <div class="fxt-content">
                    <div class="fxt-form">
                        <h2><?php echo Yii::$app->params['lCreateAccount'][$lang]?></h2>
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-3">
                                    <?= $form->field($model, 'username')->textInput(['class' => 'form-control','placeholder'=>'ex: johndoe, johndoe2020']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-3">
                                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control','placeholder'=>'example@mail.com']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-4">
                                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control','placeholder'=>'********','id'=>'password']) ?>
                                    <i toggle="#password" class="fa fa-fw fa-eye toggle-password field-icon"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-5">
                                    <div class="text-center">
                                        <button type="submit" class="fxt-btn-fill"><?php echo Yii::$app->params['lCreate'][$lang]?></button>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$lang = Yii::$app->language;
$this->title = 'Новый пароль';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="fxt-template-animation fxt-template-layout10 has-animation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-12 fxt-bg-img" data-bg-image="/loginres/img/figure/bg10-l.jpg">
                <div class="fxt-header">
                    <a href="/" class="fxt-logo"><img src="/res/images/logo-white.png" alt="Logo"></a>
                    <h1><?php echo Yii::$app->params['passNewTitle'][$lang]?></h1>
                    <p><?php echo Yii::$app->params['passNewText'][$lang]?></p>
                </div>
            </div>
            <div class="col-md-6 col-12 fxt-bg-color">
                <div class="fxt-content">
                    <div class="fxt-form">
                        <h2><?php echo Yii::$app->params['passNewFormTitle'][$lang]?></h2>
                        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-3">
                                <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control','id'=>'password']) ?>
                                <i toggle="#password" class="fa fa-fw fa-eye toggle-password field-icon"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="fxt-transformY-50 fxt-transition-delay-5">
                                <div class="text-center">
                                    <button type="submit" class="fxt-btn-fill"><?php echo Yii::$app->params['save'][$lang]?></button>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$lang = Yii::$app->language;
$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="fxt-template-animation fxt-template-layout10 has-animation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-12 fxt-bg-img" data-bg-image="/loginres/img/figure/bg10-l.jpg">
                <div class="fxt-header">
                    <a href="/" class="fxt-logo"><img src="/res/images/logo-white.png" alt="Logo"></a>
                    <h1><?php echo Yii::$app->params['passTitle'][$lang]?></h1>
                    <p><?php echo Yii::$app->params['passText'][$lang]?></p>
                    <a href="/registration" class="fxt-btn-ghost"><?php echo Yii::$app->params['lRegistration'][$lang]?></a>
                </div>
            </div>
            <div class="col-md-6 col-12 fxt-bg-color">
                <div class="fxt-content">
                    <div class="fxt-form">
                        <h2><?php echo Yii::$app->params['passTitleForm'][$lang]?></h2>
                        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-3">
                                    <?= $form->field($model, 'email')->textInput(['class'=>'form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="fxt-transformY-50 fxt-transition-delay-5">
                                    <div class="text-center">
                                        <button type="submit" class="fxt-btn-fill"><?php echo Yii::$app->params['send'][$lang]?></button>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
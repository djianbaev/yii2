<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 16.04.2020
 * Time: 22:04
 */

/* @var $this yii\web\View*/

$lang = Yii::$app->language;
?>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846a261e3d7.jpg);"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">

            </div>
        </div>
    </div>
</section>

<section class="services-section-four">
    <div class="auto-container">
        <div class="inner-container">
            <div class="title-box">
                <h2>Ваша заявка принята<br></h2>
            </div>
            <div class="service-list">
                <ul class="clearfix">
                    <li><span class="icon flaticon-interface-1"></span> Заполнение формы</li>
                    <li><span class="icon flaticon-interface-1"></span> Отправка заявки</li>
                </ul>
            </div>
            <div class="text">Спасибо что выбрали нас!Наши операторы свяжуться с Вами в течении одного часа</div>
            <div class="link-box">

            </div>
        </div>
    </div>
</section>



<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 18:39
 */

use yii\helpers\Url;

/* @var $this yii\web\View */

$lang = Yii::$app->language;

$this->title = 'Результаты поиска';
?>
<style>
    .search-hr{
       border-top: 1px solid #d4ae25;
    }
</style>

<section class="page-banner">
    <div class="image-layer" style="background-image:url(/res/images/menu/lifevibes5e846ad88ca65.jpg);"></div>

    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1><?php echo Yii::$app->params['searchResult'][$lang]?></h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="/"><?php echo Yii::$app->params['pMain'][$lang]?></a></li>
                        <li><?php echo Yii::$app->params['searchResult'][$lang]?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if (!empty($row_guide) || !empty($row_tours) || !empty($row_services) || !empty($row_info) || !empty($row_cities) || !empty($row_moments) || !empty($row_footers)): ?>
    <section class="destinations-two">
        <div class="auto-container">
            <div class="results-row">
                <div class="title-row clearfix">
                    <div class="sec-title">
                        <h2><?php echo Yii::$app->params['searchResultFor'][$lang]?> <span style="color: #d4ae25;"><?php echo $query; ?></span></h2>
                    </div>
                </div>

                <?php if (!empty($row_guide)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_guide as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'guide', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'guide', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'guide', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'guide', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_tours)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_tours as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'tour', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'tour', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'tour', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'tour', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_services)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_services as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'service', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'service', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'service', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'service', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_info)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_info as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'info', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'info', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'info', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'info', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_cities)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_cities as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'city', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'city', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'city', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'city', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_moments)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_moments as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'moment', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'moment', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'moment', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'moment', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

                <?php if (!empty($row_footers)): ?>
                    <div class="row clearfix">
                        <?php foreach ($row_footers as $info): ?>
                            <div class="news-block-three masonry-item col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                                    <div class="image-box">
                                        <figure class="image">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'footer', 'id' => $info['id']]) ?>"><img
                                                        src="<?php echo $info['image'] ?>" alt="" title=""></a>
                                        </figure>
                                        <a href="<?php echo Url::to(['site/searchitem', 'type' => 'footer', 'id' => $info['id']]) ?>"
                                           class="link-layer"></a>
                                    </div>
                                    <div class="lower-content">
                                        <div class="content">
                                            <h4>
                                                <a href="<?php echo Url::to(['site/searchitem', 'type' => 'footer', 'id' => $info['id']]) ?>"><?php echo $info['name_' . $lang] ?></a>
                                            </h4>
                                        </div>
                                        <div class="link-box">
                                            <a href="<?php echo Url::to(['site/searchitem', 'type' => 'footer', 'id' => $info['id']]) ?>">
                                                <span class="icon flaticon-logout"></span>
                                                <span class="link-text"><?php echo Yii::$app->params['mLearnMore'][$lang]?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <hr class="search-hr">
                <?php endif; ?>

            </div>
        </div>
    </section>
<?php else: ?>
    <section class="destinations-two">
        <div class="auto-container">
            <div class="results-row">
                <div class="title-row clearfix">
                    <div class="sec-title">
                        <h2><?php echo Yii::$app->params['searchResultNo'][$lang]?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>




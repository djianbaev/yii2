<?php

/* @var $this yii\web\View */
$lang = Yii::$app->language;

switch ($lang){
    case 'ru':
        $this->title = 'Узбекистан - Сердце древнего Востока. Великий Шелковый Путь';
        $this->registerMetaTag([
            'name' => 'description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'name' => 'keywords',
            'content' => '',
        ]);
        $this->registerMetaTag([
            'property' => 'og:title',
            'content'=>$this->title,
        ]);
        $this->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'property' => 'og:loacle',
            'content'=>'ru-RU',
        ]);
        break;
    case 'en':
        $this->title = 'Узбекистан - Сердце древнего Востока. Великий Шелковый Путь';
        $this->registerMetaTag([
            'name' => 'description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'name' => 'keywords',
            'content' => '',
        ]);
        $this->registerMetaTag([
            'property' => 'og:title',
            'content'=>$this->title,
        ]);
        $this->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'property' => 'og:loacle',
            'content'=>'ru-RU',
        ]);
        break;
    case 'fr':
        $this->title = 'Узбекистан - Сердце древнего Востока. Великий Шелковый Путь';
        $this->registerMetaTag([
            'name' => 'description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'name' => 'keywords',
            'content' => '',
        ]);
        $this->registerMetaTag([
            'property' => 'og:title',
            'content'=>$this->title,
        ]);
        $this->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'property' => 'og:loacle',
            'content'=>'ru-RU',
        ]);
        break;
    case 'it':
        $this->title = 'Узбекистан - Сердце древнего Востока. Великий Шелковый Путь';
        $this->registerMetaTag([
            'name' => 'description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'name' => 'keywords',
            'content' => '',
        ]);
        $this->registerMetaTag([
            'property' => 'og:title',
            'content'=>$this->title,
        ]);
        $this->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Отройте для древний и загадочный мир',
        ]);
        $this->registerMetaTag([
            'property' => 'og:loacle',
            'content'=>'ru-RU',
        ]);
        break;
}
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$this->registerMetaTag([
    'property' => 'og:url',
    'content'=>$actual_link,
]);
$this->registerMetaTag([
    'property' => 'og:site_name',
    'content'=>'life-vibes.com',
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'=>'http://life-vibes.com/res/images/logo1.png',
]);

?>
<?php echo \frontend\components\SliderWidget::widget();?>

<?php echo \frontend\components\IndexQuoteWidget::widget();?>

<?php echo \frontend\components\IndexGuideWidget::widget(); ?>

<?php echo \frontend\components\IndexDestinationWidget::widget();?>

<?php if(!empty($video)):?>
    <div class="embed-responsive embed-responsive-16by9" style="height: initial;">
        <?php echo $video['link']?>
    </div>
<?php endif;?>

<?php echo \frontend\components\ReviewWidget::widget();?>
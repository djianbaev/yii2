<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Stylesheets -->
    <link href="/res/css/bootstrap.css" rel="stylesheet"/>
    <link href="/res/css/style.css" rel="stylesheet"/>
    <link href="/res/css/u-header.css" rel="stylesheet"/>

    <!-- Responsive File -->
    <link href="/res/css/responsive.css" rel="stylesheet"/>

    <link rel="shortcut icon" href="/res/images/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/res/images/favicon.ico" type="image/x-icon"/>

    <!-- Responsive Settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="/res/js/respond.js"></script>
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader">
        <div class="icon"></div>
    </div>
    <?php echo \frontend\components\HeaderWidget::widget();?>

    <?php echo $content;?>

    <?php echo \frontend\components\FooterWidget::widget();?>
</div>
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html">
    <span class="icon flaticon-back"></span>
</div>
<script src="/res/js/jquery.js"></script>
<script src="/res/js/popper.min.js"></script>
<script src="/res/js/bootstrap.min.js"></script>
<script src="/res/js/jquery-ui.js"></script>
<script src="/res/js/jquery.fancybox.js"></script>
<script src="/res/js/owl.js"></script>
<script src="/res/js/mixitup.js"></script>
<script src="/res/js/appear.js"></script>
<script src="/res/js/wow.js"></script>
<script src="/res/js/scrollbar.js"></script>
<script src="/res/js/datetimepicker.js"></script>
<script src="/res/js/validate.js"></script>
<script src="/res/js/paroller.js"></script>
<script src="/res/js/element-in-view.js"></script>
<script src="/res/js/custom-script.js"></script>
<script>
    $(".u-header-nav").hover(function () {
        $(".u-header-nav").addClass("open");
    });
    $(".u-header-nav").mouseleave(function () {
        if ($(document).scrollTop() < 10) {
            $(".u-header-nav").removeClass("open");
        }
    });
    $(window).scroll(function (e) {
        if ($(document).scrollTop() > 0) {
            $(".u-header-nav").addClass("open");
        } else {
            $(".u-header-nav").removeClass("open");
        }
    });

    $(".u-header-nav-sub-list a").hover(function() {
        let elem = $(this);
        let id = elem.attr("data-id");
        elem.parent(".u-header-nav-sub-content")
            .siblings(".u-header-nav-sub-item")
            .removeClass("show");
        $(`#${id}`)
            .parent(".u-header-nav-sub-content")
            .find(".u-header-nav-sub-item")
            .removeClass("show");
        $(`#${id}`).addClass("show");
    });

    $(".u-header-phone i").click(function () {
        $(".u-header-mobile-phone").toggleClass("show");
    });
    $(".u-header-menu-toggle").click(function () {
        $(".u-header-mobile").show();
        $(".u-header-mobile-menu").animate(
            {
                left: 0
            },
            500
        );
    });
    $(".u-header-mobile-bg, .u-header-mobile-close").click(function () {
        $(".u-header-mobile-menu").animate(
            {
                left: "-100%"
            },
            500,
            function () {
                $(".u-header-mobile").hide();
            }
        );
    });
    $(".u-header-mobile-back").click(function () {
        $(this)
            .parent()
            .parent()
            .hide();
    });
    $(".u-header-mobile-link a").click(function () {
        $(this)
            .next()
            .show();
    });
    $("#search").click(function () {
        $(".u-header-search-form").css({display: "flex"});
    });
    $(document).on("click", ".u-header-search-close", function () {
        $("#search-form").css({display: "none"});
    });

    $(document).on('click',function (e) {
        var el = '#search';
        if (jQuery(e.target).closest(el).length) return;
        $("#search-form").css({display: "none"});
    });
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/x-icon" href="/res/images/favicon.png">
    <link rel="stylesheet" href="/loginres/css/bootstrap.min.css">
    <link rel="stylesheet" href="/loginres/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/loginres/font/flaticon.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/loginres/css/style.css">
</head>
<body>
<?php $this->beginBody() ?>

    <?php echo $content;?>

<script src="/loginres/js/jquery-3.3.1.min.js"></script>
<script src="/loginres/js/popper.min.js"></script>
<script src="/loginres/js/bootstrap.min.js"></script>
<script src="/loginres/js/imagesloaded.pkgd.min.js"></script>
<script src="/loginres/js/validator.min.js"></script>
<script src="/loginres/js/main.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

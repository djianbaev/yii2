<?php
return [
    'adminEmail' => 'admin@life-vibes.com',
    'infoEmail' => 'info@life-vibes.com',
    'managerEmail' => 'manager@life-vibes.com',
    'bookingEmail' => 'booking@life-vibes.com',
    'senderEmail' => 'admin@life-vibes.com',
    'senderName' => 'Life Vibes',
    'user.passwordResetTokenExpire' => 3600,


    /*** Поиск ***/
    'hSearch'=>[
        'ru'=>'Поиск',
        'en'=>'Search',
        'fr'=>'Recherche',
        'it'=>'Ricerca',
    ],
    'hSearchHint'=>[
        'ru'=>'Что вы хотите найти?',
        'en'=>'What do you want to find?',
        'fr'=>'Que voulez-vous trouver?',
        'it'=>'Cosa volete trovare?',
    ],

    'hSearchSubmit'=>[
        'ru'=>'Искать',
        'en'=>'Search',
        'fr'=>'Rechercher',
        'it'=>'Cerca',
    ],
    /*** Войти ***/
    'hLogin'=>[
        'ru'=>'Войти',
        'en'=>'Login',
        'fr'=>'Identifiez-vous',
        'it'=>'Accedi',
    ],
    'hProfile'=>[
        'ru'=>'Профиль',
        'en'=>'Profile',
        'fr'=>'Profil',
        'it'=>'Profilo',
    ],

    /*** Контакты ***/
    'hContact'=>[
        'ru'=>'Контакты',
        'en'=>'Contacts',
        'fr'=>'Contacts',
        'it'=>'Contatti',
    ],

    /*** Соц сети ***/
    'hSocial'=>[
        'ru'=>'Соц.сети',
        'en'=>'Social Network',
        'fr'=>'Réseaux sociaux',
        'it'=>'Reti sociali',
    ],

    /*** Узнать больше ***/
    'mLearnMore'=>[
        'ru'=>'Подробнее',
        'en'=>'Learn More',
        'fr'=>'Savoir plus',
        'it'=>'Scopri di più',
    ],
    'mBack'=>[
        'ru'=>'Назад',
        'en'=>'Back',
        'fr'=>'Retour',
        'it'=>'Indietro',
    ],

    /*** Breadcumbs Главная***/
    'pMain'=>[
        'ru'=>'Главная',
        'en'=>'Home Page',
        'fr'=>'Page principal',
        'it'=>'Principale',
    ],

    /*** Subscribe Widget ***/
    'wSubscribe'=>[
        'ru'=>'Подпишитесь на нашу рассылку',
        'en'=>'Subscribe to our newsletter',
        'fr'=>'Inscrivez-vous à notre liste de contacts',
        'it'=>'Iscriviti alla nostra mailing list',
    ],
    'wSubscribeText'=>[
        'ru'=>'Подпишитесь и будьте вкурсе последних акций и новостей',
        'en'=>'Subscribe and keep up to date for the latest events and news',
        'fr'=>'Inscrivez-vous et restez informé des dernières promotions et actualités',
        'it'=>'Iscriviti e rimani aggiornato sulle ultime promozioni e novità',
    ],
    'wSubscribeFormName'=>[
        'ru'=>'Ваше имя',
        'en'=>'Your Name',
        'fr'=>'Votre prénom',
        'it'=>'Nome',
    ],
    'wSubscribeFormEmail'=>[
        'ru'=>'Эл.Адрес',
        'en'=>'E-mail',
        'fr'=>'Courrier',
        'it'=>'E-mail',
    ],
    'wSubscribeFormSubmit'=>[
        'ru'=>'Подписаться',
        'en'=>'Subscribe',
        'fr'=>'Souscription',
        'it'=>'Abbonarsi',
    ],

    /*** Показать все ****/
    'wShowAll'=>[
        'ru'=>'Показать всё',
        'en'=>'Show All',
        'fr'=>'Afficher tout',
        'it'=>'Mostra tutti',
    ],
    /*** Фильтр***/
    'wFilterCities'=>[
        'ru'=>'Города',
        'en'=>'Cities',
        'fr'=>'Villes',
        'it'=>'Città',
    ],
    'wFilterSubmit'=>[
        'ru'=>'Применить фильтр',
        'en'=>'Apply Filter',
        'fr'=>'Appliquer le filtre',
        'it'=>'Applica il filtro',
    ],

    /*** Experience ***/
    'expTitle'=>[
        'ru'=>'Выберите то что вам подходит больше всего',
        'en'=>'Choose the one that suite you best',
        'fr'=>'Choisissez ce qui vous convient le mieux',
        'it'=>'Scegliete quello più conveniente per lei',
    ],
    'expSubTitle'=>[
        'ru'=>'Популярные туры',
        'en'=>'Popular tours',
        'fr'=>'Voyage populaire',
        'it'=>'Visite popolari',
    ],
    'expPriceFrom'=>[
        'ru'=>'От',
        'en'=>'From',
        'fr'=>'A partir de',
        'it'=>'Da',
    ],
    'expBook'=>[
        'ru'=>'Забронировать',
        'en'=>'Book',
        'fr'=>'Réserver',
        'it'=>'Prenotare',
    ],
    'expAllTour'=>[
        'ru'=>'Все туры',
        'en'=>'All tours',
        'fr'=>'Tous les programmes de voyage',
        'it'=>'Tutti i programmi di viaggio',
    ],

    /*** Tours ***/
    'tourDetailed'=>[
        'ru'=>'Подробная информация',
        'en'=>'Detailed information',
        'fr'=>'Information détaillées',
        'it'=>'Informazioni dettagliate',
    ],
    'tourPerson'=>[
        'ru'=>'Персона',
        'en'=>'Persona',
        'fr'=>'Personne',
        'it'=>'Persona',
    ],
    'tourPrice'=>[
        'ru'=>'Цена',
        'en'=>'Price',
        'fr'=>'Prix',
        'it'=>'Prezzo',
    ],
    'bookName'=>[
        'ru'=>'Ваше имя',
        'en'=>'Your Name',
        'fr'=>'Votre prénom',
        'it'=>'Nome',
    ],
    'bookEmail'=>[
        'ru'=>'Эл.Адрес',
        'en'=>'E-mail',
        'fr'=>'Courrier',
        'it'=>'E-mail',
    ],
    'bookAdults'=>[
        'ru'=>'Взрослые',
        'en'=>'Adults',
        'fr'=>'Adultes',
        'it'=>'Adulti',
    ],
    'bookKids'=>[
        'ru'=>'Дети',
        'en'=>'Kids',
        'fr'=>'Enfants',
        'it'=>'Bambini',
    ],
    'bookSubmit'=>[
        'ru'=>'Подтвердить',
        'en'=>'Confirm',
        'fr'=>'Confirmer',
        'it'=>'Confermare',
    ],

    'tourCalendar'=>[
        'ru'=>'Лучшее время для путешествий',
        'en'=>'Best time to travel',
        'fr'=>'Meilleur moment pour voyager',
        'it'=>'Miglior momento per viaggiare',
    ],

    'reviews'=>[
        'ru'=>'Отзывы',
        'en'=>'Reviews',
        'fr'=>'Commentaires',
        'it'=>'Recensioni',
    ],
    'reviewTitle'=>[
        'ru'=>'Поделитесь своими впечетлениями',
        'en'=>'Share your impressions',
        'fr'=>'Partagez vos impressions',
        'it'=>'Condividi le tue impressioni',
    ],
    'reviewSub1'=>[
        'ru'=>'Ваш email адрес не будет доступен для общего просмотра.',
        'en'=>'Your email address will not be available for general viewing.',
        'fr'=>'Votre adresse électronique ne sera pas disponible pour une consultation générale.',
        'it'=>'Il tuo indirizzo e-mail non sarà disponibile per la consultazione generale.',
    ],
    'reviewSub2'=>[
        'ru'=>'Ваш отзыв будет опубликован после модерации.',
        'en'=>'Your feedback will be published after moderation.',
        'fr'=>'Votre avis sera publié après la modération.',
        'it'=>'La tua critica sarà pubblicata dopo la moderazione.',
    ],

    'reviewText'=>[
        'ru'=>'Ваш отзыв',
        'en'=>'Your feedback',
        'fr'=>'Votre avis',
        'it'=>'La tua critica',
    ],
    'reviewName'=>[
        'ru'=>'Имя Фамилия',
        'en'=>'First name, Last name',
        'fr'=>'Prénom, nom de famille ',
        'it'=>'Nome, Cognome ',
    ],
    'reviewEmail'=>[
        'ru'=>'Эл.Адрес',
        'en'=>'E-mail',
        'fr'=>'Courrier',
        'it'=>'E-mail',
    ],
    'reviewFile'=>[
        'ru'=>'Выбрать файл (Фотография)',
        'en'=>'Select file (The photo)',
        'fr'=>'Sélectionner un fichier (La photo)',
        'it'=>'Scegliere il file (La foto)',
    ],
    'reviewSend'=>[
        'ru'=>'Поделится',
        'en'=>'Share',
        'fr'=>'Partager',
        'it'=>'Condividere',
    ],

    /*** Мастер классы ***/
    'hours'=>[
        'ru'=>'ч.',
        'en'=>'h.',
        'fr'=>'h.',
        'it'=>'o.',
    ],
    'dates'=>[
        'ru'=>'Даты проведения',
        'en'=>'Dates',
        'fr'=>'Rendez-vous',
        'it'=>'Date',
    ],

    /*** Log In***/
    'lWelcome'=>[
        'ru'=>'Добро пожаловать!',
        'en'=>'Welcome!',
        'fr'=>'Bienvenue!',
        'it'=>'Benvenuti!',
    ],
    'lNoProfile'=>[
        'ru'=>'Если у Вас еще нет аккаунта, то советуем вам зарегистрироваться. Это займет всего пару секунд вашего времени.',
        'en'=>'If you do not have an account yet, we advise you to register. It will only take a few seconds of your time.',
        'fr'=>'Si vous n\'avez pas encore de compte, nous vous conseillons de vous inscrire. Cela ne prendra que quelques secondes de votre temps.',
        'it'=>'Se non avete ancora un account, vi consigliamo di registrarvi. Ci vorranno solo pochi secondi del vostro tempo.',
    ],
    'lYesProfile'=>[
        'ru'=>'Если у Вас есть аккаунта, Вы можете войти или же продолжить как гость.',
        'en'=>'If you have an account, you can log in or continue as a guest.',
        'fr'=>'Si vous avez un compte, vous pouvez vous connecter ou continuer en tant qu\'invité.',
        'it'=>'Se hai un account, puoi accedere o continuare come ospite.',
    ],
    'lRegistration'=>[
        'ru'=>'Регистрация',
        'en'=>'Registration',
        'fr'=>'Enregistrement',
        'it'=>'Registrazione',
    ],
    'lLogin'=>[
        'ru'=>'Войдите в свой аккаунт',
        'en'=>'Sign in to your account',
        'fr'=>'Identifiez-vous sur votre compte',
        'it'=>'Accedi al tuo account',
    ],
    'lForgot'=>[
        'ru'=>'Забыли пароль',
        'en'=>'Password forgotten',
        'fr'=>'Mot de passe oublié',
        'it'=>'Password dimenticata',
    ],
    'lCreateAccount'=>[
        'ru'=>'Создать аккаунт',
        'en'=>'Create an account',
        'fr'=>'Créer un compte',
        'it'=>'Crea un account',
    ],
    'lCreate'=>[
        'ru'=>'Создать',
        'en'=>'Create',
        'fr'=>'Créer',
        'it'=>'Creare',
    ],
    'passTitle'=>[
        'ru'=>'Забыли пароль?',
        'en'=>'Forgot your password?',
        'fr'=>'Mot de passe oublié?',
        'it'=>'Hai dimenticato la password?',
    ],
    'passText'=>[
        'ru'=>'Введите адрес эл.почты введенный вами при регистрации. Если у Вас нет доступа к эл.почте, зарегистрируйтесь снова. Это займет несколько секунд.',
        'en'=>'Enter the email address you entered during registration. If you do not have access to e-mail, register again. It takes a few of seconds.',
        'fr'=>'Saisissez l\'adresse e-mail que vous avez saisie lors de l\'inscription. Si vous n\'avez pas accès au courrier électronique, réinscrivez-vous. Cela prend quelques secondes.',
        'it'=>'Inserisci l\'indirizzo email che hai inserito durante la registrazione. Se non si ha accesso all\'e-mail, registrarsi di nuovo. Ci vogliono pochi secondi.',
    ],
    'passTitleForm'=>[
        'ru'=>'Восстановить пароль',
        'en'=>'Restore password',
        'fr'=>'Restaurer le mot de passe',
        'it'=>'Ripristina password',
    ],
    'send'=>[
        'ru'=>'Отправить ',
        'en'=>'Send',
        'fr'=>'Envoyer',
        'it'=>'Inviare',
    ],

    'passNewTitle'=>[
        'ru'=>'Запомните новый пароль',
        'en'=>'Remember the new password',
        'fr'=>'N\'oubliez pas le nouveau mot de passe',
        'it'=>'Ricorda la nuova password',
    ],
    'passNewText'=>[
        'ru'=>'Если Вы снова забудете пароль, не переживайте, Вы сможете его воостановить в любое время.',
        'en'=>'If you forget the password again, do not worry, you can restore it at any time.',
        'fr'=>'Si vous oubliez à nouveau le mot de passe, ne vous inquiétez pas, vous pouvez le restaurer à tout moment.',
        'it'=>'Se dimentichi di nuovo la password, non ti preoccupare, puoi ripristinarla in qualsiasi momento.',
    ],
    'passNewFormTitle'=>[
        'ru'=>'Введите новый пароль',
        'en'=>'Enter a new password',
        'fr'=>'entrer un nouveau mot de passe',
        'it'=>'Inserisci una nuova password',
    ],
    'save'=>[
        'ru'=>'Сохранить',
        'en'=>'Save',
        'fr'=>'Sauver',
        'it'=>'Salva',
    ],

    'searchResult'=>[
        'ru'=>'Результаты поиска',
        'en'=>'Searching results',
        'fr'=>'Résultats de recherche',
        'it'=>'Risultati di ricerca',
    ],
    'searchResultNo'=>[
        'ru'=>'Совпадений не найдено',
        'en'=>'No matches found',
        'fr'=>'Aucun résultat trouvé',
        'it'=>'Nessun risultato',
    ],
    'searchResultFor'=>[
        'ru'=>'Результаты по запросу',
        'en'=>'Results upon request',
        'fr'=>'Résultats sur demande',
        'it'=>'Risultati su richiesta',
    ],

    /*** Контакты ***/
    'cAddress'=>[
        'ru'=>'<h4>Адрес</h4><div class="subtitle">Для визита в офис</div>',
        'en'=>'<h4>Address</h4><div class="subtitle">For a visit to the office</div>',
        'fr'=>'<h4>Adresse</h4><div class="subtitle">Pour une visite au bureau</div>',
        'it'=>'<h4>L\'indirizzo</h4><div class="subtitle">Per una visita in ufficio</div>',
    ],
    'cPhone'=>[
        'ru'=>'<h4>Телефон</h4><div class="subtitle">Для звонков</div>',
        'en'=>'<h4>Telephone</h4><div class="subtitle">For calls</div>',
        'fr'=>'<h4>Téléphone</h4><div class="subtitle">Pour les appels</div>',
        'it'=>'<h4>Telefono</h4><div class="subtitle">Per chiamare</div>',
    ],
    'cEmail'=>[
        'ru'=>'<h4>Электронная почта</h4><div class="subtitle">Для вопросов</div>',
        'en'=>'<h4>E-mail</h4><div class="subtitle">For questions</div>',
        'fr'=>'<h4>Courrier</h4><div class="subtitle">Pour les questions</div>',
        'it'=>'<h4>E-mail</h4><div class="subtitle">Per domande</div>',
    ],
    'cHours'=>[
        'ru'=>'<h4>Рабочие часы</h4><div class="subtitle">Для удобного визита</div>',
        'en'=>'<h4>Working hours</h4><div class="subtitle">For a convenient visit</div>',
        'fr'=>'<h4>Horaires de travail</h4><div class="subtitle">Pour une visite confortable</div>',
        'it'=>'<h4>Orario di lavoro</h4><div class="subtitle">Per una visita conveniente</div>',
    ],

    'cFormTitle'=>[
        'ru'=>'<h2>Отправьте<strong> Сообщение</strong></h2><div class="lower-text">Всегда рады вам помочь</div>',
        'en'=>'<h2>Send<strong> a message</strong></h2><div class="lower-text">Always happy to help you</div>',
        'fr'=>'<h2>Envoyer<strong> un message</strong></h2><div class="lower-text">Toujours content de vous aider</div>',
        'it'=>'<h2>Invia<strong> un messaggio</strong></h2><div class="lower-text">Sempre felice di aiutarvi</div>',
    ],
    'cFormSubject'=>[
        'ru'=>'Тема',
        'en'=>'Subject',
        'fr'=>'Sujet',
        'it'=>'Oggetto',
    ],
    'cFormMessage'=>[
        'ru'=>'Сообщение',
        'en'=>'Message',
        'fr'=>'Message',
        'it'=>'Messaggio',
    ],
    'cFormMessageText'=>[
        'ru'=>'Введите ваше сообщение',
        'en'=>'Type in your message',
        'fr'=>'Saisissez votre message',
        'it'=>'Inserisci il tuo messaggio',
    ],
];

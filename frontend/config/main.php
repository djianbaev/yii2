<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'   => 'en',
    'sourceLanguage' => 'en_GB',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>''
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['en', 'it', 'fr', 'de', 'ru'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/'=>'site/index',

                'search'=>'site/search',
                'search/result/<type:[0-9a-zA-Z\-]+>/<id:\d+>'=>'site/searchitem',
                'subscribe'=>'site/subscribe',
                'tour-booking'=>'site/tourbooking',
                'service-booking'=>'site/servicebooking',
                'moment-booking'=>'site/momentbooking',

                'login'=>'site/login',
                'logout'=>'site/logout',
                'registration'=>'site/signup',
                'verify-email'=>'site/verify-email',
                'forgot-password'=>'site/request-password-reset',
                'reset-password'=>'site/reset-password',

                'welcome-new-client'=>'client/welcome',
                'failed-verify'=>'client/failedverify',
                'email-for-reset-send'=>'client/emailresetpass',
                'account'=>'client/index',
                'create-my-data'=>'client/create',
                'edit-my-data'=>'client/update',

                'contact-us'=>'site/contact',
                'about-us'=>'menu/us',
                'about-excellence'=>'menu/excellence',
                'about-our-team'=>'menu/ourteam',
                'tours-packages'=>'/site/tours',
                'tours-packages/<tour_slug:[0-9a-zA-Z\-]+>'=>'site/tourview',
                'services/<service_id:\d+>'=>'site/service',
                'read-more-service/<service_item_id:\d+>'=>'site/serviceitem',
                'send-application'=>'site/application',

                'more-unforgettable-moments'=>'site/moment',
                'more-uzbek-cuisine'=>'site/cuisine',

                'more-unforgettable-moments/<moment_slug:[0-9a-zA-Z\-]+>'=>'site/momentview',
                'more-uzbek-cuisine/<cuisine_slug:[0-9a-zA-Z\-]+>'=>'site/cuisineview',

                'about-destination/<city_id:\d+>'=>'site/city',

                '/ajax/review-tour'=>'site/reviewtour',
                '/ajax/review-service'=>'site/reviewservice',
                '/ajax/contact'=>'site/ajaxcontact',

                'usefull-links/<footer_id:\d+>'=>'site/footer',

                'page-<menu_slug:[0-9a-zA-Z\-]+>'=>'menu/index',
                '<menu_slug:[0-9a-zA-Z\-]+>/<guide_slug:[0-9a-zA-Z\-]+>'=>'menu/headerguide',
                '<menu_slug:[0-9a-zA-Z\-]+>/<city_id:\d+>/<guide_slug:[0-9a-zA-Z\-]+>'=>'menu/guideitems',
                '<menu_slug:[0-9a-zA-Z\-]+>/<guide_slug:[0-9a-zA-Z\-]+>/<item_slug:[0-9a-zA-Z\-]+>'=>'menu/guideitem',
                [
                    'pattern'=>'<menu_slug:[0-9a-zA-Z\-]+>/<info_slug:[0-9a-zA-Z\-]+>',
                    'route'=>'menu/infoitem',
                    'suffix'=>'.html'
                ],
            ],
        ],

    ],
    'params' => $params,
];

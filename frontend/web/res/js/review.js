$('#tour-review-form').on('submit',function (e) {
    e.preventDefault();
    var lang = document.documentElement.lang;
    var url = '/ajax/review-tour';
    //var form = $('#tour-review-form')[0];
    //var formData = new FormData(form);
    var formData = new FormData(this);
    $("#rtSubmit").prop("disabled", true);

    if(lang == 'en'){

    }else{
        url = '/'+lang+url;
    }

    $.ajax({
        url:url,
        type: 'POST',
        data:formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend:function(){
            swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
        },
        success:function(response) {
            if(response == 200){
                swal({
                    type: 'success',
                    position: 'center',
                    showConfirmButton: false,
                });
            }else {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }
        },
        error:function() {
            swal({
                type: 'error',
                position: 'center',
                showConfirmButton: false,
            });
        }
    });

});

$("#fileTR").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'image/jpeg', 'image/png', 'image/jpg'];
    if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) || (fileType == match[4]) || (fileType == match[5]))){
        alert('JPG, JPEG, & PNG');
        $("#fileTR").val('');
        return false;
    }
});

$('#service-review-form').on('submit',function (e) {
    e.preventDefault();
    var lang = document.documentElement.lang;
    var url = '/ajax/review-service';
    //var form = $('#tour-review-form')[0];
    //var formData = new FormData(form);
    var formData = new FormData(this);
    $("#rsSubmit").prop("disabled", true);

    if(lang == 'en'){

    }else{
        url = '/'+lang+url;
    }

    $.ajax({
        url:url,
        type: 'POST',
        data:formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend:function(){
            swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
        },
        success:function(response) {
            if(response == 200){
                swal({
                    type: 'success',
                    position: 'center',
                    showConfirmButton: false,
                });
            }else {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }
        },
        error:function() {
            swal({
                type: 'error',
                position: 'center',
                showConfirmButton: false,
            });
        }
    });

});

$("#fileSR").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'image/jpeg', 'image/png', 'image/jpg'];
    if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) || (fileType == match[4]) || (fileType == match[5]))){
        alert('JPG, JPEG, & PNG');
        $("#fileSR").val('');
        return false;
    }
});


$('#contact-form').on('submit',function (e) {
    e.preventDefault();
    var lang = document.documentElement.lang;
    var url = '/ajax/contact';
    var data = $(this).serializeArray();

    if(lang == 'en'){
    }else{
        url = '/'+lang+url;
    }

    $.ajax({
        url:url,
        type: 'POST',
        data:data,
        beforeSend:function(){
            swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
        },
        success:function(response) {
            console.log(response);
            swal({
                type: 'success',
                position: 'center',
                showConfirmButton: false,
            });
        },
        error:function(XMLHttpRequest, textStatus, errorThrown) {
            if (XMLHttpRequest.readyState == 4) {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }
            else if (XMLHttpRequest.readyState == 0) {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }
            else {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }
        }
    });

});



$('#subscribe-form').on('submit',function (e) {
    e.preventDefault();
    var lang = document.documentElement.lang;
    var url = '/subscribe';
    var data = $(this).serializeArray();

    if(lang == 'en'){
    }else{
        url = '/'+lang+url;
    }

    $.ajax({
        url:url,
        type: 'POST',
        data:data,
        beforeSend:function(){
            swal({
                imageUrl:'/res/images/spinner.svg',
                position: 'center',
                showConfirmButton: false,
            });
        },
        success:function(response) {
            if (response == 200){
                swal({
                    type: 'success',
                    position: 'center',
                    showConfirmButton: false,
                });
            }else {
                swal({
                    type: 'error',
                    position: 'center',
                    showConfirmButton: false,
                });
            }

        },
        error:function() {
            swal({
                type: 'error',
                position: 'center',
                showConfirmButton: false,
            });
        }
    });

});


$('#tour-book-form').on('submit',function (e) {
    swal({
        imageUrl:'/res/images/spinner.svg',
        position: 'center',
        showConfirmButton: false,
    });
});
$('#service-book-form').on('submit',function (e) {
    swal({
        imageUrl:'/res/images/spinner.svg',
        position: 'center',
        showConfirmButton: false,
    });
});
$('#moment-book-form').on('submit',function (e) {
    swal({
        imageUrl:'/res/images/spinner.svg',
        position: 'center',
        showConfirmButton: false,
    });
});
$('#cuisine-book-form').on('submit',function (e) {
    swal({
        imageUrl:'/res/images/spinner.svg',
        position: 'center',
        showConfirmButton: false,
    });
});



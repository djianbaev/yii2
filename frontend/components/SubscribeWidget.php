<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 01.04.2020
 * Time: 12:43
 */
namespace frontend\components;

use yii\base\Widget;

class SubscribeWidget extends Widget{
        public function run(){
            $this->view->registerJsFile('/res/js/review.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
            $this->view->registerJsFile('/res/sweetalert/sweetalert2.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
            $this->view->registerCssFile('/res/sweetalert/sweetalert2.min.css');
            return $this->render('subscribe');
        }
}
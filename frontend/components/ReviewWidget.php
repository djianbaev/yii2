<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 16:35
 */

namespace frontend\components;

use yii\base\Widget;
use yii\db\Query;

class ReviewWidget extends Widget{
    public function run()
    {
        $query = new Query();
        $query->select([
            'rt.fullname as fullname',
            'rt.text as review',
            't.name_ru as name_ru',
            't.name_en as name_en',
            't.name_fr as name_fr',
            't.name_it as name_it',
            't.name_de as name_de',
        ])->from(['ri'=>'review_index'])
            ->leftJoin(['rt'=>'review_tour'],'rt.id=ri.review_tour_id')
            ->leftJoin(['t'=>'tour'],'t.id=rt.tour_id')
            ->where(['=','ri.review_service_id',0]);

        $command = $query->createCommand();
        $rowT = $command->queryAll();

        $query = new Query();
        $query->select([
            'rs.fullname as fullname',
            'rs.text as review',
            'si.name_ru as name_ru',
            'si.name_en as name_en',
            'si.name_fr as name_fr',
            'si.name_it as name_it',
            'si.name_de as name_de',
        ])->from(['ri'=>'review_index'])
            ->leftJoin(['rs'=>'review_service'],'rs.id=ri.review_service_id')
            ->leftJoin(['si'=>'service_item'],'si.id=rs.service_item_id')
            ->where(['=','ri.review_tour_id',0]);

        $command = $query->createCommand();
        $rowS = $command->queryAll();


        return $this->render('review',[
            'rowT'=>$rowT,
            'rowS'=>$rowS
        ]);
    }
}
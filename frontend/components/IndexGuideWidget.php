<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:39
 */

namespace frontend\components;

use backend\models\Menu;
use backend\models\widget\IndexGuide;
use yii\jui\Widget;

class IndexGuideWidget extends Widget{
    public function run(){
        $model = IndexGuide::find()->asArray()->one();
        $menu = Menu::find()->where(['action'=>'menu/guide'])->asArray()->one();
        return $this->render('index/guide',[
            'model'=>$model,
            'menu'=>$menu
        ]);
    }
}

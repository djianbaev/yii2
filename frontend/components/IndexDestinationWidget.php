<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:43
 */

namespace frontend\components;

use backend\models\city\City;
use backend\models\widget\IndexDestination;
use yii\jui\Widget;

class IndexDestinationWidget extends Widget{
    public function run()
    {
        $block = IndexDestination::find()->asArray()->one();
        $models = City::find()->asArray()->all();
        return $this->render('index/destination',[
            'block'=>$block,
            'models'=>$models
        ]);
    }
}
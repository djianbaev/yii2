<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:21
 */

namespace frontend\components;

use backend\models\AboutMenu;
use backend\models\city\GuideMenu;
use backend\models\info\InfoMenu;
use backend\models\Menu;
use backend\models\PlusMenu;
use backend\models\Social;
use backend\models\TeamMenu;
use backend\models\tour\Service;
use backend\models\tour\TourMenu;
use yii\jui\Widget;

class HeaderWidget extends Widget{
    public function run()
    {
        $models = Menu::find()->where(['status'=>1])->orderBy(['number'=>SORT_ASC])->asArray()->all();

        /*** about us subs ***/
        $about_us = AboutMenu::find()->asArray()->one();
        $about_plus = PlusMenu::find()->asArray()->one();
        $about_team = TeamMenu::find()->asArray()->one();

        /*** my guide ***/
        $guide_items = GuideMenu::find()->where(['status'=>1])->orderBy(['number'=>SORT_ASC])->asArray()->all();

        /*** info ***/
        $info_items = InfoMenu::find()->where(['status'=>1])->orderBy(['number'=>SORT_ASC])->asArray()->all();

        /*** services ***/
        $service_items = Service::find()->asArray()->all();
        $tour_menu = TourMenu::find()->asArray()->one();

        /*** social ***/
        $social = Social::find()->asArray()->one();

        return $this->render('header',[
            'models'=>$models,
            'about_us'=>$about_us,
            'about_plus'=>$about_plus,
            'about_team'=>$about_team,
            'guide_items'=>$guide_items,
            'info_items'=>$info_items,
            'service_items'=>$service_items,
            'tour_menu'=>$tour_menu,
            'social'=>$social
        ]);
    }
}
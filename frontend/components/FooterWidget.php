<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:46
 */

namespace frontend\components;

use backend\models\Footer;
use backend\models\FooterItem;
use backend\models\Social;
use yii\jui\Widget;

class FooterWidget extends Widget{
    public function run(){
        $about = Social::find()->asArray()->one();
        $categories = Footer::find()->asArray()->all();
        $datas = [];
        foreach ($categories as $category){
            $datas[$category['id']] = FooterItem::find()->where(['footer_id'=>$category['id']])->asArray()->all();
        }

        return $this->render('footer',[
            'categories'=>$categories,
            'datas'=>$datas,
            'about'=>$about
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:44
 */

use yii\helpers\Url;

$lang = Yii::$app->language;
?>
<section class="destinations-section">
    <div class="auto-container">
        <div class="title-row clearfix">
            <div class="sec-title-two centered with-border">
                <div class="upper-text"><?php echo $block['sub_title_' . $lang] ?></div>
                <h2><?php echo $block['title_' . $lang] ?></h2>
                <div class="separator"></div>
            </div>
        </div>

        <div class="destinations-box">
            <div class="row clearfix">
                <?php if (!empty($models)): ?>
                    <?php foreach ($models as $model): ?>
                        <div class="destination-block col-xl-3 col-lg-4 col-md-6 col-sm-12 wow fadeInLeft"
                             data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure class="image">
                                        <a href="<?php echo Url::to(['site/city','city_id'=>$model['id']])?>">
                                            <img src="<?php echo $model['image']?>" alt="" title=""></a>
                                    </figure>
                                    <div class="caption-box">
                                        <div class="count-name"><?php echo $model['name_'.$lang]?></div>
                                    </div>
                                    <div class="hover-box">
                                        <div class="hover-inner">
                                            <div class="hover-content">
                                                <div class="icon-box">
                                                    <img src="<?php echo $model['map']?>"/>
                                                </div>
                                                <h5 class="title"><?php echo $model['name_'.$lang]?></h5>
                                                <div class="link-box">
                                                    <a href="<?php echo Url::to(['site/city','city_id'=>$model['id']])?>" class="theme-btn btn-style-two">
                                                        <div class="btn-title"><?php echo Yii::$app->params['mLearnMore'][$lang]?></div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>

    </div>
</section>

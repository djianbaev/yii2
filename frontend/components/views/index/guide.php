<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:40
 */

use yii\helpers\Url;

$lang = Yii::$app->language;
?>

<section class="facts-section">
    <div class="image-layer" style="background-image: url(<?php echo $model['image']?>);"></div>
    <div class="auto-container">
        <div class="sec-title light centered">
            <h2><?php echo $model['quote_'.$lang]?></h2>
        </div>
        <div class="row clearfix text-center">
            <!--Featured Service-->
            <div class="featured-service-block-two col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <img src="/res/images/guide.png" width="100"/>
                    </div>
                    <div class="content-box">
                        <div class="title-box">
                            <h4><?php echo $model['title_'.$lang]?></h4>
                        </div>
                    </div>
                    <div class="link-box">
                        <a href="<?php echo Url::to(['menu/index','menu_slug'=>$menu['slug']])?>" class="theme-btn btn-style-three"><span class="btn-title"><?php echo $model['button_'.$lang]?></span></a>
                    </div>
                </div>
            </div>
            <!--Featured Service-->
        </div>
    </div>
</section>

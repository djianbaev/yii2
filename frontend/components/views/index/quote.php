<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:32
 */

use yii\helpers\Url;

$lang = Yii::$app->language;
?>
<section class="featured-services">
    <div class="image-layer" style="background-image: url(/res/images/background/image-4.jpg);"></div>
    <div class="auto-container">
        <div class="sec-title-two centered with-border">
            <div class="upper-text"><?php echo $quote['author_'.$lang]?></div>
            <h2><?php echo $quote['text_'.$lang]?></h2>
            <div class="separator"></div>
        </div>
        <div class="row clearfix">

            <div class="featured-service-block col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="2000ms">
                    <div class="icon-box">
                        <img src="<?php echo $models[0]['image']?>"/>
                    </div>
                    <div class="content-box">
                        <div class="title-box">
                            <h4><?php echo $models[0]['name_'.$lang]?></h4>
                        </div>
                        <div class="text">
                            <?php echo $models[0]['text_'.$lang]?>
                        </div>
                    </div>
                    <div class="link-box">
                        <a href="/more-unforgettable-moments" class="theme-btn btn-style-five"><div class="btn-title"><?php echo Yii::$app->params['mLearnMore'][$lang]?></div></a>
                    </div>
                </div>
            </div>

            <div class="featured-service-block col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="2000ms">
                    <div class="icon-box">
                        <img src="<?php echo $models[1]['image']?>"/>
                    </div>
                    <div class="content-box">
                        <div class="title-box">
                            <h4><?php echo $models[1]['name_'.$lang]?></h4>
                        </div>
                        <div class="text">
                            <?php echo $models[1]['text_'.$lang]?>
                        </div>
                    </div>
                    <div class="link-box">
                        <a href="<?php echo Url::to(['menu/index','menu_slug'=>$exp['slug']])?>" class="theme-btn btn-style-five"><div class="btn-title"><?php echo Yii::$app->params['mLearnMore'][$lang]?></div></a>
                    </div>
                </div>
            </div>

            <div class="featured-service-block col-xl-4 col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="2000ms">
                    <div class="icon-box">
                        <img src="<?php echo $models[2]['image']?>"/>
                    </div>
                    <div class="content-box">
                        <div class="title-box">
                            <h4><?php echo $models[2]['name_'.$lang]?></h4>
                        </div>
                        <div class="text">
                            <?php echo $models[2]['text_'.$lang]?>
                        </div>
                    </div>
                    <div class="link-box">
                        <a href="/more-uzbek-cuisine" class="theme-btn btn-style-five"><div class="btn-title"><?php echo Yii::$app->params['mLearnMore'][$lang]?></div></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

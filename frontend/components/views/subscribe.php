<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 01.04.2020
 * Time: 12:45
 */
$lang = Yii::$app->language;
?>
<section class="subscribe-section wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
    <div class="">
        <div class="inner">
            <div class="title">
                <h4><?php echo Yii::$app->params['wSubscribe'][$lang]?></h4>
                <div class="subtitle"><?php echo Yii::$app->params['wSubscribeText'][$lang]?></div>
            </div>
            <div class="default-form subscribe-form-two">
                <form method="post" id="subscribe-form">
                    <input type="hidden" name="<?php echo Yii::$app->request->csrfParam?>" value="<?php echo Yii::$app->request->csrfToken?>">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="field-inner">
                                <input type="text" name="name" placeholder="<?php echo Yii::$app->params['wSubscribeFormName'][$lang]?>" required="" value="">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="field-inner">
                                <input type="email" name="email" placeholder="<?php echo Yii::$app->params['wSubscribeFormEmail'][$lang]?>" required="" value="">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-12 col-sm-12">
                            <button type="submit" class="theme-btn btn-style-six"><span class="btn-title"><?php echo Yii::$app->params['wSubscribeFormSubmit'][$lang]?></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

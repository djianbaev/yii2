<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:22
 */

use yii\helpers\Url;

$lang = Yii::$app->language;

?>

<header class="u-header">
    <div class="u-header-white">
        <div class="container">
            <div class="u-header-top">
                <div class="u-header-menu-toggle">
                    <i class="icon flaticon-menu-1"></i>
                </div>
                <div class="u-header-logo">
                    <a href="/"><img src="/res/images/logo1.png" alt=""/></a>
                </div>
                <div class="u-header-contact">
                    <div>
                        <div>
                            <i class="sl-icon-phone" aria-hidden="true"></i>
                        </div>
                        <div>
                            <a href="#"><?php echo $social['phone'] ?></a>
                            <span><?php echo $social['email'] ?><br><?php echo $social['address_' . $lang] ?></span>
                        </div>
                    </div>
                </div>
                <div class="u-header-actions">
                    <div class="u-header-action" id="search">
                        <i class="icon flaticon-magnifying-glass"></i>
                        <a rel="nofollow" href="#">Поиск</a>
                        <form action="/search" id="search-form" class="u-header-search-form">
                            <div>
                                <i class="icon flaticon-magnifying-glass"></i>
                                <input type="text" name="words" placeholder="Что вы хотите найти?" required/>
                            </div>
                            <button type="submit" class="u-header-search-close">
                                искать
                                <i class="fa fa-chevron-right"></i>
                            </button>
                        </form>
                    </div>
                    <div class="u-header-action">
                        <i class="sl-icon-user"></i>
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <a href="/account">Аккаунт</a>
                        <?php else: ?>
                            <a href="/login">Войти</a>
                        <?php endif; ?>
                    </div>
                    <div class="u-header-action">
                        <i class="sl-icon-envelope"></i
                        ><a href="/contact-us">Контакы</a>
                    </div>
                    <div class="u-header-action">
                        <i class="sl-icon-share"></i>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">Соц.сети</a
                        >
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="<?php echo $social['telegram'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-telegram"></i>Telegram</a>
                            <a href="<?php echo $social['instagram'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-instagram"></i>Instagram</a>
                            <a href="<?php echo $social['facebook'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-facebook"></i>Facebook</a>
                            <a href="<?php echo $social['youtube'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-youtube"></i>Youtube</a>
                            <a href="<?php echo $social['whatsapp'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-whatsapp"></i>Whatsapp</a>
                            <a href="<?php echo $social['twitter'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-twitter"></i>Twitter</a>
                            <a href="<?php echo $social['pinterest'] ?>" class="dropdown-item u-header-social"><i
                                        class="fab fa-pinterest"></i>Pinterest</a>
                        </div>
                    </div>

                    <div class="u-header-action">
                        <i class="sl-icon-globe"></i>
                        <?php if ($lang == 'en'):?>
                        <a href="/en" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">English</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="/ru" class="dropdown-item">Русский</a>
                            <a href="/fr" class="dropdown-item">France</a>
                            <a href="/it" class="dropdown-item">Italian</a>
                        </div>
                        <?php endif;?>
                        <?php if ($lang == 'ru'):?>
                            <a href="/ru" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Русский</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="/en" class="dropdown-item">English</a>
                                <a href="/fr" class="dropdown-item">France</a>
                                <a href="/it" class="dropdown-item">Italian</a>
                            </div>
                        <?php endif;?>
                        <?php if ($lang == 'fr'):?>
                            <a href="/fr" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">France</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="/en" class="dropdown-item">English</a>
                                <a href="/ru" class="dropdown-item">Русский</a>
                                <a href="/it" class="dropdown-item">Italian</a>
                            </div>
                        <?php endif;?>
                        <?php if ($lang == 'it'):?>
                            <a href="/it" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Italian</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="/en" class="dropdown-item">English</a>
                                <a href="/ru" class="dropdown-item">Русский</a>
                                <a href="/fr" class="dropdown-item">France</a>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <div class="u-header-phone">
                    <i class="sl-icon-phone"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="u-header-mobile-phone text-center">
        <a href="#"><?php echo $social['phone'] ?></a>
    </div>
    <div class="u-header-nav">
        <div class="container">
            <ul class="u-header-nav-list">


                <?php if (!empty($models)): ?>
                    <?php foreach ($models as $model): ?>

                        <?php if ($model['action'] == 'menu/about'): ?>
                            <li>
                                <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a>

                                <div class="u-header-dropdown">
                                    <div class="container">
                                        <div class="u-header-dropdown-flex">
                                            <div class="u-header-nav-sub-list">
                                                <a data-id="sub<?php echo $model['id'] ?>-1"
                                                   href="/about-us"><?php echo $about_us['title_' . $lang] ?></a>
                                                <a data-id="sub<?php echo $model['id'] ?>-2"
                                                   href="/about-excellence"><?php echo $about_plus['title_' . $lang] ?></a>
                                                <a data-id="sub<?php echo $model['id'] ?>-3"
                                                   href="/about-our-team"><?php echo $about_team['title_' . $lang] ?></a>
                                            </div>
                                            <div class="u-header-nav-sub-content">
                                                <div class="u-header-nav-sub-item show"
                                                     id="sub<?php echo $model['id'] ?>-1">
                                                    <div class="u-header-nav-sub-img">
                                                        <img src="<?php echo $about_us['image'] ?>" alt=""/>
                                                    </div>
                                                    <div>
                                                        <div class="u-header-nav-sub-title">
                                                            <?php echo $about_us['title_' . $lang] ?>
                                                        </div>
                                                        <div class="u-header-nav-sub-text">
                                                            <?php echo $about_us['text_' . $lang] ?>
                                                        </div>
                                                        <a href="/about-us" class="u-header-nav-sub-more">Learn More
                                                            <i class="fa fa-chevron-right"
                                                               style="font-size: 11px; margin-left: 10px;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="u-header-nav-sub-item" id="sub<?php echo $model['id'] ?>-2">
                                                    <div class="u-header-nav-sub-img">
                                                        <img src="<?php echo $about_plus['image'] ?>" alt=""/>
                                                    </div>
                                                    <div>
                                                        <div class="u-header-nav-sub-title">
                                                            <?php echo $about_plus['title_' . $lang] ?>
                                                        </div>
                                                        <div class="u-header-nav-sub-text">
                                                            <?php echo $about_plus['text_' . $lang] ?>
                                                        </div>
                                                        <a href="/about-excellence" class="u-header-nav-sub-more">Learn
                                                            More
                                                            <i class="fa fa-chevron-right"
                                                               style="font-size: 11px; margin-left: 10px;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="u-header-nav-sub-item" id="sub<?php echo $model['id'] ?>-3">
                                                    <div class="u-header-nav-sub-img">
                                                        <img src="<?php echo $about_team['image'] ?>" alt=""/>
                                                    </div>
                                                    <div>
                                                        <div class="u-header-nav-sub-title">
                                                            <?php echo $about_team['title_' . $lang] ?>
                                                        </div>
                                                        <div class="u-header-nav-sub-text">
                                                            <?php echo $about_team['text_' . $lang] ?>
                                                        </div>
                                                        <a href="/about-our-team" class="u-header-nav-sub-more">Learn
                                                            More
                                                            <i class="fa fa-chevron-right"
                                                               style="font-size: 11px; margin-left: 10px;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>

                        <?php if ($model['action'] == 'menu/guide'): ?>
                            <li>
                                <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a>
                                <?php if (!empty($guide_items)): ?>
                                    <div class="u-header-dropdown">
                                        <div class="container">
                                            <div class="u-header-dropdown-flex">
                                                <div class="u-header-nav-sub-list">
                                                    <?php for ($i = 0; $i < count($guide_items); $i++): ?>
                                                        <a data-id="sub<?php echo $model['id'] ?>-<?php echo $i ?>"
                                                           href="<?php echo Url::to(['menu/headerguide', 'menu_slug' => $model['slug'], 'guide_slug' => $guide_items[$i]['slug']]) ?>"><?php echo $guide_items[$i]['name_' . $lang] ?></a>
                                                    <?php endfor; ?>
                                                </div>
                                                <div class="u-header-nav-sub-content">
                                                    <?php for ($i = 0; $i < count($guide_items); $i++): ?>
                                                        <?php if ($i == 0): ?>
                                                            <div class="u-header-nav-sub-item show"
                                                                 id="sub<?php echo $model['id'] ?>-<?php echo $i; ?>">
                                                                <div class="u-header-nav-sub-img">
                                                                    <img src="<?php echo $guide_items[$i]['image'] ?>"
                                                                         alt=""/>
                                                                </div>
                                                                <div>
                                                                    <div class="u-header-nav-sub-title">
                                                                        <?php echo $guide_items[$i]['name_' . $lang] ?>
                                                                    </div>
                                                                    <div class="u-header-nav-sub-text">
                                                                        <?php echo $guide_items[$i]['text_' . $lang] ?>
                                                                    </div>
                                                                    <a href="<?php echo Url::to(['menu/headerguide', 'menu_slug' => $model['slug'], 'guide_slug' => $guide_items[$i]['slug']]) ?>"
                                                                       class="u-header-nav-sub-more">Learn More
                                                                        <i class="fa fa-chevron-right"
                                                                           style="font-size: 11px; margin-left: 10px;"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="u-header-nav-sub-item"
                                                                 id="sub<?php echo $model['id'] ?>-<?php echo $i; ?>">
                                                                <div class="u-header-nav-sub-img">
                                                                    <img src="<?php echo $guide_items[$i]['image'] ?>"
                                                                         alt=""/>
                                                                </div>
                                                                <div>
                                                                    <div class="u-header-nav-sub-title">
                                                                        <?php echo $guide_items[$i]['name_' . $lang] ?>
                                                                    </div>
                                                                    <div class="u-header-nav-sub-text">
                                                                        <?php echo $guide_items[$i]['text_' . $lang] ?>
                                                                    </div>
                                                                    <a href="<?php echo Url::to(['menu/headerguide', 'menu_slug' => $model['slug'], 'guide_slug' => $guide_items[$i]['slug']]) ?>"
                                                                       class="u-header-nav-sub-more">Learn More
                                                                        <i class="fa fa-chevron-right"
                                                                           style="font-size: 11px; margin-left: 10px;"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>

                        <?php if ($model['action'] == 'menu/regionalinfo'): ?>
                            <li>
                                <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a>
                                <?php if (!empty($info_items)): ?>
                                    <div class="u-header-dropdown">
                                        <div class="container">
                                            <div class="u-header-dropdown-flex">
                                                <div class="u-header-nav-sub-list">
                                                    <?php for ($i = 0; $i < count($info_items); $i++): ?>
                                                        <a data-id="sub<?php echo $model['id'] ?>-<?php echo $i ?>"
                                                           href="<?php echo Url::to(['menu/infoitem', 'menu_slug' => $model['slug'], 'info_slug' => $info_items[$i]['slug']]) ?>"><?php echo $info_items[$i]['name_' . $lang] ?></a>
                                                    <?php endfor; ?>
                                                </div>
                                                <div class="u-header-nav-sub-content">
                                                    <?php for ($i = 0; $i < count($info_items); $i++): ?>
                                                        <?php if ($i == 0): ?>
                                                            <div class="u-header-nav-sub-item show"
                                                                 id="sub<?php echo $model['id'] ?>-<?php echo $i; ?>">
                                                                <div class="u-header-nav-sub-img">
                                                                    <img src="<?php echo $info_items[$i]['image'] ?>"
                                                                         alt=""/>
                                                                </div>
                                                                <div>
                                                                    <div class="u-header-nav-sub-title">
                                                                        <?php echo $info_items[$i]['name_' . $lang] ?>
                                                                    </div>
                                                                    <div class="u-header-nav-sub-text">
                                                                        <?php echo $info_items[$i]['text_' . $lang] ?>
                                                                    </div>
                                                                    <a href="<?php echo Url::to(['menu/infoitem', 'menu_slug' => $model['slug'], 'info_slug' => $info_items[$i]['slug']]) ?>"
                                                                       class="u-header-nav-sub-more">Learn More
                                                                        <i class="fa fa-chevron-right"
                                                                           style="font-size: 11px; margin-left: 10px;"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="u-header-nav-sub-item"
                                                                 id="sub<?php echo $model['id'] ?>-<?php echo $i; ?>">
                                                                <div class="u-header-nav-sub-img">
                                                                    <img src="<?php echo $info_items[$i]['image'] ?>"
                                                                         alt=""/>
                                                                </div>
                                                                <div>
                                                                    <div class="u-header-nav-sub-title">
                                                                        <?php echo $info_items[$i]['name_' . $lang] ?>
                                                                    </div>
                                                                    <div class="u-header-nav-sub-text">
                                                                        <?php echo $info_items[$i]['text_' . $lang] ?>
                                                                    </div>
                                                                    <a href="<?php echo Url::to(['menu/infoitem', 'menu_slug' => $model['slug'], 'info_slug' => $info_items[$i]['slug']]) ?>"
                                                                       class="u-header-nav-sub-more">Learn More
                                                                        <i class="fa fa-chevron-right"
                                                                           style="font-size: 11px; margin-left: 10px;"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>


                        <?php if ($model['action'] == 'menu/experience'): ?>
                            <li>
                                <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a>
                                <?php if (!empty($service_items)): ?>
                                    <div class="u-header-dropdown">
                                        <div class="container">
                                            <div class="u-header-dropdown-flex">
                                                <div class="u-header-nav-sub-list">
                                                    <a data-id="sub<?php echo $model['id'] ?>-666"
                                                       href="/tours-packages"><?php echo $tour_menu['name_' . $lang] ?></a>
                                                    <?php for ($i = 0; $i < count($service_items); $i++): ?>
                                                        <a data-id="sub<?php echo $model['id'] ?>-<?php echo $i ?>"
                                                           href="<?php echo Url::to(['site/service', 'service_id' => $service_items[$i]['id']]) ?>"><?php echo $service_items[$i]['name_' . $lang] ?></a>
                                                    <?php endfor; ?>
                                                </div>
                                                <div class="u-header-nav-sub-content">
                                                    <div class="u-header-nav-sub-item show"
                                                         id="sub<?php echo $model['id'] ?>-666">
                                                        <div class="u-header-nav-sub-img">
                                                            <img src="<?php echo $tour_menu['image'] ?>" alt=""/>
                                                        </div>
                                                        <div>
                                                            <div class="u-header-nav-sub-title">
                                                                <?php echo $tour_menu['name_' . $lang] ?>
                                                            </div>
                                                            <div class="u-header-nav-sub-text">
                                                                <?php echo $tour_menu['text_' . $lang] ?>
                                                            </div>
                                                            <a href="/tours-packages" class="u-header-nav-sub-more">Learn
                                                                More
                                                                <i class="fa fa-chevron-right"
                                                                   style="font-size: 11px; margin-left: 10px;"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <?php for ($i = 0; $i < count($service_items); $i++): ?>

                                                        <div class="u-header-nav-sub-item"
                                                             id="sub<?php echo $model['id'] ?>-<?php echo $i; ?>">
                                                            <div class="u-header-nav-sub-img">
                                                                <img src="<?php echo $service_items[$i]['image'] ?>"
                                                                     alt=""/>
                                                            </div>
                                                            <div>
                                                                <div class="u-header-nav-sub-title">
                                                                    <?php echo $service_items[$i]['name_' . $lang] ?>
                                                                </div>
                                                                <div class="u-header-nav-sub-text">
                                                                    <?php echo $service_items[$i]['text_' . $lang] ?>
                                                                </div>
                                                                <a href="<?php echo Url::to(['site/service', 'service_id' => $service_items[$i]['id']]) ?>"
                                                                   class="u-header-nav-sub-more">Learn More
                                                                    <i class="fa fa-chevron-right"
                                                                       style="font-size: 11px; margin-left: 10px;"></i>
                                                                </a>
                                                            </div>
                                                        </div>

                                                    <?php endfor; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>


                        <?php if ($model['action'] == 'menu/application'): ?>
                            <li>
                                <a href="<?php echo Url::to(['menu/index', 'menu_slug' => $model['slug']]) ?>"><?php echo $model['name_' . $lang] ?></a>
                            </li>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>


            </ul>
        </div>
    </div>
    <div class="u-header-mobile">
        <div class="u-header-mobile-bg"></div>
        <div class="u-header-mobile-menu">
            <div class="u-header-mobile-close">
                <i class="sl-icon-arrow-left-circle"></i>
            </div>
            <form class="u-header-mobile-search">
                <i class="icon flaticon-magnifying-glass"></i>
                <input type="search" placeholder="Search"/>
            </form>
            <div class="u-header-mobile-content">
                <div class="u-header-mobile-list">
                    <div class="u-header-mobile-link">
                        <a href="#"><span>Menu 1</span></a>
                    </div>
                    <div class="u-header-mobile-link">
                        <a href="#"><span>Menu 2</span>
                            <i class="fa fa-chevron-right"></i></a>
                        <div class="u-header-menu-sub">
                            <div class="u-header-mobile-list">
                                <div class="u-header-mobile-link mb-1 u-header-mobile-back">
                                    <a href="#" style="justify-content: flex-start;">
                                        <i class="fa fa-chevron-left"></i>
                                        <span class="pl-1">BACK</span>
                                    </a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 1</span></a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 2</span></a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 3</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-id="mob-sub-2" class="u-header-mobile-link">
                        <a href="#"><span>Menu 3</span>
                            <i class="fa fa-chevron-right"></i></a>
                        <div class="u-header-menu-sub">
                            <div class="u-header-mobile-list">
                                <div class="u-header-mobile-link mb-1 u-header-mobile-back">
                                    <a href="#" style="justify-content: flex-start;">
                                        <i class="fa fa-chevron-left"></i>
                                        <span class="pl-1">BACK</span>
                                    </a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 1</span></a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 2</span></a>
                                </div>
                                <div class="u-header-mobile-link">
                                    <a href="#"><span>Menu 3</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="u-header-contact u-header-mobile-contact" style="display: flex !important;">
                    <div>
                        <i class="fa fa-phone"></i>
                    </div>
                    <div style="display: block;">
                        <a href="#">+99899 000 00 00</a>
                    </div>
                </div>
                <div class="u-header-mobile-actions">
                    <div class="u-header-action">
                        <i class="sl-icon-user"></i>
                        <a href="#">SignUp</a>
                    </div>
                    <div class="u-header-action">
                        <i class="sl-icon-envelope"></i><a href="#">Contact</a>
                    </div>
                    <div class="u-header-action">
                        <i class="sl-icon-share"></i>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">Social</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item u-header-social"><i
                                        class="fab fa-telegram"></i>Telegram</a>
                            <a href="#" class="dropdown-item u-header-social"><i class="fab fa-instagram"></i>Instagram</a>
                            <a href="#" class="dropdown-item u-header-social"><i
                                        class="fab fa-facebook"></i>Facebook</a>
                            <a href="#" class="dropdown-item u-header-social"><i class="fab fa-youtube"></i>Youtube</a>
                        </div>
                    </div>

                    <div class="u-header-action">
                        <i class="sl-icon-globe"></i>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">English</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item" type="button">Action</a>
                            <a href="#" class="dropdown-item" type="button">Another</a>
                            <a href="#" class="dropdown-item" type="button">Something</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:46
 */

use yii\helpers\Url;

$lang = Yii::$app->language;
?>
<footer class="main-footer">
    <!--Widgets Section-->
    <div class="widgets-section container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Column-->
                <div class="column col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="footer-widget info-widget">
                        <div class="logo">
                            <a href="#"><img src="/res/images/logo1.png" alt=""></a>
                        </div>
                        <div class="info">
                            <p style="color: #FFFFFF;">
                                <?php echo $about['about_'.$lang]?>
                            </p>
                        </div>
                    </div>
                </div>

                <?php if (!empty($categories)): ?>
                    <?php foreach ($categories as $category): ?>
                        <div class="column col-xl-4 col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
                                <div class="widget-content">
                                    <div class="widget-title">
                                        <h4><?php echo $category['name_' . $lang] ?></h4>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <ul class="links">
                                                <?php if (!empty($datas[$category['id']])): ?>
                                                    <?php foreach ($datas[$category['id']] as $data): ?>
                                                        <li><a href="<?php echo Url::to(['site/footer','footer_id'=>$data['id']])?>"><?php echo $data['name_'.$lang]?></a></li>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="inner clearfix container">
                <div class="copyright" style="color: #FFFFFF">Life-Vibes &copy; 2020.</div>
            </div>
        </div>
    </div>

</footer>

<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 15.04.2020
 * Time: 16:38
 */

$lang = Yii::$app->language;

?>

<?php if (!empty($rowT) || !empty($rowS)): ?>
    <section class="testimonials-section-three">
        <div class="image-layer left-layer" style="background-image: url(/res/images/background/image-10.png);"></div>

        <div class="auto-container">
            <div class="sec-title centered">
                <h2><strong>Впечетления</strong> путешественников</h2>
            </div>
        </div>
        <div class="auto-container">

            <div class="carousel-box">
                <div class="testimonial-carousel-three owl-theme owl-carousel">
                    <?php if (!empty($rowT)): ?>
                        <?php foreach ($rowT as $item): ?>
                            <div class="testimonial-block-two">
                                <div class="inner-box">
                                    <div class="content">
                                        <div class="title-box">
                                            <h3>“<?php echo $item['name_'.$lang]?>”</h3>
                                        </div>
                                        <div class="text"><?php echo $item['review']?></div>
                                        <div class="info">
                                            <div class="icon-box"><span class="flaticon-quote"></span></div>
                                            <div class="name"><?php echo $item['fullname']?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (!empty($rowS)): ?>
                        <?php foreach ($rowS as $item): ?>
                            <div class="testimonial-block-two">
                                <div class="inner-box">
                                    <div class="content">
                                        <div class="title-box">
                                            <h3>“<?php echo $item['name_'.$lang]?>”</h3>
                                        </div>
                                        <div class="text"><?php echo $item['review']?></div>
                                        <div class="info">
                                            <div class="icon-box"><span class="flaticon-quote"></span></div>
                                            <div class="name"><?php echo $item['fullname']?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>


                </div>
            </div>

        </div>
    </section>

<?php endif; ?>
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:27
 */
$lang = Yii::$app->language;
?>
<?php if(!empty($models)):?>
<section class="banner-section banner-two">
    <div class="banner-carousel owl-theme owl-carousel">
        <?php foreach ($models as $model):?>
        <div class="slide-item">
            <div class="image-layer" style="background-image: url(<?php echo $model['image']?>);"></div>
            <div class="auto-container">
                <div class="content-box">
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</section>
<?php endif;?>
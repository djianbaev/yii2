<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:30
 */
namespace frontend\components;

use backend\models\Menu;
use backend\models\widget\IndexMoments;
use backend\models\widget\IndexQuote;
use yii\jui\Widget;

class IndexQuoteWidget extends Widget{
    public function run(){
        $quote = IndexQuote::find()->asArray()->one();
        $models = IndexMoments::find()->asArray()->all();
        $exp = Menu::find()->where(['action'=>'menu/experience'])->asArray()->one();
        return $this->render('index/quote',[
            'quote'=>$quote,
            'models'=>$models,
            'exp'=>$exp
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Jianbaev Jasur djianbaev.dj@gmail.com
 * Date: 26.03.2020
 * Time: 14:26
 */

namespace frontend\components;

use backend\models\widget\IndexSlider;
use yii\jui\Widget;

class SliderWidget extends Widget{
    public function run(){
        $models = IndexSlider::find()->asArray()->all();
        return $this->render('slider',[
            'models'=>$models
        ]);
    }
}